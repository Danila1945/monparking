<?php

namespace App\Repository;

use App\Entity\Messages;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * @method Messages|null find($id, $lockMode = null, $lockVersion = null)
 * @method Messages|null findOneBy(array $criteria, array $orderBy = null)
 * @method Messages[]    findAll()
 * @method Messages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Messages::class);
    }


    /**
     * @param User $loggedInUser
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function findReceivedAndNotAcknowledgedMessages(User $loggedInUser): int
    {
        $query = $this->createQueryBuilder('m')
            ->select('COUNT(m)')
            ->andWhere('m.receiver = :loggedInUser')
            ->setParameter('loggedInUser', $loggedInUser)
            ->andWhere('m.receiptAcknowledgement = false');

        return (int) $query->getQuery()->getSingleScalarResult();
    }


    /*
    public function findOneBySomeField($value): ?Messages
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
