<?php

namespace App\Repository;

use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Utils\DateTools;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;

/**
 * @method ParkingSublease|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParkingSublease|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParkingSublease[]    findAll()
 * @method ParkingSublease[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @throws Exception
 */
class ParkingSubleaseRepository extends ServiceEntityRepository implements ParkingSubleaseRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParkingSublease::class);
    }

    /**
     * {@inheritdoc}
     */
    public function findIsSubleaseOpenedOrTakenByDayDateAndByRenter(DateTimeImmutable $dayDate,
                                                                    User $loggedInRenterUser): ?ParkingSublease
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.user', 'user')
            ->andWhere('user = :loggedInRenterUser')
            ->setParameter('loggedInRenterUser', $loggedInRenterUser)
            ->andWhere('parking_sublease.isSubleaseOpened = true OR parking_sublease.isTaken = true')
            ->andWhere('parking_sublease.dayDate = :dayDate')
            ->setParameter('dayDate', $dayDate)
            ->orderBy('parking_sublease.creationDate');

        try {
            return $query->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    //Et faire un getOneOrNullResult à la place du bordel

    /**
     * {@inheritdoc}
     */
    public function findIsSubleaseTakenByDayDateAndByTaker(DateTimeImmutable $dayDate, User $loggedTakerUser): ?ParkingSublease
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->andWhere('parking_sublease.dayDate = :dayDate')
            ->setParameter('dayDate', $dayDate)
            ->andWhere('taker = :loggedTakerUser')
            ->setParameter('loggedTakerUser', $loggedTakerUser)
            ->andWhere('parking_sublease.isSubleaseOpened = false')
            ->orderBy('parking_sublease.creationDate');

        try {
            return $query->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByYearAndMonth(string $yearString, string $monthString): array
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('YEAR(parking_sublease.dayDate) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(parking_sublease.dayDate) = :month')
            ->setParameter('month', $monthString)
            ->andWhere('parking_sublease.isTaken = true');

        return $query->getQuery()->getResult();
    }

    /**
     * @return ParkingSublease[]
     */
    public function findTabTakenSubleasesByCurrentMonthAndByRenter(string $yearString, string $monthString,
                                                                   User $renter): array
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('YEAR(parking_sublease.dayDate) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(parking_sublease.dayDate) = :month')
            ->setParameter('month', $monthString)
            ->andWhere('parking_sublease.isTaken = true')
            ->andWhere('parking_sublease.user = :renter')
            ->setParameter('renter', $renter);

        return $query->getQuery()->getResult();
    }

    /**
     * @return ParkingSublease[]
     *
     * @throws Exception
     */
    public function findTabOpenSubleasesByCurrentMonthAndByRenter(string $yearString, string $monthString,
                                                                  User $renter): array
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('YEAR(parking_sublease.dayDate) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(parking_sublease.dayDate) = :month')
            ->setParameter('month', $monthString)
            ->andWhere('parking_sublease.isSubleaseOpened = true')
            ->andWhere('parking_sublease.user = :renter')
            ->setParameter('renter', $renter);

        return $query->getQuery()->getResult();
    }

    /**
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseOpenedByYearAndMonth(string $yearString, string $monthString): array
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('YEAR(parking_sublease.dayDate) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(parking_sublease.dayDate) = :month')
            ->setParameter('month', $monthString)
            ->andWhere('parking_sublease.isSubleaseOpened = true')
            ->orderBy('parking_sublease.creationDate');

        return $query->getQuery()->getResult();
    }

    /**
     * @return ParkingSublease[]
     */
    public function findTabOpenAndTakenSubleasesByChosenMonth(string $yearString, string $monthString): array
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('YEAR(parking_sublease.dayDate) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(parking_sublease.dayDate) = :month')
            ->setParameter('month', $monthString);

        return $query->getQuery()->getResult();
    }

//    /**
//     * @return ParkingSublease[]
//     */
//    public function findTabIsSubleaseTakenOrOpened0()
//    {
//        $query = $this->createQueryBuilder('parking_sublease')
//            ->select('parking_sublease')
//            ->andWhere('parking_sublease.isTaken = :isTaken')
//            ->setParameter('isTaken', true)
//            ->orWhere('parking_sublease.isSubleaseOpened = true')
//            ->leftJoin('parking_sublease.day', 'day')
//            ->addSelect('day')
//            ->leftJoin('parking_sublease.user', 'user')
//            ->addSelect('user')
//            ->leftJoin('parking_sublease.taker', 'taker')
//            ->addSelect('taker')
//            ->orderBy('day.date', 'DESC');
//
//        return $query->getQuery()->getResult();
//    }

//    /**
//     * @return ParkingSublease[]
//     */
//    public function findTabIsSubleaseTakenOrOpened()
//    {
//        $query = $this->createQueryBuilder('parking_sublease')
//            ->select('parking_sublease')
//            ->orderBy('parking_sublease.dayDate', 'DESC');
//
//        return $query->getQuery()->getResult();
//    }

    /**
     * @return ParkingSublease[]
     */
    public function findTabSubleasesByUser(User $user)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.user = :user')
            ->orWhere('parking_sublease.taker = :user')
            ->setParameter('user', $user);

        return $query->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findTabIsSubleaseTakenByRenter(int $loggedRenterUserId, DateTimeImmutable $date)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.user', 'renter')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('renter.id = :renterUser')
            ->setParameter('renterUser', $loggedRenterUserId)
            ->andWhere('day.date >= :date')
            ->setParameter('date', $date)
            ->orderBy('day.date', 'DESC');

        return $query->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     *
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByTaker(User $loggedTaker, DateTimeImmutable $date)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('taker = :loggedTakerUser')
            ->setParameter('loggedTakerUser', $loggedTaker)
            ->andWhere('day.date >= :date')
            ->setParameter('date', $date)
            ->orderBy('day.date', 'DESC');

        return $query->getQuery()->getResult();
    }

    public function findTabPaidSubleasesTakenByTaker(User $loggedTaker)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.user', 'renter')
            ->andWhere('parking_sublease.paidAt IS NOT NULL
            AND parking_sublease.isTaken = true
            AND taker = :loggedTakerUser')
            ->orWhere('parking_sublease.price = 0.0
            AND parking_sublease.isTaken = true
            AND taker = :loggedTakerUser')
            ->setParameter('loggedTakerUser', $loggedTaker)
            ->orderBy('parking_sublease.dayDate', 'DESC')
            ->addOrderBy('renter.firstName', 'ASC');

        return $query->getQuery()->getResult();
    }

    public function findTabNotPaidSubleasesTakenByTaker(User $loggedTaker)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.user', 'renter')
            ->andWhere('parking_sublease.isTaken = true')
            ->andWhere('taker = :loggedTakerUser')
            ->setParameter('loggedTakerUser', $loggedTaker)
            ->andWhere('parking_sublease.price > 0.0')
            ->andWhere('parking_sublease.paidAt IS NULL')
            ->orderBy('parking_sublease.dayDate', 'DESC')
            ->addOrderBy('renter.firstName', 'ASC');

        return $query->getQuery()->getResult();
    }

    public function findTabAllSubleasesTakenByTaker(User $loggedTaker)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.user', 'renter')
            ->andWhere('parking_sublease.isTaken = true')
            ->andWhere('taker = :loggedTakerUser')
            ->setParameter('loggedTakerUser', $loggedTaker)
            ->orderBy('parking_sublease.dayDate', 'DESC')
            ->addOrderBy('renter.firstName', 'ASC');

        return $query->getQuery()->getResult();
    }

    public function findTabPaidSubleasesTakenByRenter(User $loggedRenter)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.user', 'renter')
            ->andWhere('parking_sublease.isTaken = true
            AND renter = :loggedRenterUser
            AND parking_sublease.paidAt IS NOT NULL')
            ->orWhere('parking_sublease.price = 0.0
            AND parking_sublease.isTaken = true
            AND renter = :loggedRenterUser')
            ->setParameter('loggedRenterUser', $loggedRenter)
            ->orderBy('parking_sublease.dayDate', 'DESC')
            ->addOrderBy('taker.firstName', 'ASC');

        return $query->getQuery()->getResult();
    }

    public function findTabNotPaidSubleasesTakenByRenter(User $loggedRenter)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.user', 'renter')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('renter = :loggedRenterUser')
            ->setParameter('loggedRenterUser', $loggedRenter)
            ->andWhere('parking_sublease.price > 0.0')
            ->andWhere('parking_sublease.paidAt IS NULL')
            ->orderBy('parking_sublease.dayDate', 'DESC')
            ->addOrderBy('taker.firstName', 'ASC');

        return $query->getQuery()->getResult();
    }

    public function findTabAllSubleasesTakenByRenter(User $loggedRenter)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.user', 'renter')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('renter = :loggedRenterUser')
            ->setParameter('loggedRenterUser', $loggedRenter)
            ->orderBy('parking_sublease.dayDate', 'DESC')
            ->addOrderBy('renter.firstName', 'ASC');

        return $query->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getContractorsIdRelatedToSubleasesList(User $loggedTaker)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('DISTINCT renter.id')
            ->innerJoin('parking_sublease.user', 'renter')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('taker = :loggedTakerUser')
            ->setParameter('loggedTakerUser', $loggedTaker)
            ->orderBy('renter.firstName');

        $result = $query->getQuery()->getResult();

        return array_map(function ($lease) {
            return $lease['id'];
        }, $result);
    }

    /**
     * {@inheritdoc}
     */
    public function findTabIsSubleaseTakenByRenterAndByTakerBetweenTwoDates(User $loggedInUser, User $otherUser,
                                                                            DateTimeImmutable $beginDate,
                                                                            DateTimeImmutable $endDate)
    {
        $loggedRenterUserId = null;
        $takerUserId = null;
        if ('ROLE_RENTER' === $loggedInUser->getRole()) {
            $loggedRenterUserId = $loggedInUser->getId();
            $takerUserId = $otherUser->getId();
        } elseif ('ROLE_TENANT' === $loggedInUser->getRole()) {
            $loggedRenterUserId = $otherUser->getId();
            $takerUserId = $loggedInUser->getId();
        }

        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.user', 'renter')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('renter.id = :renterUser')
            ->setParameter('renterUser', $loggedRenterUserId)
            ->andWhere('taker.id = :takerUser')
            ->setParameter('takerUser', $takerUserId)
            ->andWhere('parking_sublease.dayDate >= :date')
            ->setParameter('date', $beginDate)
            ->andWhere('parking_sublease.dayDate <= :enddate')
            ->setParameter('enddate', $endDate)
            ->orderBy('parking_sublease.dayDate', 'ASC');

        return $query->getQuery()->getResult();
    }

    /**
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByRenterBetweenTwoDates(int $loggedRenterUserId, DateTimeImmutable $beginDate,
                                                                  DateTimeImmutable $endDate)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.user', 'renter')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('renter.id = :renterUser')
            ->setParameter('renterUser', $loggedRenterUserId)
            ->andWhere('parking_sublease.dayDate >= :beginDate')
            ->setParameter('beginDate', $beginDate)
            ->andWhere('parking_sublease.dayDate <= :endDate')
            ->setParameter('endDate', $endDate)
            ->orderBy('parking_sublease.dayDate', 'ASC');

        return $query->getQuery()->getResult();
    }

    /**
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByTakerBetweenTwoDates(int $loggedTakerId, DateTimeImmutable $beginDate,
                                                                 DateTimeImmutable $endDate)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('taker.id = :loggedTakerId')
            ->setParameter('loggedTakerId', $loggedTakerId)
            ->andWhere('parking_sublease.dayDate >= :beginDate')
            ->setParameter('beginDate', $beginDate)
            ->andWhere('parking_sublease.dayDate <= :endDate')
            ->setParameter('endDate', $endDate)
            ->orderBy('parking_sublease.dayDate', 'ASC');

        return $query->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function findMoneyToBeReceivedSubleasesPerUserSum(User $user): float
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('SUM(parking_sublease.price) as sum')
            ->andWhere('parking_sublease.user = :user')
            ->setParameter('user', $user)
            ->andWhere('parking_sublease.isTaken = true')
            ->innerJoin('parking_sublease.user', 'renter')
            ->andWhere('renter.isFreeRenter = false')
            ->andWhere('parking_sublease.paidAt IS NULL');

        return (float) $query->getQuery()->getSingleScalarResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function findMoneyReceivedSubleasesPerUserSum(User $user): float
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('SUM(parking_sublease.price) as sum')
            ->andWhere('parking_sublease.user = :user')
            ->setParameter('user', $user)
            ->andWhere('parking_sublease.isTaken = true')
            ->andWhere('parking_sublease.paidAt IS NOT NULL');

        return (float) $query->getQuery()->getSingleScalarResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function findMoneyToBeReceivedAndReceivedSubleasesPerUserSum(User $user): float
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('SUM(parking_sublease.price) as sum')
            ->andWhere('parking_sublease.user = :user')
            ->setParameter('user', $user)
            ->andWhere('parking_sublease.isTaken = :true')
            ->setParameter('true', true)
            ->innerJoin('parking_sublease.user', 'renter')
            ->andWhere('renter.isFreeRenter = false');

        return (float) $query->getQuery()->getSingleScalarResult();
    }

    /**
     * {@inheritdoc}
     * @return ParkingSublease[]
     */
    public function findTabMoneyDueSubleasesPerTaker(User $taker): array
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.taker = :taker')
            ->setParameter('taker', $taker)
            ->andWhere('parking_sublease.isTaken = true')
            ->andWhere('parking_sublease.paidAt IS NULL')
            ->andWhere('parking_sublease.price > 0.0')
            ->orderBy('parking_sublease.dayDate');

        return $query->getQuery()->getResult();
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function findMoneyDueSubleasesPerTakerSum(User $taker): float
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('SUM(parking_sublease.price) as sum')
            ->andWhere('parking_sublease.taker = :taker')
            ->setParameter('taker', $taker)
            ->andWhere('parking_sublease.isTaken = true')
            ->andWhere('parking_sublease.paidAt IS NULL')
            ->andWhere('parking_sublease.price > 0.0')
            ->orderBy('parking_sublease.dayDate');

        return (float) $query->getQuery()->getSingleScalarResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(ParkingSublease $parkingSublease, bool $andFlush = true): void
    {
        $entityManager = $this->getEntityManager();

        $entityManager->persist($parkingSublease);

        if ($andFlush) {
            $entityManager->flush();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function flush(bool $andFlush = true): void
    {
        $entityManager = $this->getEntityManager();

        if ($andFlush) {
            $entityManager->flush();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function saveParkingSubleasesTab(array $parkingSublease, bool $andFlush = true): array
    {
        $entityManager = $this->getEntityManager();

        if ($andFlush) {
            $entityManager->flush();
        }

        return [];
    }

    /**
     * {@inheritdoc}
     *
     * @throws ORMException
     */
    public function remove(ParkingSublease $parkingSublease, bool $andFlush = true): void
    {
        $entityManager = $this->getEntityManager();

        $entityManager->remove($parkingSublease);

        if ($andFlush) {
            $entityManager->flush();
        }
    }

//    /**
//     * @throws NoResultException
//     * @throws NonUniqueResultException
//     * @throws Exception
//     */
//    public function countPossibleSubleasesByYearMonthWithoutTaker0(string $yearString, string $monthString): int
//    {
//        $query = $this->createQueryBuilder('parking_sublease')
//            ->select('COUNT(parking_sublease) as count')
//            ->andWhere('parking_sublease.isSubleaseOpened = true')
//            ->andWhere('YEAR(parking_sublease.dayDate) = :year')
//            ->setParameter('year', $yearString)
//            ->andWhere('MONTH(parking_sublease.dayDate) = :month')
//            ->setParameter('month', $monthString)
//            ->andWhere('parking_sublease.dayDate >= :today')
//            ->setParameter('today', DateTools::getTodayDate())
//            ->groupBy('parking_sublease.dayDate');
//
//        return (int) $query->getQuery()->getSingleResult()['count'] ?? 0;
//    }

    /**
     * @throws Exception
     */
    public function findTabPossibleSubleasesByYearMonth(string $yearString, string $monthString): array
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.isSubleaseOpened = true')
            ->andWhere('YEAR(parking_sublease.dayDate) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(parking_sublease.dayDate) = :month')
            ->setParameter('month', $monthString)
            ->andWhere('parking_sublease.dayDate >= :today')
            ->setParameter('today', DateTools::getTodayDate())
            ->orderBy('parking_sublease.creationDate', 'ASC');

        return $query->getQuery()->getResult();
    }

    /**
     * @return ParkingSublease[]
     *
     * @throws Exception
     */
    public function findTabIsSubleaseTakenByTakerDateTodayAndLater(string $yearString, string $monthString,
                                                                   User $loggedTaker): array
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->andWhere('taker = :loggedTakerUser')
            ->setParameter('loggedTakerUser', $loggedTaker)
            ->andWhere('YEAR(parking_sublease.dayDate) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(parking_sublease.dayDate) = :month')
            ->setParameter('month', $monthString)
            ->andWhere('parking_sublease.dayDate >= :today')
            ->setParameter('today', DateTools::getTodayDate());

        return $query->getQuery()->getResult();
    }

    /**
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByTakerInCurrentMonth(string $yearString, string $monthString,
                                                                User $loggedTaker): array
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->andWhere('taker = :loggedTakerUser')
            ->setParameter('loggedTakerUser', $loggedTaker)
            ->andWhere('YEAR(parking_sublease.dayDate) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(parking_sublease.dayDate) = :month')
            ->setParameter('month', $monthString);

        return $query->getQuery()->getResult();
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function countPossibleSubleasesByYearMonth(string $yearString, string $monthString): int
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('COUNT(parking_sublease)')
            ->andWhere('parking_sublease.isSubleaseOpened = true')
            ->andWhere('YEAR(parking_sublease.dayDate) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(parking_sublease.dayDate) = :month')
            ->setParameter('month', $monthString)
            ->andWhere('parking_sublease.dayDate >= :today')
            ->setParameter('today', DateTools::getTodayDate());

        return (int) $query->getQuery()->getSingleScalarResult();
    }

    /**
     * @throws Exception
     */
    public function findTabByParkingSubleaseNumber(int $parkingNumber): array
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.subleaseParkingNumber = :parkingNumber')
            ->setParameter('parkingNumber', $parkingNumber)
            ->andWhere('parking_sublease.dayDate >= :today')
            ->setParameter('today', DateTools::getTodayDate());

        return $query->getQuery()->getResult();
    }
}
