<?php

namespace App\Repository;

use App\Entity\Text;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Text|null find($id, $lockMode = null, $lockVersion = null)
 * @method Text|null findOneBy(array $criteria, array $orderBy = null)
 * @method Text[]    findAll()
 * @method Text[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TextRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Text::class);
    }

    // /**
    //  * @return Requests[] Returns an array of Requests objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Requests
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param User $author
     * @return Text[]
     */
    public function findAllTextsByMostRecentCreationDate(User $author): array
    {
        return $this->createQueryBuilder('text')
            ->andWhere('text.isDiscarded = false')
            ->andWhere('text.authorId = :authorId')
            ->setParameter('authorId', $author->getId())
            ->orderBy('text.creationDate', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Text[] Returns an array of Text objects
     */
    public function findAllDiscardedTextsByMostRecentCreationDate(User $author): array
    {
        return $this->createQueryBuilder('text')
            ->andWhere('text.isDiscarded = true')
            ->andWhere('text.authorId = :authorId')
            ->setParameter('authorId', $author->getId())
            ->orderBy('text.creationDate', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Text[]
     */
    public function findByTextContent(string $content, User $author): array
    {
        $qb = $this->createQueryBuilder('text')
            ->andWhere('text.content LIKE :content')
            ->setParameter('content', '%'.$content.'%')
            ->andWhere('text.isDiscarded = false')
            ->andWhere('text.authorId = :authorId')
            ->setParameter('authorId', $author->getId());

        return $qb->getQuery()->getResult();
    }
}
