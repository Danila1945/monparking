<?php

namespace App\Repository;

use App\Entity\ParkingSublease;
use App\Entity\User;
use DateTimeImmutable;

interface ParkingSubleaseRepositoryInterface
{
    public function findIsSubleaseTakenByDayDateAndByTaker(DateTimeImmutable $dayDateDate, User $loggedTakerUser): ?ParkingSublease;

    public function findIsSubleaseOpenedOrTakenByDayDateAndByRenter(DateTimeImmutable $dayDate, User $loggedInRenterUser): ?ParkingSublease;

    /**
     * @return mixed
     */
    public function findTabIsSubleaseTakenByRenterAndByTakerBetweenTwoDates(User $loggedInUser, User $otherUser,
                                                                            DateTimeImmutable $beginDate,
                                                                            DateTimeImmutable $endDate);

    /**
     * @param User $user
     * @return float
     */
    public function findMoneyToBeReceivedSubleasesPerUserSum(User $user): float;

    /**
     * @param User $user
     * @return float
     */
    public function findMoneyReceivedSubleasesPerUserSum(User $user): float;

    /**
     * @param User $user
     * @return float
     */
    public function findMoneyToBeReceivedAndReceivedSubleasesPerUserSum(User $user): float;

    /**
     * @return mixed
     */
    public function findTabIsSubleaseTakenByRenter(int $loggedRenterUserId, DateTimeImmutable $date);

    /**
     * @return mixed
     */
    public function getContractorsIdRelatedToSubleasesList(User $loggedTaker);

    public function findTabIsSubleaseTakenByTaker(User $loggedTaker, DateTimeImmutable $date);

    public function save(ParkingSublease $parkingSublease, bool $andFlush = true): void;

    /**
     * @return mixed
     */
    public function findTabMoneyDueSubleasesPerTaker(User $taker);

    public function flush(bool $andFlush = true): void;

    /**
     * @param ParkingSublease[] $parkingSublease
     */
    public function saveParkingSubleasesTab(array $parkingSublease, bool $andFlush = true): array;

    public function remove(ParkingSublease $parkingSublease, bool $andFlush = true): void;
}
