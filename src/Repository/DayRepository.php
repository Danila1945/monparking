<?php

namespace App\Repository;

use App\Entity\Day;
use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Utils\DateTools;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;

/**
 * @method Day|null find($id, $lockMode = null, $lockVersion = null)
 * @method Day|null findOneBy(array $criteria, array $orderBy = null)
 * @method Day[]    findAll()
 * @method Day[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Day::class);
    }

    /**
     * @return Day|null
     */
    public function findDayByParkingSublease(ParkingSublease $parkingSublease)
    {
        $query = $this->createQueryBuilder('day')
            ->select('day')
            ->innerJoin('day.parkingSublease', 'parkingSublease')
            ->andWhere('parkingSublease.id = :parkingSubleaseId')
            ->setParameter('parkingSubleaseId', $parkingSublease->getId())
            ->andWhere('parkingSublease.isTaken = :isTaken')
            ->setParameter('isTaken', true);

        try {
            return $query->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return null;
    }

//    public function findFirstAvailableParkingSubleaseByDay(Day $day){
//        $query = $this->createQueryBuilder('fa')
//            ->select('fa')
//            ->setParameter('id', $id)
//            ->innerJoin('fa.parkingSublease', 'fapark')
//            ->andWhere('fapark.isSubleaseOpened = :isSubleaseOpened')
//
//        return $query->getQuery()->getResult();
//
//    }

    /**
     * @return Day[]
     */
    public function findDaysByYearMonth(string $yearString, string $monthString)
    {
        $query = $this->createQueryBuilder('day')
            ->select('day')
            ->andWhere('YEAR(day.date) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(day.date) = :month')
            ->setParameter('month', $monthString);

        return $query->getQuery()->getResult();
    }

    /**
     * @return Day[]
     *
     * @throws Exception
     */
    public function findDaysByYearMonthWithoutTaker(string $yearString, string $monthString, User $taker): array
    {
        $query = $this->createQueryBuilder('day')
            ->select('day')
            ->innerJoin('day.parkingSublease', 'parkingSubleaseRelation')
            ->innerJoin('parkingSubleaseRelation.taker', 'taker')
            ->andWhere('taker != :taker')
            ->setParameter('taker', $taker)
            ->andWhere('parkingSubleaseRelation.isSubleaseOpened = true')
            ->andWhere('YEAR(day.date) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(day.date) = :month')
            ->setParameter('month', $monthString)

            ->andWhere('day.date >= :today')
            ->setParameter('today', DateTools::getTodayDate());

        return $query->getQuery()->getResult();
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function getTotalFreeParkingSubleaseByYearMonth(string $yearString, string $monthString): int
    {
        $query = $this->createQueryBuilder('day')
            ->select('SUM(day.isFreeParkingSubleaseByDayQuantity)')
            ->andWhere('YEAR(day.date) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('day.date >= :today')
            ->setParameter('today', DateTools::getTodayDate())
            ->andWhere('MONTH(day.date) = :month')
            ->setParameter('month', $monthString);

        return (int) $query->getQuery()->getSingleScalarResult();
    }
}
