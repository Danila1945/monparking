<?php

namespace App\Repository;

use App\Entity\UsersLogsEvents;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @method UsersLogsEvents|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersLogsEvents|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersLogsEvents[]    findAll()
 * @method UsersLogsEvents[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersLogsEventsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsersLogsEvents::class);
    }

    public function findTabOldestLogs(int $maxResults)
    {
        $query = $this->createQueryBuilder('logs')
            ->select('logs')
            ->orderBy('logs.loginDate', 'ASC')

            ->orderBy('logs.loginDate')
            ->setMaxResults($maxResults); // here we are only interested in the first result of a tab containing all of the opened subleases of a determied day

        return $query->getQuery()->getResult();

//        try {
//            return $query->getQuery()->getMaxResults();
//        } catch (NonUniqueResultException $e) {
//        }
//
//        return null;

    }


//    public function findIsSubleaseOpenedByDay(int $dayId)
//    {
//        $query = $this->createQueryBuilder('parking_sublease')
//            ->select('parking_sublease')
//            ->innerJoin('parking_sublease.day', 'day')
//            ->andWhere('day.id = :dayId')
//            ->setParameter('dayId', $dayId)
//            ->andWhere('parking_sublease.isSubleaseOpened = :isSubleaseOpened')
//            ->setParameter('isSubleaseOpened', true)
//            ->orderBy('parking_sublease.creationDate')
//            ->setMaxResults(1); // here we are only interested in the first result of a tab containing all of the opened subleases of a determied day
//
//        try {
//            return $query->getQuery()->getOneOrNullResult();
//        } catch (NonUniqueResultException $e) {
//        }
//
//        return null;
//    }

    // /**
    //  * @return UsersLogsEvents[] Returns an array of UsersLogsEvents objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsersLogsEvents
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
