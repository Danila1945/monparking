<?php

namespace App\Form;

use App\DTO\DTOTextResearchContent;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TextResearchContentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('textResearchContent', TextType::class, [
                'attr' => ['class' => 'form-control', 'id' => 'text_research_content', 'rows' => '3', 'cols' => '30',
                    'placeholder' => 'Contenu du texte', 'maxlength' => 30, ],
                'label' => false,
                'required' => true,
                'empty_data' => '',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DTOTextResearchContent::class,
        ]);
    }
}
