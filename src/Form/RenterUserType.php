<?php

namespace App\Form;

use App\DTO\DTOUserRenter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RenterUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'Nom d\'utilisateur :',
                'attr' => ['maxlength' => 15, 'class' => 'form-control'],
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Le contenu des champs de mot de passe doit correspondre.',
                'options' => ['attr' => ['class' => 'password-field form-control form-control']],
                'required' => true,
                'first_options' => ['label' => 'Mot de passe :'],
                'second_options' => ['label' => 'Mot de passe répété :'],
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Prénom :',
                'attr' => ['maxlength' => 15, 'class' => 'form-control'],
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Nom :',
                'attr' => ['maxlength' => 15, 'class' => 'form-control'],
            ])
            ->add('parkingNumber', NumberType::class, [
                'label' => 'No de parking :',
                'attr' => ['maxlength' => 3, 'class' => 'form-control'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // Formulaire va avoir DTOUserRenter pour réceptacle
        $resolver->setDefault('data_class', DTOUserRenter::class);
    }
}
