<?php

namespace App\Form;

use App\DTO\DTOusernameUpdate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsernameUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'Username:',
                'label_attr' => ['class' => 'font-weight-bold'],
                'attr' => ['maxlength' => 15, 'class' => 'form-control'],
            ])

            ->add('phoneNumber', TextType::class, [
                'label' => 'Numéro de téléphone TWINT:',
                'label_attr' => ['class' => 'font-weight-bold'],
                'attr' => ['maxlength' => 20, 'placeholder' => 'facultatif', 'class' => 'form-control'],
                'required' => false,
            ])

            ->add('payPalEmail', TextType::class, [
                'label' => 'Adresse email PayPal:',
                'label_attr' => ['class' => 'font-weight-bold'],
                'attr' => ['maxlength' => 50, 'placeholder' => 'facultatif', 'class' => 'form-control'],
                'required' => false,
            ])

            ->add('revolutPhoneNumber', TextType::class, [
                'label' => 'Numéro de téléphone Revolut:',
                'label_attr' => ['class' => 'font-weight-bold'],
                'attr' => ['maxlength' => 20, 'placeholder' => 'facultatif', 'class' => 'form-control'],
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // Formulaire va avoir DTOUser pour réceptacle
        $resolver->setDefault('data_class', DTOusernameUpdate::class);
    }
}
