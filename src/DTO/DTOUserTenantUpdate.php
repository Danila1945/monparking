<?php


namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;


class DTOUserTenantUpdate
{

    //=========================================================================
    // Properties
    //=========================================================================


    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 15,
     *      minMessage = "Le nom d'utilisateur doit contenir au moins 2 caractères",
     *      maxMessage = "Le nom d'utilisateur ne peut pas dépasser 15 caractères",
     * )
     */
    private $username;


    /**
     * @return string|null
     */
    public function getUsername() : ?string
    {
        return $this->username;
    }

    /**
     * @param $username
     */
    public function setUsername($username) : void
    {
        $this->username = $username;
    }


    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 15,
     *      minMessage = "Le nom doit contenir au moins 2 caractères",
     *      maxMessage = "Le nom ne doit pas dépasser 15 caractères",
     * )
     */
    private $lastName;


    /**
     * @return string|null
     */
    public function getLastName() : ?string
    {
        return $this->lastName;
    }


    /**
     * @param $lastName
     */
    public function setLastName($lastName) : void
    {
        $this->lastName = $lastName;
    }


    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 15,
     *      minMessage = "Le prénom doit contenir au moins 2 caractères",
     *      maxMessage = "Le prénom ne doit pas dépasser 15 caractères",
     * )
     */
    private $firstName;


    /**
     * @return string|null
     */
    public function getFirstName() : ?string
    {
        return $this->firstName;
    }

    public function setFirstName($firstName) : void
    {
        $this->firstName = $firstName;
    }

    /**
     * @var string
     */
    private $role;


    /**
     * @return string|null
     */
    public function getRole() : ?string
    {
        return $this->role;
    }


    /**
     * @param $role
     */
    public function setRole($role) : void
    {
        $this->role = $role;
    }

}
