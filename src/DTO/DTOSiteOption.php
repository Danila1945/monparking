<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class DTOSiteOption
{
    //=========================================================================
    // Properties
    //=========================================================================

    /**
     * @var float
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     * @Assert\Length(
     *      min = 1,
     *      max = 5,
     *      minMessage = "Le montant doit contenir au moins un chiffre",
     *      maxMessage = "Le montant ne peut pas contenir plus de cinq chiffres y-compris le point",
     * )
     */
    private $price;


    public function getPrice(): float
    {
        return $this->price;
    }


    public function setPrice(float $price): void
    {
        $this->price = $price;
    }
}
