<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class DTOUserRenter
{
    //=========================================================================
    // Properties
    //=========================================================================

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 15,
     *      minMessage = "Le nom d'utilisateur doit contenir au moins 2 caractères",
     *      maxMessage = "Le nom d'utilisateur ne peut pas dépasser 15 caractères",
     * )
     */
    private $username;

    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 8,
     *      max = 50,
     *      minMessage = "Le mot de passe doit contenir au moins 8 caractères",
     *      maxMessage = "Le mot de passe doit ne peut pas dépasser 50 caractères",
     * )
     */
    private $password;

    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 15,
     *      minMessage = "Le nom doit contenir au moins 2 caractères",
     *      maxMessage = "Le nom ne doit pas dépasser 15 caractères",
     * )
     */
    private $lastName;

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param $lastName
     */
    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 15,
     *      minMessage = "Le prénom doit contenir au moins 2 caractères",
     *      maxMessage = "Le prénom ne doit pas dépasser 15 caractères",
     * )
     */
    private $firstName;

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @var string
     */
    private $role;

    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * @param $role
     */
    public function setRole($role): void
    {
        $this->role = $role;
    }

    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     * @Assert\Length(
     *      min = 1,
     *      max = 3,
     *      minMessage = "Le numéro de parking doit contenir au moins un chiffre",
     *      maxMessage = "Le numéro de parking ne peut pas contenir plus de trois chiffres",
     * )
     */
    private $parkingNumber;

    public function getParkingNumber(): ?int
    {
        return $this->parkingNumber;
    }

    public function setParkingNumber(int $parkingNumber): void
    {
        $this->parkingNumber = $parkingNumber;
    }
}
