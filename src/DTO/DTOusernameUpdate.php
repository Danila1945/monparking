<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class DTOusernameUpdate
{
    //=========================================================================
    // Properties
    //=========================================================================

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 15,
     *      minMessage = "Le nom d'utilisateur doit contenir au moins 2 caractères",
     *      maxMessage = "Le nom d'utilisateur ne peut pas dépasser 15 caractères",
     * )
     */
    private $username;

    /**
     * @var string
     * @Assert\Regex("/^[0-9]+$/i")(
     *     message="Entrer des chiffres",
     *     )
     *
     * @Assert\Length(
     *     min = 10,
     *     max = 20,
     *     minMessage = "Dix chiffres minimum",
     *     maxMessage = "Vingt chiffres maximum",
     *     )
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @Assert\Regex("/^[0-9]+$/i")(
     *     message="Entrer des chiffres",
     *     )
     *
     * @Assert\Length(
     *     min = 10,
     *     max = 20,
     *     minMessage = "Dix chiffres minimum",
     *     maxMessage = "Vingt chiffres maximum",
     *     )
     */
    private $revolutPhoneNumber;

    public function getRevolutPhoneNumber(): ?string
    {
        return $this->revolutPhoneNumber;
    }

    public function setRevolutPhoneNumber(?string $revolutPhoneNumber): void
    {
        $this->revolutPhoneNumber = $revolutPhoneNumber;
    }

    /**
     * @var string
     *
     * @Assert\Email(
     *     message = "L'adresse '{{ value }}' n'est pas une adresse email valide."
     * )
     *
     * @Assert\Length(
     *     min = 11,
     *     max = 50,
     *     minMessage = "11 caractères minimum",
     *     maxMessage = "50 caractères maximum",
     *     )
     */
    private $payPalEmail;

    public function getPayPalEmail(): ?string
    {
        return $this->payPalEmail;
    }

    public function setPayPalEmail(?string $newPayPalEmail): void
    {
        $this->payPalEmail = $newPayPalEmail;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }
}
