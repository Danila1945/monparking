<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class DTOTextUpdate
{

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 2000,
     *      minMessage = "Le texte doit contenir au moins 2 caractères",
     *      maxMessage = "Le texte ne peut pas dépasser 2000 caractères",
     * )
     */
    private $content;


    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }


    /**
     * @param string $content
     * @return $this
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

}
