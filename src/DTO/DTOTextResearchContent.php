<?php


namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class DTOTextResearchContent
{

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 2,
     *     max = 30,
     *     minMessage = "La recherche doit être composée d'au moins 2 caractères",
     *     maxMessage = "La recherche ne doit pas être composée de plus de 30 caractères",
     *     )
     */
    private $textResearchContent;


    /**
     * @return string|null
     */
    public function getTextResearchContent(): ?string
    {
        return $this->textResearchContent;
    }


    /**
     * @param string $textResearchContent
     */
    public function setTextResearchContent(string $textResearchContent): void
    {
        $this->textResearchContent = $textResearchContent;
    }

}
