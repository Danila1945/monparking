<?php


namespace App\DTO\Sublease;


use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

class DTOParkingSublease
{


    private $userMonthTotal;


    /**
     * DTOParkingSublease constructor.
     * @param int $userId
     * @param string $month
     * @param ArrayCollection $subleases
     */
    public function __construct(int $userId, string $month, ArrayCollection $subleases)
    {
        $this->userMonthTotal = new DTOParkingSubleaseTakerList($month, $userId, $subleases[]);
    }
}

