<?php

namespace App\DTO\Sublease;

use App\Entity\ParkingSublease;

class DTOParkingSubleaseMonthList
{
    private $takerId;
    private $month;
    private $subleasesToBeCalculated;
    private $subleases;
    private $isPaid;
    private $subleasesTotal;

    public function __construct(int $takerId, string $month, array $subleases)
    {
        $this->takerId = $takerId;
        $this->month = $month;
        $this->subleasesToBeCalculated = $subleases;
        $this->isPaid = true;
        $this->subleasesTotal = 0.0;
    }

    public function getMonth(): string
    {
        return $this->month;
    }

    public function setMonth(string $month): void
    {
        $this->month = $month;
    }

    public function getSubleases(): ?array
    {
        return $this->subleases;
    }

    public function setSubleases(array $subleases): void
    {
        $this->subleases = $subleases;
    }

    public function isPaid(): bool
    {
        return $this->isPaid;
    }

    public function setIsPaid(bool $isPaid): void
    {
        $this->isPaid = $isPaid;
    }

    public function getSubleasesTotal(): float
    {
        return $this->subleasesTotal;
    }

    public function setSubleasesTotal(float $subleasesTotal): void
    {
        $this->subleasesTotal = $subleasesTotal;
    }

    public function addSublease(ParkingSublease $sublease)
    {
        if ($sublease->getDayDate()->format('Y-m') === $this->month) {
            $this->subleases[] = $sublease;

            if ($sublease->getIsNotPaid()) {
                $this->isPaid = false;
            }

            $this->subleasesTotal += $sublease->getPrice();
        }
    }

    public function tableFillingUp(): void
    {
        foreach ($this->subleasesToBeCalculated as $sublease) {
            $this->addSublease($sublease);
        }
    }
}
