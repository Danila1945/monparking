<?php

namespace App\DTO\Sublease;

use App\Entity\ParkingSublease;
use App\Entity\User;
use DateTimeImmutable;
use DateTimeZone;
use Exception;

class DTOParkingSubleaseTakerList
{
    private $contractor;
    private $userRole;
    private $month;
    private $subleases;
    private $unPaidSubleasesSum;
    private $monthSubleasesPerContractor;
    private $monthSubleasesPerContractorTotal;

    public function __construct(string $month, User $contractor, string $userRole, array $subleases)
    {
        $this->contractor = $contractor;
        $this->userRole = $userRole;
        $this->month = $month;
        $this->subleases = $subleases;
        $this->subleasesPerMonthAndContractorFulfillment();
        $this->unPaidSumCalculate();
    }

    public function unPaidSumCalculate(): void
    {
        $this->unPaidSubleasesSum = 0;

        if (!empty($this->monthSubleasesPerContractor)) {
            foreach ($this->monthSubleasesPerContractor as $sublease) {
                /* @var ParkingSublease $sublease */
                if ($sublease->getIsNotPaid()) {
                    $this->unPaidSubleasesSum += $sublease->getPrice();
                }
            }
        }
    }

    public function getUnpaidSum(): float
    {
        return $this->unPaidSubleasesSum;
    }

    private function subleasesPerMonthAndContractorFulfillment(): void
    {
        $this->monthSubleasesPerContractorTotal = 0;

        if ('roleRenter' === $this->userRole) {
            $this->subleasesTabBuilding('roleRenter');
        } elseif ('roleTenant' === $this->userRole) {
            $this->subleasesTabBuilding('roleTenant');
        }
        if (!empty($this->monthSubleasesPerContractor)) {
            $this->monthSubleasesPerContractor = array_reverse($this->monthSubleasesPerContractor);
        }
    }

    public function getMonth(): string
    {
        return $this->month;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDueDate(): ?DateTimeImmutable
    {
        try {
            $dateItem = new DateTimeImmutable($this->month, new DateTimeZone('Europe/Paris'));
        } catch (Exception $e) {
            return null;
        }
        $dateItem = $dateItem->modify('last day of this month');
        $dateItem = $dateItem->modify('+ 11 days');

        return $dateItem;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getBeginDueDate(): ?DateTimeImmutable
    {
        try {
            $dateItem = new DateTimeImmutable($this->month, new DateTimeZone('Europe/Paris'));
        } catch (Exception $e) {
            return null;
        }
        $dateItem = $dateItem->modify('last day of this month');
        $dateItem = $dateItem->modify('+ 1 day');

        return $dateItem;
    }

    public function checkIfMonthIsPaid(): bool
    {
        $isMonthPaid = true;

        if (isset($this->monthSubleasesPerContractor)) {
            /* @var ParkingSublease $sublease */
            foreach ($this->monthSubleasesPerContractor as $sublease) {
                if (true === $isMonthPaid) {
                    if ($sublease->getIsNotPaid()) {
                        $isMonthPaid = false;
                    }
                }
            }
        }

        return $isMonthPaid;
    }

    public function getPaymentDate(): ?DateTimeImmutable
    {
        $subleases = $this->monthSubleasesPerContractor;

        /* @var ParkingSublease $lastSublease */
        if (null !== $subleases) {
            $lastSublease = end($subleases);

            return $lastSublease->getPaidAt();
        }

        return null;
    }

    public function getMonthSubleasesPerContractorTotal(): int
    {
        return $this->monthSubleasesPerContractorTotal;
    }

    public function getMonthSubleasesPerContractor(): array
    {
        if (empty($this->monthSubleasesPerContractor)) {
            return [];
        }

        return $this->monthSubleasesPerContractor;
    }

    public function getContractor(): ?User
    {
        if (null !== $this->contractor) {
            return $this->contractor;
        }

        return null;
    }

    public function getUserRole(): string
    {
        return $this->userRole;
    }

    private function subleasesTabBuilding(string $connectedUserRole): void
    {
        /* @var ParkingSublease $sublease */
        foreach ($this->subleases as $sublease) {
            if ('roleRenter' === $connectedUserRole) {
                if (($sublease->getTaker()->getId() === $this->contractor->getId())
                    && ($sublease->getDayDate()->format('Y-m') === $this->month)) {
                    $this->monthSubleasesPerContractor[] = $sublease;
                    if (false === $sublease->getUser()->getIsFreeRenter()
                        || null === $sublease->getUser()->getIsFreeRenter()) {
                        $this->monthSubleasesPerContractorTotal += $sublease->getPrice();
                    }
                }
            } elseif ('roleTenant' === $connectedUserRole) {
                if (($sublease->getUser()->getId() === $this->contractor->getId())
                    && ($sublease->getDayDate()->format('Y-m') === $this->month)) {
                    $this->monthSubleasesPerContractor[] = $sublease;
                    if (false === $sublease->getUser()->getIsFreeRenter()
                        || null === $sublease->getUser()->getIsFreeRenter()) {
                        $this->monthSubleasesPerContractorTotal += $sublease->getPrice();
                    }
                }
            }
        }
    }
}
