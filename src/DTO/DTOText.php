<?php


namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class DTOText
{

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 2000,
     *      minMessage = "Le texte doit contenir au moins 2 caractères",
     *      maxMessage = "Le texte ne peut pas dépasser 2000 caractères",
     * )
     */
    private $content;


    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }


    /**
     * @param string $text
     */
    public function setContent(string $text): void
    {
        $this->content = $text;
    }

}
