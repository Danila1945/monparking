<?php

namespace App\Controller;

use App\DTO\DTOChangePassword;
use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Repository\ParkingSubleaseRepository;
use App\Utils\UserProvider;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserConsulting extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @Route("/userConsulting/{userId}", name="user_consulting", requirements={"userId" = "\d+"})
     * @Entity("user", expr="repository.find(userId)")
     *
     * @throws Exception
     */
    public function __invoke(Request $request, User $user, FormFactoryInterface $formFactory,
                             ParkingSubleaseRepository $parkingSubleaseRepository,
                             UserProvider $userProvider): Response
    {
        $loggedInUser = $userProvider->getLoggedInUser();

        $userProvider->recordUsersLogs('Mon profil');

        $userProvider->checkIsAdministratorOrLoggedInUser($user);

        $form = $formFactory->create(ChangePasswordType::class, new DTOChangePassword());
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $data = $form->getData();

            if ($form->isValid()) {
                $newPassword = $data->getPassword();
                $user->changePassword($newPassword);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', 'Le mot de passe a bien été modifié');

                return $this->redirectToRoute('user_consulting', ['userId' => $user->getId()]);
            }
        }

        $subleasesList = $parkingSubleaseRepository->findTabSubleasesByUser($user);

        if (!empty($subleasesList)) {
            $relatedSubleasesCounter = count($subleasesList);
        } else {
            $relatedSubleasesCounter = 0;
        }

        return $this->render('UserConsulting.twig', [
            'ConsultedUser' => $user,
            'LoggedInUser' => $loggedInUser,
            'formularPass' => $form->createView(),
            'RelatedSubleasesCounter' => $relatedSubleasesCounter,
        ]);
    }
}
