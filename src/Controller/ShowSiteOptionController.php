<?php

namespace App\Controller;

use App\Repository\SiteOptionRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ShowSiteOptionController extends AbstractController
{
    /**
     * @Route("/show/site/option", name="show_site_option")
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     */
    public function __invoke(SiteOptionRepository $optionRepository)
    {

//        * @Security("is_granted('ROLE_ADMINISTRATOR')")
        $currentPrice = $optionRepository->findOneBy(['optionName' => 'parkingPrice'])->getPrice();

        return $this->render('show_site_option/index.html.twig', [
            'current_price' => $currentPrice,
        ]);
    }
}
