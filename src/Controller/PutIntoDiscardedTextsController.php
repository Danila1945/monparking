<?php

namespace App\Controller;

use App\Entity\Text;
use App\Repository\TextRepository;
use App\Utils\UserProvider;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PutIntoDiscardedTextsController extends AbstractController
{
    /**
     * @Route("/put/into/discarded/texts/{textId}", name="put_into_discarded_texts", requirements={"textId" = "\d+"})
     * @Entity("text", expr="repository.find(textId)")
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @throws Exception
     */
    public function __invoke(TextRepository $textRepository, EntityManagerInterface $entityManager,
                             Text $text, UserProvider $userProvider): Response
    {
        $userProvider->checkIsLoggedInUserTexting();

        $userProvider->checkIsLoggedInUserTextAuthor($text);

        if (false === $text->getIsDiscarded()) {
            $text->setIsDiscarded(true);
            $userProvider->recordUsersLogs('Put text into discarded list');
        } else {
            $text->setIsDiscarded(false);
            $userProvider->recordUsersLogs('retrieve text from discarded and put into texts list');
        }

        $entityManager->flush();

        if ($text->getIsDiscarded()) {
            return $this->redirectToRoute('texts_list');
        }

        return $this->redirectToRoute('discarded_texts_list');
    }
}
