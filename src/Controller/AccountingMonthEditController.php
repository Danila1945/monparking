<?php

namespace App\Controller;

use App\ControllerHelpers\SubleasesCalculation;
use App\Entity\User;
use App\Repository\ParkingSubleaseRepository;
use App\Utils\DateTools;
use App\Utils\UserProvider;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountingMonthEditController extends AbstractController
{
    /**
     * @Route("/accounting/month/edit/{month}/{contractorId}/", name="accounting_month_edit",
     *     requirements={"month" = "\d{4}-\d{1,2}", "contractorId" = "\d+"})
     * @Security("is_granted('ROLE_RENTER')")
     * @Entity("contractor", expr="repository.find(contractorId)")
     *
     * @throws Exception
     */
    public function __invoke(string $month, User $contractor,
                             ParkingSubleaseRepository $parkingSubleaseRepository,
                             UserProvider $userProvider): Response
    {
        $userProvider->recordUsersLogs('Détails des prêts');

        $subleases =
            $parkingSubleaseRepository->findTabIsSubleaseTakenByRenterAndByTakerBetweenTwoDates(
                $userProvider->getLoggedInUser(),
                $contractor,
                DateTools::getFirstDateOfAStringMonth($month),
                DateTools::getLastDateOfAStringMonth($month)
            );

        $concernedMonthDate = DateTools::getFirstDateOfAStringMonth($month);

        $calculation = new SubleasesCalculation($subleases, $userProvider->getLoggedInUser());

        return $this->render('accounting_month_edit/index.html.twig', [
            'concerned_month_date' => $concernedMonthDate,
            'Contractor' => $contractor,
            'subleases' => $subleases,
            'isPaidSum' => $calculation->getIsPaidSum(),
            'isNotPaidSum' => $calculation->getIsNotPaidSum(),
            'month' => $month,
            'TotalDueAmountForReceipt' => $calculation->getTotalDueAmountForReceipt(),
        ]);
    }
}
