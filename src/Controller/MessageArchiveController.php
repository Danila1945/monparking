<?php

namespace App\Controller;

use App\Entity\Messages;
use App\Exception\InvalidMessageException;
use App\Utils\MessagesProvider;
use App\Utils\UserProvider;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageArchiveController extends AbstractController
{
    /**
     * @Route("/message/archive/{messageId}", name="message_archive", requirements={"\d+"})
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_RENTER') or is_granted('ROLE_PREVIOUS_ADMIN')")
     * @Entity("message", expr="repository.find(messageId)")
     *
     * @throws InvalidMessageException
     */
    public function __invoke(Messages $message, EntityManagerInterface $entityManager,
                             UserProvider $userProvider, MessagesProvider $messagesProvider): Response
    {
        $messagesProvider->checkIsMessageRead($userProvider->getLoggedInUser(), $message);

        $messagesProvider->setArchivedByReceiverOrTransmitter($userProvider->getLoggedInUser(), $message);

        if ($userProvider->getLoggedInUser() === $message->getReceiver()) {
            return $this->redirectToRoute('message_receive', ['inboxType' => 'inbox']);
        }

        return $this->redirectToRoute('message_sent', ['sentboxType' => 'sent']);
    }
}
