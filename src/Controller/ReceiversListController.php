<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Utils\UserProvider;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReceiversListController extends AbstractController
{
    /**
     * @Route("/receivers/list", name="receivers_list")
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     *
     * @throws Exception
     */
    public function __invoke(UserRepository $userRepository, UserProvider $userProvider): Response
    {
        $userProvider->recordUsersLogs('Choix liste destinataires');

        $administrator = $userRepository->getUsersListPerRole('ROLE_ADMINISTRATOR', $userProvider->getLoggedInUser());
        $renters = $userRepository->getUsersListPerRole('ROLE_RENTER', $userProvider->getLoggedInUser());
        $tenants = $userRepository->getUsersListPerRole('ROLE_TENANT', $userProvider->getLoggedInUser());

        return $this->render('receivers_list/index.html.twig', [
            'Administrator' => $administrator,
            'Renters' => $renters,
            'Tenants' => $tenants,
        ]);
    }
}
