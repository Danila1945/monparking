<?php

namespace App\Controller;

use App\Repository\TextRepository;
use App\Utils\UserProvider;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DiscardedTextsListController extends AbstractController
{
    /**
     * @Route("discarded/texts/list", name="discarded_texts_list")
     *
     * @param TextRepository $textRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     */
    public function __invoke(TextRepository $textRepository, EntityManagerInterface $entityManager,
                             UserProvider $userProvider): Response
    {
        $userProvider->checkIsLoggedInUserTexting();
        $userProvider->recordUsersLogs('discarded texts list');

        return $this->render('discarded_texts_list/index.html.twig', [
            'texts_list' => $textRepository->findAllDiscardedTextsByMostRecentCreationDate($userProvider->getLoggedInUser()),
        ]);
    }
}
