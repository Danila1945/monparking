<?php

namespace App\Controller;

use App\DTO\DTOusernameUpdate;
use App\Entity\User;
use App\Form\UsernameUpdateType;
use App\Repository\UserRepository;
use App\Utils\UserProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UsernameUpdate extends AbstractController
{
    /**
     * @Route("/user/usernameUpdate/{userId}", name="username_update")
     * @Entity("chosenUser", expr="repository.find(userId)")
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     */
    public function __invoke(Request $request, FormFactoryInterface $formFactory, UserRepository $userRepository,
                             User $chosenUser, UserProvider $userProvider): Response
    {
        $userProvider->checkIsLoggedInUserSameAsChosenUser($chosenUser);

        $userProvider->checkIsRenterOrTenantUser($chosenUser);

        $DTOUser = new DTOusernameUpdate();

        $DTOUser->setUsername($chosenUser->getUsername());
        $DTOUser->setPhoneNumber($chosenUser->getPhoneNumber());
        $DTOUser->setPayPalEmail($chosenUser->getPayPalEmail());
        $DTOUser->setRevolutPhoneNumber($chosenUser->getRevolutPhoneNumber());

        $form = $formFactory->create(UsernameUpdateType::class, $DTOUser);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $data = $form->getData();

            $newUsername = $data->getUsername();

            $existingUser = $userRepository->findOneBy(['username' => $newUsername]);

            if (null !== $existingUser && $chosenUser->getUsername() !== $newUsername) {
                $form->addError(new FormError('Nom d\'utilisateur déjà utilisé'));
            }

            if ($form->isValid()) {
                /* @var DTOusernameUpdate $data */
                $newUsername = $data->getUsername();
                $newPhoneNumber = $data->getPhoneNumber();
                $newPayPalEmail = $data->getPayPalEmail();
                $newRevolutPhoneNumber = $data->getRevolutPhoneNumber();
                $chosenUser->changeUsername($newUsername);
                $chosenUser->changePhoneNumber($newPhoneNumber);
                $chosenUser->changePayPalEmail($newPayPalEmail);
                $chosenUser->changeRevolutPhoneNumber($newRevolutPhoneNumber);

                $this->getDoctrine()->getManager()->persist($chosenUser);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('user_consulting', ['userId' => $chosenUser->getId()]);
            }
        }

        return $this->render('UsernameUpdate.twig', [
            'formular' => $form->createView(),
            'ExistingUser' => $chosenUser,
        ]);
    }
}
