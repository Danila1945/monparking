<?php

namespace App\Controller;

use App\Sublease\RentalsAccountingManager;
use App\Utils\UserProvider;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RentalsAccountingController extends AbstractController
{
    /**
     * @throws Exception
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @Route("/accounting/{category}/", name="accounting", requirements={"category" = "[a-z-A-Z]+"})
     */
    public function __invoke(string $category, UserProvider $userProvider,
                             RentalsAccountingManager $rentalsManager): Response
    {
        $relatedContractorsList = null;

        if ($this->isGranted('ROLE_TENANT')) {
            $relatedContractorsList =
                $rentalsManager->getContractorsWithSubleasesForTenant($userProvider->getLoggedInUser(), $category);
        } elseif ($this->isGranted('ROLE_RENTER')) {
            $relatedContractorsList =
                $rentalsManager->getContractorsWithSubleasesForRenter($userProvider->getLoggedInUser(), $category);
        }

        $userProvider->recordUsersLogs('comptabilité - '.$category);

        return $this->render('rentals.twig', [
            'Category' => $category,
            'related_contractors_list' => $relatedContractorsList,
        ]);
    }
}
