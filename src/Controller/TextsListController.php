<?php

namespace App\Controller;

use App\DTO\DTOTextResearchContent;
use App\Form\TextResearchContentType;
use App\Repository\TextRepository;
use App\Utils\UserProvider;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TextsListController extends AbstractController
{
    /**
     * @Route("/texts/list", name="texts_list")
     *
     * @throws Exception
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     */
    public function __invoke(TextRepository $textRepository, EntityManagerInterface $entityManager,
                             UserProvider $userProvider, Request $request, FormFactoryInterface $formFactory): Response
    {
        $userProvider->checkIsLoggedInUserTexting();

        $userProvider->recordUsersLogs('requests list');

        $form = $formFactory->create(TextResearchContentType::class, new DTOTextResearchContent());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            /* @var DTOTextResearchContent $data */
            $data = $form->getData();
            $researchcontent = $data->getTextResearchContent();

            return $this->redirectToRoute('text_search_result', ['researchContent' => $researchcontent]);
        }

        return $this->render('texts_list/index.html.twig', [
            'texts_list' => $textRepository->findAllTextsByMostRecentCreationDate($userProvider->getLoggedInUser()),
            'formular' => $form->createView(),
            'referer' => $request->headers->get('referer', '/'),
        ]);
    }
}
