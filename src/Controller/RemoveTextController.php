<?php

namespace App\Controller;

use App\Entity\Text;
use App\Utils\UserProvider;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RemoveTextController extends AbstractController
{
    /**
     * @Route("/remove/text/{textId}", name="remove_text", requirements={"textId" = "\d+"})
     * @Entity("text", expr="repository.find(textId)")
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     *
     * @throws Exception
     */
    public function __invoke(EntityManagerInterface $entityManager,
                             Text $text, UserProvider $userProvider): Response
    {
        $userProvider->checkIsLoggedInUserTexting();
        $userProvider->checkIsLoggedInUserTextAuthor($text);
        $userProvider->recordUsersLogs('text removed');

        $entityManager->remove($text);
        $entityManager->flush();

        return $this->redirectToRoute('discarded_texts_list');
    }
}
