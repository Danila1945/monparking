<?php

namespace App\Controller;

use App\DTO\DTOText;
use App\Entity\Text;
use App\Form\TextType;
use App\Utils\PageTools;
use App\Utils\UserProvider;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TextCreateController extends AbstractController
{
    /**
     * @Route("/text/create", name="text_create")
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     *
     * @throws Exception
     */
    public function __invoke(Request $request, FormFactoryInterface $formFactory,
                             EntityManagerInterface $entityManager, UserProvider $userProvider): Response
    {
        $userProvider->checkIsLoggedInUserTexting();

        $userProvider->recordUsersLogs('text create');

        $form = $formFactory->create(TextType::class, new DTOText());

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            /* @var Text $data */
            $data = $form->getData();

            if ($form->isValid()) {
                $content = $data->getContent();
                $text = new Text($content, $userProvider->getLoggedInUser());

                $this->getDoctrine()->getManager()->persist($text);
                $entityManager->flush();

                return $this->redirectToRoute('texts_list');
            }
        }

        return $this->render('text_create/index.html.twig', [
            'formular' => $form->createView(),
            'referer' => PageTools::getReferer($request),
        ]);
    }
}
