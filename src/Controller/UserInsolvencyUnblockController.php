<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Utils\UserProvider;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserInsolvencyUnblockController extends AbstractController
{
    /**
     * @Route("/user/insolvency/unblock/{userId}", name="user_insolvency_unblock", requirements={"userId" = "\d+"})
     * @Entity("user", expr="repository.find(userId)")
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     */
    public function __invoke(UserRepository $userRepository, EntityManagerInterface $entityManager,
                             User $user, UserProvider $userProvider): Response
    {
        $userProvider->immunityToggle($user);

        return $this->redirectToRoute('user_consulting', ['userId' => $user->getId()]);
    }
}
