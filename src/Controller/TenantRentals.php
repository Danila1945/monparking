<?php

namespace App\Controller;

use App\Repository\ParkingSubleaseRepository;
use App\Utils\DateTools;
use App\Utils\PageTools;
use App\Utils\UserProvider;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TenantRentals extends AbstractController
{
    /**
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param Request $request
     * @param UserProvider $userProvider
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_TENANT')")
     * @Route("/myTenantRentals/", name="my_tenant_rentals")
     */
    public function __invoke(ParkingSubleaseRepository $parkingSubleaseRepository, Request $request,
                             UserProvider $userProvider): Response
    {

        $loggedInUser = $userProvider->getLoggedInUser();
        $referer = PageTools::getReferer($request);
        $isRefererCalendar = PageTools::isWordAppearing($referer, 'calendar');

        $today = DateTools::getTodayDate();
        $todayPlusTwoMonths = DateTools::getGivenDatePlusTwoMonths($today);

        $currentPeriodSubleasesList = $parkingSubleaseRepository->
        findTabIsSubleaseTakenByTakerBetweenTwoDates($loggedInUser->getId(), $today, $todayPlusTwoMonths);

        return $this->render('TenantRentals.twig', [
            'User' => $loggedInUser,
            'Referer' => $referer,
            'CurrentPeriodSubleasesList' => $currentPeriodSubleasesList,
            'IsRefererCalendar' => $isRefererCalendar,
            'Today' => $today,
        ]);
    }
}
