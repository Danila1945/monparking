<?php

namespace App\Controller;

use App\Entity\ParkingSublease;
use App\Exception\SubleaseAlreadyClosedException;
use App\Sublease\ParkingManager;
use App\Utils\PageTools;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StopsubleaseController extends AbstractController
{
    /**
     * @param ParkingSublease $parkingSublease
     * @param ParkingManager $parkingManager
     * @param Request $request
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_RENTER')")
     * @Route("/stopsublease/{subleaseId}", name="stopsublease", requirements={"subleaseId" = "\d+"})
     * @Entity("parkingSublease", expr="repository.find(subleaseId)")
     */
    public function __invoke(ParkingSublease $parkingSublease, ParkingManager $parkingManager, Request $request): Response
    {
        $referer = PageTools::getReferer($request);
        try {
            $relatedDayDate = $parkingManager->removeOpenedSublease($parkingSublease);
        } catch (SubleaseAlreadyClosedException $e) {
            $this->addFlash('error', 'Prêt déjà souscrit. Annulation impossible !');

            return $this->redirect($referer);
        }

        $this->addFlash('subleaseWithdrawn', 'La procédure de prêt a été correctement retirée.');

        return $this->redirectToRoute('calendar', [
            'intYear' => $relatedDayDate->format('Y'),
            'intMonth' => $relatedDayDate->format('m'),
        ]);
    }
}
