<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TermsOfUserController extends AbstractController
{
    /**
     * @Route("/terms/of/user", name="terms_of_user")
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     */
    public function __invoke(): Response
    {
        return $this->render('terms_of_user/index.html.twig', [
        ]);
    }
}
