<?php

namespace App\Controller;

use App\Entity\ParkingSublease;
use App\Repository\ParkingSubleaseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PaymentConfirmationController extends AbstractController
{
    /**
     * @Route("/payment/confirmation", name="payment_confirmation")
     *
     * @return Response
     */
    public function __invoke(
        Request $request,
        EntityManagerInterface $entityManager,
        ParkingSubleaseRepository $parkingSubleaseRepository
    ) {
        // On est en tant que prêteur, on vient de me payer, et j'ai cliqué sur "On m'a payé ces locations"
        $securityUser = $this->getUser();
        $user = $securityUser->getUser();

        // On récupère les infos de la requête (en POST de préférence)
        $takerId = $request->get('taker');
        $subleasesMonth = $request->get('month');

        // On vérifie qu'il y a bien des locations qui correspondent aux paramètres
        $userSubleases = $parkingSubleaseRepository->getUserSubleases($user, $takerId, $subleasesMonth);
        if (empty($userSubleases)) {
            // Affichage d'une page d'erreur parce que l'utilisateur n'a pas prêté quoique soit...
            throw $this->createAccessDeniedException();
        }

        // On met à jour les locations pour dire qu'elles ont été payées
        foreach ($userSubleases as $sublease) {
            /* @var ParkingSublease $sublease */
            $sublease->markAsPaid();
            $entityManager->persist($sublease);
        }

        $entityManager->flush();

        $this->addFlash('Subleases updated!');

        return $this->redirectToRoute('sublease_list');
    }
}
