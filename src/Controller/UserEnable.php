<?php


namespace App\Controller;

use App\Entity\User;
use App\Utils\UserProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserEnable extends AbstractController
{

    /**
     * @param User $userToBeEnabled
     * @param UserProvider $userProvider
     * @return Response
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @Route("/manager/user/userEnable/{userId}", name="user_enable", requirements={"userId" = "\d+"})
     * @Entity("userToBeEnabled", expr="repository.find(userId)")
     */
    public function __invoke(User $userToBeEnabled, UserProvider $userProvider): Response
    {
        $userProvider->enableToggle($userToBeEnabled);

        return $this->redirectToRoute('user_consulting', ['userId' => $userToBeEnabled->getId()]);
    }
}
