<?php

namespace App\Controller;

use App\Repository\MessagesRepository;
use App\Utils\UserProvider;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageReceiveController extends AbstractController
{
    /**
     * @Route("/message/received", name="message_receive")
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     *
     * @throws Exception
     */
    public function __invoke(MessagesRepository $messagesRepository, UserProvider $userProvider): Response
    {
        $userProvider->recordUsersLogs('Liste messages reçus');

        return $this->render('message_receive/index.html.twig', [
            'ReceivedMessagesList' => $messagesRepository
                ->findBy(['receiver' => $userProvider->getLoggedInUser(), 'isArchivedByReceiver' => false],
                    ['creationDate' => 'DESC']),
        ]);
    }
}
