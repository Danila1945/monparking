<?php

namespace App\Controller;

use App\Entity\Messages;
use App\Utils\MessagesProvider;
use App\Utils\UserProvider;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageOpenController extends AbstractController
{
    /**
     * @Route("/message/open/{messageId}", name="message_open", requirements={"messageID" = "\d+"})
     * @Entity("message", expr="repository.find(messageId)")
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT') or is_granted('ROLE_ADMINISTRATOR')")
     *
     * @param string $inboxType
     *
     * @return Response
     * @throws Exception
     */
    public function __invoke(Messages $message, Request $request, UserProvider $userProvider,
                             MessagesProvider $messagesProvider): Response
    {
        $url = $this->generateUrl('message_receive');

        $messagesProvider->checkLoggedInUserIsReceiver($userProvider->getLoggedInUser(), $message);

        $messagesProvider->receiptAcknowledgement($message);

        return $this->render('message_open/index.html.twig', [
            'message' => $message,
            'url' => $url,
        ]);
    }
}
