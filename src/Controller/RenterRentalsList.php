<?php


namespace App\Controller;


use App\Handlers\RentalsListHandler;
use App\Repository\ParkingSubleaseRepository;
use App\Repository\UserRepository;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RenterRentalsList extends AbstractController
{

    /**
     * @param Request $request
     * @param RentalsListHandler $rentalsListHandler
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_RENTER')")
     * @Route("/myRenterRentalsList/", name="my_renter_rentals_list")
     */
    public function __invoke(Request $request, RentalsListHandler $rentalsListHandler): Response
    {
        $securityUser = $this->getUser();
        $user = $securityUser->getUser();

        $subleasesList = $rentalsListHandler->getUserOrderedRentalsList($user);

        return $this->render('tenantDueRentalsSummary.twig', [
            'subleases_info' => $subleasesList,
        ]);
    }

}
