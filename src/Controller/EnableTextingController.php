<?php

namespace App\Controller;

use App\Entity\User;
use App\Utils\UserProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EnableTextingController extends AbstractController
{
    /**
     * @Route("/enable/employee/texting/{userId}", name="enable_employee_texting",
     *     requirements={"userId" = "\d+"})
     * @Entity("chosenUser", expr="repository.find(userId)")
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @param User $chosenUser
     * @param UserProvider $userProvider
     * @return Response
     */
    public function __invoke(User $chosenUser, UserProvider $userProvider): Response
    {
        $userProvider->setUserTextingToggle($chosenUser);

        return $this->redirectToRoute('user_consulting', ['userId' => $chosenUser->getId()]);
    }
}
