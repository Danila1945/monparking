<?php

namespace App\Controller;

use App\Entity\Text;
use App\Utils\UserProvider;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TextProcessController extends AbstractController
{
    /**
     * @Route("/text/process/{textId}", name="text_process")
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @Entity("text", expr="repository.find(textId)")
     *
     * @throws Exception
     */
    public function __invoke(UserProvider $userProvider, Text $text): Response
    {
        $userProvider->checkIsLoggedInUserTexting();

        $userProvider->checkIsLoggedInUserTextAuthor($text);

        $userProvider->recordUsersLogs('text processing');

        return $this->render('text_process/index.html.twig', [
            'text' => $text,
            'texts_list_page' => $this->generateUrl('texts_list'),
        ]);
    }
}
