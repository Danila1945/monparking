<?php

namespace App\Controller;

use App\DTO\DTOTextResearchContent;
use App\DTO\Tutorial\DTOResearchContent;
use App\Form\TextResearchContentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TextResearchController extends AbstractController
{
    /**
     * @Route("/text/research", name="text_research")
     */
    public function __invoke(Request $request, FormFactoryInterface $formFactory): Response
    {

        $form = $formFactory->create(TextResearchContentType::class, new DTOTextResearchContent());

//        $form->handleRequest($request->query->get('MyQueryParamName', 'myDefaultStringValue'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $researchContent = $data->getResearchContent();

            return $this->redirectToRoute('text_search_result', ['researchContent' => $researchContent]);

        }

        return $this->redirectToRoute('texts_list');

    }
}
