<?php

namespace App\Controller;

use App\Repository\MessagesRepository;
use App\Utils\UserProvider;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageSentController extends AbstractController
{
    /**
     * @Route("/message/sent/{sentboxType}", name="message_sent", requirements={"sentboxType" = "[a-z-A-Z]+"})
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     *
     * @throws Exception
     */
    public function __invoke(MessagesRepository $messagesRepository, UserProvider $userProvider): Response
    {
        $userProvider->recordUsersLogs('Liste messages envoyés');

        return $this->render('message_sent/index.html.twig', [
            'TransmittedMessagesList' => $transmittedMessagesList = $messagesRepository
                ->findBy(['transmitter' => $userProvider->getLoggedInUser(), 'isArchivedByTransmitter' => false],
                    ['creationDate' => 'DESC']),
        ]);
    }
}
