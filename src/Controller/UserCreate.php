<?php

namespace App\Controller;

use App\DTO\DTOUserRenter;
use App\DTO\DTOUserTenant;
use App\Entity\User;
use App\Form\RenterUserType;
use App\Form\TenantUserType;
use App\Repository\UserRepository;
use App\Utils\UserProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserCreate extends AbstractController
{
    /**
     * @Route("/user/create/{role}", name="user_create")
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     */
    public function __invoke(Request $request, FormFactoryInterface $formFactory, UserRepository $userRepository,
                             string $role, UserProvider $userProvider): Response
    {
        if ('ROLE_TENANT' === $role) {
            $form = $formFactory->create(TenantUserType::class, new DTOUserTenant());
        } elseif ('ROLE_RENTER' === $role) {
            $form = $formFactory->create(RenterUserType::class, new DTOUserRenter());
        }

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            /**
             * @var User
             */
            $data = $form->getData();

            $chosenUsername = $data->getUsername();

            $userProvider->checkIfExistingUser($chosenUsername, $form);

            if ('ROLE_RENTER' === $role) {
                $parkingNumber = $data->getParkingNumber();
                $userProvider->checkIfAlreadyExistingParkingNumber($parkingNumber, $form);
            }

            if ($form->isValid()) {
                $username = $data->getUsername();
                $password = $data->getPassword();
                $lastName = $data->getLastName();
                $firstName = $data->getFirstName();

                if ('ROLE_RENTER' === $role) {
                    $parkingNumber = $data->getParkingNumber();
                } else {
                    $parkingNumber = null;
                }

                $user = new User($username, $password, $lastName, $firstName, $role);

                if ('ROLE_RENTER' === $role) {
                    $user->setParkingNumber($parkingNumber);
                }

                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('users_list');
            }
        }

        return $this->render('UserCreate.twig', [
            'formular' => $form->createView(),
            'Role' => $role,
        ]);
    }
}
