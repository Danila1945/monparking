<?php

namespace App\Controller;

use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Repository\ParkingSubleaseRepository;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ParkingSubleaseMarkAsPaidOrUnpaidController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_RENTER')")
     * @Route("/parking/sublease/mark/as/paid/unpaid/{parkingSubleaseId}", name="parking_sublease_mark_as_paid_unpaid",
     *     requirements={"parkingSubleaseId" = "\d+"})
     * @Entity("parkingSublease", expr="repository.find(parkingSubleaseId)")
     */
    public function __invoke(ParkingSubleaseRepository $parkingSubleaseRepository, ParkingSublease $parkingSublease,
                             Request $request, EntityManagerInterface $entityManager): Response
    {
        $referer = $request->headers->get('referer', '/');

        if ((!$this->isGranted('ROLE_PREVIOUS_ADMIN')) && (!$this->isGranted('ROLE_RENTER'))) {
            throw new LogicException('User not allowed');
        }

        if ($this->isGranted('ROLE_RENTER')) {
            $loggedInRenterUser = $this->getUser()->getUser();
            if ($parkingSublease->getUser() !== $loggedInRenterUser) {
                throw new LogicException('Logged-in renter user not allowed to make this operation');
            }
        }

        if (0.0 === $parkingSublease->getPrice()) {
            throw new LogicException('Free parking sublease can\'t be accounted');
        }

        if ($parkingSublease->getIsPaid()) {
            /* @var User $loggedInRenterUser */
            $loggedInRenterUser = $this->getUser()->getUser();

            if (!$loggedInRenterUser->getIsUserEnabled() && !$this->isGranted('ROLE_PREVIOUS_ADMIN')) {
                throw new LogicException('User is disabled');
            }

            $parkingSublease->markAsNotPaid();
        } else {
            $parkingSublease->markAsPaid();
        }

        $entityManager->flush();

        return $this->redirect($referer);
    }
}
