<?php

namespace App\Controller;

use App\DTO\DTOMessages;
use App\Entity\Messages;
use App\Entity\User;
use App\Form\MessagesType;
use App\Repository\UserRepository;
use App\Utils\MessagesProvider;
use App\Utils\UserProvider;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageCreateController extends AbstractController
{
    /**
     * @Route("/message/create/{receiverId}", name="message_create", requirements={"receiverId" = "\d+"})
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @Entity("receiver", expr="repository.find(receiverId)")
     *
     * @param UserRepository $userRepository
     *
     * @return Response
     * @throws Exception
     */
    public function __invoke(FormFactoryInterface $formFactory, Request $request, User $receiver,
                             UserProvider $userProvider, EntityManagerInterface $entityManager,
                             MessagesProvider $messagesProvider): Response
    {

        $userProvider->recordUsersLogs('Création d\'un message');

        $messagesProvider->checkIsLoggedInUserSendingMessageToHimself($userProvider->getLoggedInUser(), $receiver);

        $referer = $request->headers->get('referer', '/');

        $form = $formFactory->create(MessagesType::class, new DTOMessages());

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            /* @var Messages $data */
            $data = $form->getData();

            if ($form->isValid()) {
                $text = $data->getMessage();
                $message = new Messages($userProvider->getLoggedInUser(), $text, $receiver);

                $this->getDoctrine()->getManager()->persist($message);
                $entityManager->flush();

                return $this->redirectToRoute('message_sent', ['sentboxType' => 'sent']);
            }
        }

        return $this->render('message_create/index.html.twig', [
            'Receiver' => $receiver,
            'Referer' => $referer,
            'formular' => $form->createView(),
        ]);
    }
}
