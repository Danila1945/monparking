<?php

namespace App\Controller;

use App\Entity\UsersLogsEvents;
use App\Repository\MessagesRepository;
use App\Repository\ParkingSubleaseRepository;
use App\Repository\UsersLogsEventsRepository;
use App\Sublease\ParkingManager;
use App\Utils\DateTools;
use App\Utils\UserProvider;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NavBarCounterController extends AbstractController
{
    /**
     * @Route("/nav/bar/counter", name="nav_bar_counter")
     *
     * @throws Exception
     */
    public function layout(ParkingManager $parkingManager, UsersLogsEventsRepository $eventsRepository,
                           EntityManagerInterface $entityManager, MessagesRepository $messagesRepository,
                           UserProvider $userProvider, ParkingSubleaseRepository $parkingSubleaseRepository): Response
    {
        $numberOfUnreadMessages = 0;
        $receivableTotalAmount = 0.0;
        $receivedTotalAmount = 0.0;
        $receivableAndReceivedTotalAmount = 0.0;
        $dueTotalAmount = 0.0;
        $dueAmountOnNextDueDate = 0.0;
        $currentMonthDueDate = null;
        $previousMonthString = null;

        if (null !== $this->getUser()) {

            $numberOfUnreadMessages = $messagesRepository
                ->findReceivedAndNotAcknowledgedMessages($userProvider->getLoggedInUser());

            if ($this->isGranted('ROLE_RENTER')) {
                $parkingManager->computeRenterAmounts(
                    $receivableTotalAmount,
                    $receivedTotalAmount,
                    $receivableAndReceivedTotalAmount);
            }

            if ($this->isGranted('ROLE_TENANT')) {
                $dueTotalAmount = $parkingSubleaseRepository
                    ->findMoneyDueSubleasesPerTakerSum($userProvider->getLoggedInUser());
                $dueAmountOnNextDueDate = $parkingManager->getDueAmountOnNextDueDate();

                $previousMonthString = DateTools::getPreviousMonthString();
                $currentMonthDueDate = DateTools::getCurrentMonthDueDate();
            }

            if (!$this->isGranted('ROLE_ADMINISTRATOR') && !$this->isGranted('ROLE_PREVIOUS_ADMIN')) {

                $logsToBeDeletedCount = null;
                $allLogsCount = count($eventsRepository->findAll());

                if ($allLogsCount > 500) {
                    $logsToBeDeletedCount = $allLogsCount - 500;

                    /* @var UsersLogsEvents[] $logsToBeDeleted */
                    $logsToBeDeleted = $eventsRepository->findTabOldestLogs($logsToBeDeletedCount);

                    foreach ($logsToBeDeleted as $log) {
                        $entityManager->remove($log);
                    }
                    $entityManager->flush();
                }
            }
        }

        return $this->render('Layout.twig', [
            'numberOfUnreadMessages' => $numberOfUnreadMessages,
            'receivableTotalAmount' => $receivableTotalAmount,
            'receivedTotalAmount' => $receivedTotalAmount,
            'receivableAndReceivedTotalAmount' => $receivableAndReceivedTotalAmount,
            'dueAmountOnNextDueDate' => $dueAmountOnNextDueDate,
            'currentMonthDueDate' => $currentMonthDueDate,
            'dueTotalAmount' => $dueTotalAmount,
            'previousMonthString' => $previousMonthString,
        ]);
    }
}
