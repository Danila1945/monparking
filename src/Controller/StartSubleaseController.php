<?php

namespace App\Controller;

use App\Sublease\ParkingManager;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class StartSubleaseController extends AbstractController
{

    /**
     * @param string $dateString
     * @param ParkingManager $parkingManager
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_RENTER')")
     * @Route("/startsublease/{dateString}", name="startsublease", requirements={"dateString" = "\d{4}\-\d{2}\-\d{2}"})
     */
    public function __invoke(string $dateString, ParkingManager $parkingManager): Response
    {
        $parkingSublease = $parkingManager->createOpenedSublease($dateString);
        $this->addFlash('success', 'Proposition de prêt enregistrée avec succès !');

        return $this->redirectToRoute('calendar', [
            'intYear' => $parkingSublease->getDayDate()->format('Y'),
            'intMonth' => $parkingSublease->getDayDate()->format('m'),
        ]);
    }
}
