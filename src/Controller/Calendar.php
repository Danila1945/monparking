<?php

namespace App\Controller;

use App\Calendar\CalendarManager;
use App\ControllerHelpers\CalendarData;
use App\Repository\ParkingSubleaseRepository;
use App\Sublease\MoneyManager;
use App\Utils\DateTools;
use App\Utils\UserProvider;
use DateTimeImmutable;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Calendar extends AbstractController
{
    /**
     * @throws Exception
     * @Route("/calendar/{intYear}/{intMonth}", name="calendar", requirements={"year" = "\d{4}", "month" = "\d{1,2}"})
     */
    public function __invoke(ParkingSubleaseRepository $parkingSubleaseRepository, int $intYear, int $intMonth,
                             MoneyManager $moneyManager, UserProvider $userProvider,
                             CalendarManager $calendarManager): Response
    {

        $calendarManager->checkMonthIsBetween1And12($intMonth);

        if ($this->isGranted('ROLE_RENTER') && !$this->isGranted('ROLE_ADMINISTRATOR')) {
            return $this->render('CalendarConsulting.twig', [
                'calendar' => new CalendarData($intYear, $intMonth),
                'current_working_year_int' => (int) DateTools::getTodayDate()->format('Y'),
                'current_working_year_plus_one_year_int' => (int)
                DateTools::getGivenDatePlusOneYear(DateTools::getTodayDate())->format('m'),
                'current_working_month_int' => (int) DateTools::getTodayDate()->format('m'),
                'current_working_month_plus_one_month_int' => (int)
                DateTools::getGivenDatePlusOneMonth(DateTools::getTodayDate())->format('m'),
                'current_working_month_plus_two_month_int' => (int)
                DateTools::getGivenDatePlusTwoMonths(DateTools::getTodayDate())->format('m'),
                'all_month_day_dates' => $calendarManager->getAllDatesOfAMonth($intYear, $intMonth),
                'ChosenYearMonth' => new DateTimeImmutable($intYear.'-'.$intMonth),
                'OpenToSubleaseList' => $calendarManager->getAllOpenedDayDatesTab((string) $intYear, (string) $intMonth),
                'price' => $moneyManager->getPrice(),
                'renter_taken_subleases_in_current_month' => $calendarManager
                    ->getTakenSubleasesInCurrentMonthByRenter((string) $intYear, (string) $intMonth),
                'renter_open_subleases_in_current_month' => $calendarManager
                    ->getOpenSubleasesInCurrentMonthByRenter((string) $intYear, (string) $intMonth),
                'SelectedYearInt' => $intYear,
                'SelectedMonthInt' => $intMonth,
                'TodayPlusTwoMonths' => DateTools::getGivenDatePlusTwoMonths(DateTools::getTodayDate()),
            ]);
        }

        if ($this->isGranted('ROLE_TENANT') && !$this->isGranted('ROLE_ADMINISTRATOR')) {
            return $this->render('CalendarConsulting.twig', [
                'calendar' => new CalendarData($intYear, $intMonth),
                'current_working_year_int' => (int) DateTools::getTodayDate()->format('Y'),
                'current_working_year_plus_one_year_int' => (int)
                DateTools::getGivenDatePlusOneYear(DateTools::getTodayDate())->format('m'),
                'current_working_month_int' => (int) DateTools::getTodayDate()->format('m'),
                'current_working_month_plus_one_month_int' => (int)
                DateTools::getGivenDatePlusOneMonth(DateTools::getTodayDate())->format('m'),
                'current_working_month_plus_two_month_int' => (int)
                DateTools::getGivenDatePlusTwoMonths(DateTools::getTodayDate())->format('m'),
                'all_month_day_dates' => $calendarManager->getAllDatesOfAMonth($intYear, $intMonth),
                'ChosenYearMonth' => new DateTimeImmutable($intYear.'-'.$intMonth),
                'CurrentMonthTotalOpenToSublease' => $parkingSubleaseRepository
                    ->countPossibleSubleasesByYearMonth((string) $intYear, (string) $intMonth),
                'CurrentMonthTotalOpenToSubleaseForTenant' => $calendarManager
                    ->getTenantPossibleSubleasesQuantity((string) $intYear, (string) $intMonth),
                'IsExistingTakenSubleaseByDayDateAndByTakerList' => $calendarManager
                    ->getTenantTakenSubleasesIndexedByDatesTab((string) $intYear, (string) $intMonth),
                'OpenToSubleaseList' => $calendarManager->getAllOpenedDayDatesTab((string) $intYear, (string) $intMonth),
                'ParkingSubleaseRepository' => $parkingSubleaseRepository,
                'SelectedYearInt' => $intYear,
                'SelectedMonthInt' => $intMonth,
                'tenant_possible_subleases_dates_tab' => $calendarManager
                    ->getTenantPossibleSubleasesDatesTab((string) $intYear, (string) $intMonth),
            ]);
        }

        if ($this->isGranted('ROLE_ADMINISTRATOR')) {
            return $this->render('CalendarConsulting.twig', [
                'calendar' => new CalendarData($intYear, $intMonth),
                'current_working_year_int' => (int) DateTools::getTodayDate()->format('Y'),
                'current_working_year_plus_one_year_int' => (int)
                DateTools::getGivenDatePlusOneYear(DateTools::getTodayDate())->format('m'),
                'current_working_month_int' => (int) DateTools::getTodayDate()->format('m'),
                'current_working_month_plus_one_month_int' => (int)
                DateTools::getGivenDatePlusOneMonth(DateTools::getTodayDate())->format('m'),
                'current_working_month_plus_two_month_int' => (int)
                DateTools::getGivenDatePlusTwoMonths(DateTools::getTodayDate())->format('m'),
                'all_month_day_dates' => $calendarManager->getAllDatesOfAMonth($intYear, $intMonth),
                'ChosenYearMonth' => new DateTimeImmutable($intYear.'-'.$intMonth),
                'CurrentMonthTotalOpenToSublease' => $parkingSubleaseRepository
                    ->countPossibleSubleasesByYearMonth((string) $intYear, (string) $intMonth),
                'OpenToSubleaseList' => $calendarManager->getAllOpenedDayDatesTab((string) $intYear, (string) $intMonth),
                'ParkingSubleaseRepository' => $parkingSubleaseRepository,
                'SelectedYearInt' => $intYear,
                'SelectedMonthInt' => $intMonth,
                'TakenDayDatesOfCurrentMonth' => $calendarManager
                    ->getAllTakendDayDatesTab((string) $intYear, (string) $intMonth),
                'TakenOrOpenedSubleaseList' => $calendarManager
                    ->getAllOpenAndTakenSubleasesInChosenMonth((string) $intYear, (string) $intMonth),
                'TodayPlusTwoMonths' => DateTools::getGivenDatePlusTwoMonths(DateTools::getTodayDate()),
            ]);
        }

        return $this->render('CalendarConsulting.twig', [
            'calendar' => new CalendarData($intYear, $intMonth),
            'current_working_year_int' => (int) DateTools::getTodayDate()->format('Y'),
            'current_working_year_plus_one_year_int' => (int)
            DateTools::getGivenDatePlusOneYear(DateTools::getTodayDate())->format('m'),
            'current_working_month_int' => (int) DateTools::getTodayDate()->format('m'),
            'current_working_month_plus_one_month_int' => (int)
            DateTools::getGivenDatePlusOneMonth(DateTools::getTodayDate())->format('m'),
            'current_working_month_plus_two_month_int' => (int)
            DateTools::getGivenDatePlusTwoMonths(DateTools::getTodayDate())->format('m'),
            'all_month_day_dates' => $calendarManager->getAllDatesOfAMonth($intYear, $intMonth),
            'ChosenYearMonth' => new DateTimeImmutable($intYear.'-'.$intMonth),
            'CurrentMonthTotalOpenToSublease' => $parkingSubleaseRepository
                ->countPossibleSubleasesByYearMonth((string) $intYear, (string) $intMonth),
            'OpenToSubleaseList' => $calendarManager->getAllOpenedDayDatesTab((string) $intYear, (string) $intMonth),
            'SelectedMonthInt' => $intMonth,
            'SelectedYearInt' => $intYear,
            'TodayPlusTwoMonths' => DateTools::getGivenDatePlusTwoMonths(DateTools::getTodayDate()),
        ]);
    }

    /**
     * @Route("/", name="calendar_redirect")
     */
    public function redirectToCalendar()
    {
        $intYear = date('Y');
        $intMonth = date('m');

        return $this->redirectToRoute('calendar', [
            'intYear' => $intYear,
            'intMonth' => $intMonth,
        ]);
    }
}
