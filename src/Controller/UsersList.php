<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UsersList extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @Route("/usersList/", name="users_list")
     */
    public function __invoke(UserRepository $userRepository, Request $request): Response
    {
        $userList = $userRepository->getUsersList();

        return $this->render('UsersList.twig', [
            'UserList' => $userList,
        ]);
    }
}
