<?php

namespace App\Controller;

use App\Entity\User;
use App\Utils\UserProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SetFreeRenterController extends AbstractController
{
    /**
     * @Route("/set/free/renter/{userId}", name="set_free_renter", requirements={"userId" = "\d+"})
     * @Entity("user", expr="repository.find(userId)")
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @param User $user
     * @param UserProvider $userProvider
     * @return Response
     */
    public function __invoke(User $user, UserProvider $userProvider): Response
    {
        $userProvider->setFreeRenterToggle($user);

        return $this->redirectToRoute('user_consulting', ['userId' => $user->getId()]);
    }
}
