<?php

namespace App\Controller;

use App\Entity\Messages;
use App\Exception\InvalidMessageException;
use App\Utils\MessagesProvider;
use App\Utils\UserProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageRemoveController extends AbstractController
{
    /**
     * @Route("/message/remove/{messageId}", name="message_remove", requirements={"\d+"})
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @Entity("message", expr="repository.find(messageId)")
     *
     * @throws InvalidMessageException
     */
    public function __invoke(Messages $message, Request $request, UserProvider $userProvider,
                             MessagesProvider $messagesProvider): Response
    {
        $referer = $request->headers->get('referer', '/');

        $messagesProvider->messageRemove($userProvider->getLoggedInUser(), $message);

        $isRefererMessageOpen = true == strpos($referer, 'open');

        if ($isRefererMessageOpen) {
            return $this->redirectToRoute('message_receive');
        } else {
            return $this->redirect($referer);
        }
    }
}
