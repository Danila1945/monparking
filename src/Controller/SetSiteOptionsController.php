<?php

namespace App\Controller;

use App\DTO\DTOSiteOption;
use App\Form\SiteOptionType;
use App\Repository\SiteOptionRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SetSiteOptionsController extends AbstractController
{
    /**
     * @Route("/set/site/options/", name="set_site_options")
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     */
    public function __invoke(Request $request, FormFactoryInterface $formFactory,
                             SiteOptionRepository $optionRepository): Response
    {
        $DTOSiteOption = new DTOSiteOption();
        $existingOption = $optionRepository->findOneBy(['optionName' => 'parkingPrice']);

        $DTOSiteOption->setPrice($existingOption->getPrice());

        $form = $formFactory->create(SiteOptionType::class, $DTOSiteOption);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            /* @var DTOSiteOption $data */
            $data = $form->getData();

            if ($form->isValid()) {
                $newPrice = $data->getPrice();

                if ($existingOption->getPrice() !== $newPrice) {
                    $existingOption->setPrice($newPrice);
                    $this->getDoctrine()->getManager()->flush();

                    return $this->redirectToRoute('show_site_option');
                }

                return $this->redirectToRoute('show_site_option');
            }
        }

        return $this->render('set_site_options/index.html.twig', [
            'formular' => $form->createView(),
        ]);
    }
}
