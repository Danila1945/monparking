<?php

namespace App\Controller;

use App\DTO\DTOTextUpdate;
use App\Entity\Text;
use App\Form\TextUpdateType;
use App\Utils\UserProvider;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TextUpdateController extends AbstractController
{
    /**
     * @Route("/text/update/{textId}", name="text_update")
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @Entity("text", expr="repository.find(textId)")
     */
    public function __invoke(Text $text, FormFactoryInterface $formFactory, Request $request,
                             EntityManagerInterface $entityManager, UserProvider $userProvider): Response
    {
        $userProvider->checkIsLoggedInUserTexting();

        $DTOTextUpdate = new DTOTextUpdate();

        $DTOTextUpdate->setContent($text->getContent());

        $form = $formFactory->create(TextUpdateType::class, $DTOTextUpdate);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            /* @var Text $data */
            $data = $form->getData();

            if ($form->isValid()) {
                $newRequest = $data->getContent();
                $text->setContent($newRequest);

                $entityManager->flush();

                return $this->redirectToRoute('text_process', ['textId' => $text->getId()]);
            }
        }

        return $this->render('text_update/index.html.twig', [
            'text' => $text,
            'formular' => $form->createView(),
        ]);
    }
}
