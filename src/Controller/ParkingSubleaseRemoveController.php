<?php

namespace App\Controller;

use App\Entity\ParkingSublease;
use App\Exception\InvalidSubleaseException;
use App\Sublease\ParkingManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ParkingSubleaseRemoveController extends AbstractController
{
    /**
     * @Route("/parking/sublease/remove/{parkingSubleaseId}", name="parking_sublease_remove",
     *     requirements={"parkingSubleaseId" = "\d+"})
     * @Entity("parkingSublease", expr="repository.find(parkingSubleaseId)")
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @throws InvalidSubleaseException
     */
    public function __invoke(ParkingSublease $parkingSublease, ParkingManager $parkingManager): Response
    {
        $parkingSublease = $parkingManager->unTakeSublease($parkingSublease);

        return $this->redirectToRoute('calendar', [
            'intYear' => $parkingSublease->getDayDate()->format('Y'),
            'intMonth' => $parkingSublease->getDayDate()->format('m'),
        ]);
    }
}
