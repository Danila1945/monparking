<?php

namespace App\Controller;

use App\Entity\ParkingSublease;
use App\Sublease\ParkingManager;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class TakeSubleaseController extends AbstractController
{
    const WITHPRICE = true;

    /**
     * @param ParkingSublease $parkingSublease
     * @param ParkingManager $parkingManager
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_TENANT')")
     * @Route("/daytake/{parkingId}", name="day_take", requirements={"parkingId" = "\d+"})
     * @Entity("parkingSublease", expr="repository.find(parkingId)")
     */
    public function __invoke(ParkingSublease $parkingSublease, ParkingManager $parkingManager): Response
    {
        $subleaseToBeTaken = $parkingManager->takeSublease($parkingSublease);
//        dd($subleaseToBeTaken);

        $this->addFlash('success', 'Emprunt enregistré avec succès !');

        return $this->redirectToRoute(
            'calendar', [
            'intYear' => $subleaseToBeTaken->getDayDate()->format('Y'),
            'intMonth' => $subleaseToBeTaken->getDayDate()->format('m'),
        ]);
    }
}
