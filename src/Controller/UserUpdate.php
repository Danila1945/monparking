<?php

namespace App\Controller;

use App\DTO\DTOUserRenterUpdate;
use App\DTO\DTOUserTenantUpdate;
use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Form\RenterUserUpdateType;
use App\Form\TenantUserUpdateType;
use App\Repository\ParkingSubleaseRepository;
use App\Repository\UserRepository;
use App\Utils\PageTools;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserUpdate extends AbstractController
{
    /**
     * @Route("/user/update/{userId}", name="user_update")
     * @Entity("chosenUser", expr="repository.find(userId)")
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @throws \Exception
     */
    public function __invoke(Request $request, FormFactoryInterface $formFactory, UserRepository $userRepository,
                             User $chosenUser, ParkingSubleaseRepository $parkingSubleaseRepository): Response
    {
        $referer = PageTools::getReferer($request);

        if ('ROLE_RENTER' === $chosenUser->getRole()) {
            $DTOUser = new DTOUserRenterUpdate();
        } else {
            $DTOUser = new DTOUserTenantUpdate();
        }

        $DTOUser->setUsername($chosenUser->getUsername());
        $DTOUser->setLastName($chosenUser->getLastName());
        $DTOUser->setFirstName($chosenUser->getFirstName());

        if ('ROLE_RENTER' === $chosenUser->getRole()) {
            $DTOUser->setParkingNumber($chosenUser->getParkingNumber());
        }

        if ('ROLE_RENTER' === $chosenUser->getRole()) {
            $form = $formFactory->create(RenterUserUpdateType::class, $DTOUser);
            $form->handleRequest($request);
        } else {
            $form = $formFactory->create(TenantUserUpdateType::class, $DTOUser);
            $form->handleRequest($request);
        }

        if ($form->isSubmitted()) {
            $data = $form->getData();
            $newUsername = $data->getUsername();
            $existingUser = $userRepository->findOneBy(['username' => $newUsername]);

            if (null !== $existingUser && $chosenUser->getUsername() !== $newUsername) {
                $form->addError(new FormError('Nom d\'utilisateur déjà utilisé'));
            }

            if ('ROLE_RENTER' === $chosenUser->getRole()) {
                $newParkingNumber = $data->getParkingNumber();
                $existingParkingNumberTab = $userRepository->findTabUsersByParkingNumber($newParkingNumber);

                if (!(empty($existingParkingNumberTab)) && $chosenUser->getParkingNumber() !== $newParkingNumber) {
                    $form->addError(new FormError('No de parking appartenant déjà à un autre prêteur'));
                }
            }

            if ($form->isValid()) {
                $newUsername = $data->getUsername();
                $newLastName = $data->getLastName();
                $newFirstName = $data->getFirstName();

                if ('ROLE_RENTER' === $chosenUser->getRole()) {
                    $newParkingNumber = $data->getParkingNumber();
                } else {
                    $newParkingNumber = null;
                }

                $chosenUser->changeUsername($newUsername);

                $chosenUser->changeLastName($newLastName);
                $chosenUser->changeFirstName($newFirstName);

                if ('ROLE_RENTER' === $chosenUser->getRole()) {
                    /* @var ParkingSublease[] $currentSubleasesByParkingNumberTab */
                    $currentSubleasesByParkingNumberTab
                        = $parkingSubleaseRepository->findTabByParkingSubleaseNumber($chosenUser->getParkingNumber());

                    foreach ($currentSubleasesByParkingNumberTab as $sublease) {
                        $sublease->setSubleaseParkingNumber($newParkingNumber);
                    }

                    $chosenUser->changeParkingNumber($newParkingNumber);
                }

                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('user_consulting', ['userId' => $chosenUser->getId()]);
            }
        }

        if ('ROLE_RENTER' === $chosenUser->getRole()) {
            return $this->render('UserRenterUpdate.twig', [
                'formular' => $form->createView(),
                'ExistingUser' => $chosenUser,
                'Referer' => $referer,
            ]);
        } else {
            return $this->render('UserTenantUpdate.twig', [
                'formular' => $form->createView(),
                'ExistingUser' => $chosenUser,
                'Referer' => $referer,
            ]);
        }
    }
}
