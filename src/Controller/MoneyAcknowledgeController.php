<?php

namespace App\Controller;

use App\Entity\User;
use App\Sublease\MoneyManager;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MoneyAcknowledgeController extends AbstractController
{
    /**
     * @Route("/money/acknowledge/{contractorId}/{month}/", name="money_acknowledge",
     *     requirements={"contractorId" = "\d+", "month" = "\d{4}\-\d{2}"})
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_PREVIOUS_ADMIN')")
     * @Entity("contractor", expr="repository.find(contractorId)")
     *
     * @throws Exception
     */
    public function __invoke(User $contractor, string $month, MoneyManager $moneyManager): Response
    {
        $moneyManager->moneyAcknowledge($contractor, $month);

        return $this->redirectToRoute('accounting_month_edit', [
            'month' => $month,
            'contractorId' => $contractor->getId(),
        ]);
    }
}
