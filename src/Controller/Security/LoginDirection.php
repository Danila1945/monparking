<?php


namespace App\Controller\Security;


use App\ControllerHelpers\Security\SecurityUser;
use App\Entity\UsersLogsEvents;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;


class LoginDirection extends AbstractController
{

    /**
     * @param Session $session
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse
     * @throws Exception
     * @Route("/logindirection", name="logindirection")
     */
    public function onLoginSuccess(Session $session, EntityManagerInterface $entityManager): Response
    {
        $page = $session->remove('previous');

        $loginPage = substr($page, -1);

        /* @var SecurityUser $securityUser */
        $securityUser = $this->getUser();
        $logedInUser = $securityUser->getUser();

        if ($logedInUser->getUsername() !== 'admin') {
            $loginEvent = new UsersLogsEvents($logedInUser,
                new DateTimeImmutable('', new DateTimeZone('Europe/Paris')), 'Login');

            $entityManager->persist($loginEvent);
            $entityManager->flush();
        }

        if ($loginPage === '/login' || $page === null){
            return $this->redirectToRoute('calendar_redirect');

        } else {
            return new RedirectResponse($page);
        }
    }
}
