<?php

namespace App\Controller;

use App\Repository\TextRepository;
use App\Utils\UserProvider;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TextSearchResultController extends AbstractController
{
    /**
     * @Route("/text/search/result", name="text_search_result")
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @throws Exception
     */
    public function __invoke(TextRepository $requestsRepository, UserProvider $userProvider, Request $request)
    {
        $userProvider->checkIsLoggedInUserTexting();

        $userProvider->recordUsersLogs('get result research list');

        $researchContent = $request->query->get('researchContent', 'null');

        $textsList = $requestsRepository->findByTextContent($researchContent, $userProvider->getLoggedInUser());

        if (null != $textsList) {
            return $this->render('text_search_result/index.html.twig', [
                'texts_list' => $textsList,
                'texts_list_page' => $this->generateUrl('texts_list'),
                'research_content' => $researchContent,
            ]);
        } else {
            $this->addFlash(
                'notice',
                'Aucun texte ne correspond à la recherche saisie !'
            );

            return $this->redirectToRoute('texts_list');
        }
    }
}
