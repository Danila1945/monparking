<?php

namespace App\Controller;

use App\Entity\ParkingSublease;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ParkingRemoveConfirmationController extends AbstractController
{
    /**
     * @Route("/parking/remove/confirmation/{parkingId}", name="parking_remove_confirmation", requirements={"parkingId" = "\d+"})
     * @Entity("parkingSublease", expr="repository.find(parkingId)")
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @param ParkingSublease $parkingSublease
     * @return Response
     */
    public function __invoke(ParkingSublease $parkingSublease): Response
    {
        $subleaseMonth = $parkingSublease->getDayDate()->format('m');
        $subleaseYear = $parkingSublease->getDayDate()->format('Y');

        return $this->render('parking_remove_confirmation/index.html.twig', [
            'ParkingSublease' => $parkingSublease,
            'SubleaseMonth' => $subleaseMonth,
            'SubleaseYear' => $subleaseYear,
        ]);
    }
}
