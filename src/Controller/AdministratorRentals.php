<?php

namespace App\Controller;

use App\Repository\ParkingSubleaseRepository;
use App\Utils\PageTools;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdministratorRentals extends AbstractController
{

    /**
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param Request $request
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @Route("/myAdministratorRentals/", name="my_administrator_rentals")
     */
    public function __invoke(ParkingSubleaseRepository $parkingSubleaseRepository, Request $request): Response
    {
        $referer = PageTools::getReferer($request);

        // Check if user's referer is 'calendar' (needed to display 'back' button)
        $isRefererCalendar = PageTools::isWordAppearing($referer, 'calendar');

        $subleasesList = $parkingSubleaseRepository->findby([], ['dayDate' => 'DESC']);

        return $this->render('AdministratorRentals.twig', [
            'subleases_list' => $subleasesList,
            'referer' => $referer,
            'is_referer_calendar' => $isRefererCalendar,
        ]);
    }
}
