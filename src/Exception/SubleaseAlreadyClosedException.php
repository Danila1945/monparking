<?php

namespace App\Exception;

use Exception;
use Throwable;

class SubleaseAlreadyClosedException extends Exception
{
    /**
     * SubleaseAlreadyClosedException constructor.
     *
     * @param string $message
     * @param int    $code
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
