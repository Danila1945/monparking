<?php

namespace App\Exception;

use App\Entity\User;

class InvalidUserException extends \InvalidArgumentException
{
    /**
     * @return static
     */
    public static function isNotEnabled(User $user): self
    {
        return new static ('User '.$user.' has been disabled');
    }

    /**
     * @return static
     */
    public static function isBlockedForInsolvency(User $user): self
    {
        return new static ('User '.$user.' has been blocked for unsolvency');
    }

    /**
     * @return static
     */
    public static function isNotRoleTenant(User $user): self
    {
        return new static ('User '.$user.' role is not a ROLE_TENANT user');
    }

    /**
     * @return static
     */
    public static function isNotRoleRenter(User $user): self
    {
        return new static ('User '.$user.' role is not a ROLE_RENTER user');
    }

    /**
     * @return static
     */
    public static function isNotRoleAdministrator(User $user): self
    {
        return new static ('User '.$user.' role is not a ROLE_ADMINISTRATOR user');
    }

    /**
     * @return static
     */
    public static function isNotAuthorizedUser(User $user): self
    {
        return new static ('User '.$user.' is not the lender of the selected sublease');
    }

    /**
     * @return static
     */
    public static function isAFreeRenter(User $user): self
    {
        return new static ('User '.$user.' is a free renter user and therefore doesn\'t get money');
    }

    /**
     * @param User $user
     *
     * @return static
     */
    public static function isNoUser(): self
    {
        return new static ('No existing user');
    }
}
