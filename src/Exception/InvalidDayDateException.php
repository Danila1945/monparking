<?php

namespace App\Exception;

use Exception;

class InvalidDayDateException extends Exception
{
    /**
     * @return static
     */
    public static function isDateNotWithinTwoMonthsOrIsPriorToToday(): self
    {
        return new static ('Chosen date is not within today plus two months or is prior to today');
    }

    /**
     * @return static
     */
    public static function isInsufficientSubleaseQuantity(): self
    {
        return new static ('No sublease available for selected day');
    }

    /**
     * @return static
     */
    public static function isMonthNotBetween1And12(): self
    {
        return new static ('Month number must be between 1 and 12');
    }

}
