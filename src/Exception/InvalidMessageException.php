<?php

namespace App\Exception;

use Exception;

class InvalidMessageException extends Exception
{
    /**
     * @return static
     */
    public static function isReceivedMessageNotReadYet(): self
    {
        return new static ('Received message can\'t be canceled if not read');
    }

    public static function isUserNotAuthorizedToCancelThisMessage(): self
    {
        return new static ('Logged in user not authoriezd to cancel this message');
    }

    public static function isUserNotAuthorizedToArchiveThisMessage(): self
    {
        return new static ('Logged in user not authorized to archive this message');
    }

    public static function loggedInUserIsNotTheAllowedReceiver(): self
    {
        return new static ('Logged in user is not authorized to open this message !');
    }

    public static function loggedInUserIsSendingMessageTomHimself(): self
    {
        return new static ('Logged in user is not authorized to send message to himself !');
    }
}
