<?php

namespace App\Exception;

use Exception;

class InvalidSubleaseException extends Exception
{
    /**
     * @return static
     */
    public static function beingNotTakenOrOpen(): self
    {
        return new static ("Not taken or opened ParkingSublease can't be removed. Renter can just stop the sublease.");
    }

    /**
     * @return static
     */
    public static function AlreadyExistingSubleaseSameDay(): self
    {
        return new static ('Sublease related to same day already existing');
    }

    /**
     * @return static
     */
    public static function beingTaken(): self
    {
        return new static ('Parking is already subleased');
    }

    /**
     * @return static
     */
    public static function isSubleaseAlreadyOpened(): self
    {
        return new static ('Another sublease has already been opened');
    }

    /**
     * @return static
     */
    public static function isSubleaseAlreadyTaken(): self
    {
        return new static ('Another sublease has already been taken');
    }

    /**
     * @return static
     */
    public static function isSubleaseNotOpen(): self
    {
        return new static ('Sublease is not open ! ');
    }
}
