<?php

namespace App\Calendar;

use App\ControllerHelpers\Security\SecurityUser;
use App\Entity\ParkingSublease;
use App\Exception\InvalidDayDateException;
use App\Repository\ParkingSubleaseRepository;
use App\Utils\UserProvider;
use DateTimeImmutable;
use Exception;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

class CalendarManager
{
    /**
     * @var ParkingSubleaseRepository
     */
    private $parkingSubleaseRepository;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @var SecurityUser
     */
    private $security;

    public function __construct(ParkingSubleaseRepository $parkingSubleaseRepository,
                                AuthorizationCheckerInterface $authorizationChecker, UserProvider $userProvider,
                                Security $security)
    {
        $this->parkingSubleaseRepository = $parkingSubleaseRepository;
        $this->authorizationChecker = $authorizationChecker;
        $this->userProvider = $userProvider;
        $this->security = $security;
    }

    /**
     * @throws Exception
     */
    public function getTenantPossibleSubleasesQuantity(string $stringYear, string $stringMonth): int
    {
        if (null !== $this->security->getUser()) {
            if ($this->authorizationChecker->isGranted('ROLE_TENANT')) {

                $possibleSubleases = $this->getOpenedPossibleSubleases($stringYear, $stringMonth);

                if (!empty($possibleSubleases)) {
                    $alreadySubleasedParkingSubleases = $this->parkingSubleaseRepository
                        ->findTabIsSubleaseTakenByTakerDateTodayAndLater($stringYear, $stringMonth,
                            $this->userProvider->getLoggedInUser());

                    $redundantDayDatesCounter
                        = $this->getRedundantDayDatesCounter($possibleSubleases,
                        $alreadySubleasedParkingSubleases);

                    $possibleSubleases
                        = count($possibleSubleases);

                    return $possibleSubleases - $redundantDayDatesCounter;
                }
            }
        }

        return 0;
    }

    /**
     * @return ParkingSublease[]|array
     *
     * @throws Exception
     */
    public function getTenantPossibleSubleasesDatesTab(string $stringYear, string $stringMonth): array
    {
        $totalOpenToSublease = $this->getOpenedPossibleSubleases($stringYear, $stringMonth);

        $possibleDatesTab = [];

        if (!empty($totalOpenToSublease)) {
            $alreadySubleasedDayDates = $this->parkingSubleaseRepository
                ->findTabIsSubleaseTakenByTakerDateTodayAndLater($stringYear, $stringMonth,
                    $this->userProvider->getLoggedInUser());

            $possibleDatesTab
                = $this->calculateTenantPossibleSubleases($totalOpenToSublease,
                $alreadySubleasedDayDates);

            $possibleDatesTab = $this->arrangeParkingSubleaseTabByDayDates($possibleDatesTab);
        }

        return $possibleDatesTab;
    }

    /**
     * @return ParkingSublease[]
     *
     * @throws Exception
     */
    private function getOpenedPossibleSubleases(string $stringYear, string $stringMonth): array
    {
        $totalOpenToSublease
            = $this->parkingSubleaseRepository->findTabPossibleSubleasesByYearMonth($stringYear, $stringMonth);

        $tempTab = [];

        /* @var ParkingSublease $sublease */
        foreach ($totalOpenToSublease as $sublease) {
            if (!isset($tempTab[$sublease->getDayDate()->format('Y-m-d')])) {
                $tempTab[$sublease->getDayDate()->format('Y-m-d')] = $sublease;
            }
        }

        $tempTab2 = [];

        foreach ($tempTab as $relatedSublease) {
            $tempTab2[] = $relatedSublease;
        }

        return $tempTab2;
    }

    public function getTenantTakenSubleasesIndexedByDatesTab(string $stringYear, string $stringMonth): array
    {
        $tab = $this->parkingSubleaseRepository
            ->findTabIsSubleaseTakenByTakerInCurrentMonth($stringYear, $stringMonth,
                $this->userProvider->getLoggedInUser());

        return $this->arrangeParkingSubleaseTabByDayDates($tab);
    }

    /**
     * @param ParkingSublease[] $currentMonthTotalOpenToSubleaseForTenant
     * @param ParkingSublease[] $alreadySubleased
     */
    private function getRedundantDayDatesCounter(array $currentMonthTotalOpenToSubleaseForTenant,
                                                 array $alreadySubleased): int
    {
        $counter = 0;

        foreach ($currentMonthTotalOpenToSubleaseForTenant as $openedSublease) {
            foreach ($alreadySubleased as $subleased) {
                if ($openedSublease->getDayDate()->format('d-m-Y')
                    === $subleased->getDayDate()->format('d-m-Y')) {
                    ++$counter;
                }
            }
        }

        return $counter;
    }

    /**
     * @param ParkingSublease[] $openedSubleases
     * @param ParkingSublease[] $takenSubleases
     *
     * @return ParkingSublease[]
     */
    private function getOpenedSubleasesWithoutTakenAtSameDatesTab(array $openedSubleases, array $takenSubleases): array
    {
        $openedSubleasesWithoutTaken = [];
        foreach ($openedSubleases as $openedSublease) {
            foreach ($takenSubleases as $takenSublease) {
                if ($openedSublease->getDayDate()->format('d-m-Y')
                    !== $takenSublease->getDayDate()->format('d-m-Y')) {
                    $openedSubleasesWithoutTaken[] = $openedSublease;
                }
            }
        }

        return $openedSubleasesWithoutTaken;
    }

    /**
     * @param ParkingSublease[] $openedSubleases
     * @param ParkingSublease[] $takenSubleases
     *
     * @return ParkingSublease[]
     */
    private function calculateTenantPossibleSubleases(array $openedSubleases, array $takenSubleases): array
    {
        $possibleSubleasesTab = [];

        switch ([$openedSubleases, $takenSubleases]) {
            case (!empty($openedSubleases)) && (!empty($takenSubleases)):
                $possibleSubleasesTab = $this->getOpenedSubleasesWithoutTakenAtSameDatesTab($openedSubleases, $takenSubleases);
                break;

            case empty($takenSubleases):
                $possibleSubleasesTab = $openedSubleases;
                break;
        }

        return $possibleSubleasesTab;
    }

    /**
     * @return ParkingSublease[]
     */
    public function arrangeParkingSubleaseTabByDayDates(array $parkingSubleasesTab): array
    {
        $sortedTab = [];
        foreach ($parkingSubleasesTab as $sublease) {
            $sortedTab[$sublease->getDayDate()->format('Y-m-d')] = $sublease;
        }

        return $sortedTab;
    }

    /**
     * @return ParkingSublease[]
     */
    public function arrangeParkingSubleaseTabIntoDayDatesTab(array $parkingSubleasesTab): array
    {
        $sortedTab = [];
        foreach ($parkingSubleasesTab as $sublease) {
            $sortedTab[$sublease->getDayDate()->format('Y-m-d')][] = $sublease;
        }

        return $sortedTab;
    }

    public function getAllTakendDayDatesTab(string $stringYear, string $stringMonth): array
    {
        $takenParkingSubleasesRelatedToCurrentMonth =
            $this->parkingSubleaseRepository->findTabIsSubleaseTakenByYearAndMonth($stringYear, $stringMonth);

        return $this->createDayDatesCounterTable($takenParkingSubleasesRelatedToCurrentMonth);
    }

    public function getAllOpenedDayDatesTab(string $stringYear, string $stringMonth): array
    {
        $openedParkingSubleasesRelatedToCurrentMonth =
            $this->parkingSubleaseRepository->findTabIsSubleaseOpenedByYearAndMonth($stringYear, $stringMonth);

        return $this->createDayDatesCounterTable($openedParkingSubleasesRelatedToCurrentMonth);
    }

    public function getTakenSubleasesInCurrentMonthByRenter(string $year, string $month)
    {
        $tab = $this->parkingSubleaseRepository->findTabTakenSubleasesByCurrentMonthAndByRenter($year, $month,
            $this->userProvider->getLoggedInUser());

        return $this->arrangeParkingSubleaseTabByDayDates($tab);
    }

    /**
     * @return ParkingSublease[]|array
     *
     * @throws Exception
     */
    public function getOpenSubleasesInCurrentMonthByRenter(string $year, string $month): array
    {
        $tab = $this->parkingSubleaseRepository->findTabOpenSubleasesByCurrentMonthAndByRenter($year, $month,
            $this->userProvider->getLoggedInUser());

        return $this->arrangeParkingSubleaseTabByDayDates($tab);
    }

    /**
     * @return ParkingSublease[]|array
     */
    public function getAllOpenAndTakenSubleasesInChosenMonth(string $year, string $month): array
    {
        $tab = $this->parkingSubleaseRepository->findTabOpenAndTakenSubleasesByChosenMonth($year, $month);

        return $this->arrangeParkingSubleaseTabIntoDayDatesTab($tab);
    }

    /**
     * @return DateTimeImmutable[]
     *
     * @throws Exception
     */
    public function getAllDatesOfAMonth(string $year, string $month): array
    {
        $firstDate = new DateTimeImmutable($year.'-'.$month.'-01');
        $lastDate = $firstDate->modify('+1 month')->modify('-1 day');

        $tab = [];

        while ($firstDate <= $lastDate) {
            $tab[] = $firstDate;
            $firstDate = $firstDate->modify('+1 day');
        }

        return $tab;
    }

    /**
     * @param ParkingSublease[] $tab
     */
    private function createDayDatesCounterTable(array $tab): array
    {
        $dayDatesOfCurrentMonth = [];

        foreach ($tab as $parkingSublease) {
            $stringDate = $parkingSublease->getDayDate()->format('Y-m-d');
            if (!isset($dayDatesOfCurrentMonth[$stringDate])) {
                $dayDatesOfCurrentMonth[$stringDate] = 0;
                ++$dayDatesOfCurrentMonth[$stringDate];
            } else {
                ++$dayDatesOfCurrentMonth[$stringDate];
            }
        }

        return $dayDatesOfCurrentMonth;
    }

    public function getPreviousMonth(int &$year, int &$month): void
    {
        --$month;
        if ($month < 1) {
            $month = 12;
            --$year;
        }
    }

    public function getNextMonth(int &$year, &$month): void
    {
        ++$month;
        if ($month > 12) {
            $month = 1;
            ++$year;
        }
    }

    /**
     * @throws InvalidDayDateException
     */
    public function checkMonthIsBetween1And12(int $month): void
    {
        if ($month < 1 || $month > 12) {
            throw InvalidDayDateException::isMonthNotBetween1And12();
        }
    }
}
