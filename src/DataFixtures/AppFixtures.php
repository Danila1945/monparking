<?php

namespace App\DataFixtures;

use App\Entity\Day;
use App\Entity\ParkingSublease;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->loadDev($manager);
        $manager->flush();
    }

    private function loadDev(ObjectManager $manager)
    {
//        ************************
//        Renters
//        ************************
        $password = 'passpass';

        $price = 5.0;
//        $renterA = new User('preteurA', $password, 'Prêteur','A', 'ROLE_RENTER');
//        $renterA->setParkingNumber(41);
//        $manager->persist($renterA);
//
//        $renterB = new User('preteurB', $password, 'Prêteur','B', 'ROLE_RENTER');
//        $renterB->setParkingNumber(27);
//        $manager->persist($renterB);

//        ************************
//        Tenants
//        ************************
//        $tenantA = new User('emprunteurA', $password, 'Emprunteur','A', 'ROLE_TENANT');
//        $manager->persist($tenantA);
//
//        $tenantB = new User('emprunteurB', $password, 'Emprunteur','B', 'ROLE_TENANT');
//        $manager->persist($tenantB);

        // parkings

        // A loue sa place à A

//        ************************
//        Administrator
//        ************************
        $administrator = new User('admin', $password, 'Administrateur', 'Admin', 'ROLE_ADMINISTRATOR');
        $manager->persist($administrator);

        $tenant = new User('tenant', $password, 'tenant', 'tenant', 'ROLE_TENANT');
        $manager->persist($tenant);

        $tenant2 = new User('tenant2', $password, 'tenant2', 'tenant2', 'ROLE_TENANT');
        $manager->persist($tenant2);

        $renter = new User('renter', $password, 'renter', 'renter', 'ROLE_RENTER');
        $renter->setParkingNumber(41);
        $manager->persist($renter);

        $renter2 = new User('renter2', $password, 'renter2', 'renter2', 'ROLE_RENTER');
        $renter2->setParkingNumber(42);
        $manager->persist($renter2);

        $date = new DateTimeImmutable();
        $date = $date->setDate(2020, 01, 02);
        $day = new Day($date);

        $newParkingSublease = new ParkingSublease($day, $renter, $price);
        $newParkingSublease->setTaker($tenant);
        $newParkingSublease->setIsTaken();
        $newParkingSublease->setDueDate();
//        $newParkingSublease->markAsPaid();

        $date2 = new DateTimeImmutable();
        $date2 = $date2->setDate(2020, 02, 17);
        $day2 = new Day($date2);

        $newParkingSublease2 = new ParkingSublease($day2, $renter2, $price);
        $newParkingSublease2->setTaker($tenant2);
        $newParkingSublease2->setIsTaken();
        $newParkingSublease2->setDueDate();
        $newParkingSublease2->markAsPaid();

        $date3 = new DateTimeImmutable();
        $date3 = $date3->setDate(2020, 02, 12);
        $day3 = new Day($date3);

        $newParkingSublease3 = new ParkingSublease($day3, $renter2, $price);
        $newParkingSublease3->setTaker($tenant2);
        $newParkingSublease3->setIsTaken();
        $newParkingSublease3->setDueDate();
        $newParkingSublease3->markAsPaid();

        $manager->persist($newParkingSublease);
        $manager->persist($newParkingSublease2);
        $manager->persist($newParkingSublease3);

        $manager->flush();
    }
}
