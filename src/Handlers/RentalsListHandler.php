<?php

namespace App\Handlers;

use App\DTO\Sublease\DTOParkingSubleaseTakerList;
use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Repository\ParkingSubleaseRepository;
use DateTimeImmutable;
use Exception;

/**
 * Class RentalsListHandler.
 */
class RentalsListHandler
{
    private $parkingSubleaseRepository;

    /**
     * RentalsListHandler constructor.
     */
    public function __construct(ParkingSubleaseRepository $parkingSubleaseRepository)
    {
        $this->parkingSubleaseRepository = $parkingSubleaseRepository;
    }

    /**
     * @throws Exception
     */
    public function getUserOrderedRentalsList(User $user): array
    {
        $subleases = $this->getUserSubleasesList($user);

        $orderedSubleases = $this->sortSubleasesByTaker($subleases);

        $sortedSubleases = $this->sortTakerSubleasesByMonth($orderedSubleases);

        return $sortedSubleases;
    }

    /**
     * Will return user's subleases for last and current year.
     *
     * @return ParkingSublease[]
     *
     * @throws Exception
     */
    public function getUserSubleasesList(User $user): array
    {
        return $this->parkingSubleaseRepository->findTabIsSubleaseTakenByRenter(
            $user->getId(), new DateTimeImmutable('last year January 1st')
        );
    }

    /**
     * Will sort subleases by taker.
     */
    public function sortSubleasesByTaker(array $subleases): array
    {
        $orderedSubleases = [];

        foreach ($subleases as $sublease) {
            /* @var ParkingSublease $sublease */
            $taker = $sublease->getTaker();
            $takerKey = $taker->getId();

//            if the entry in the table has not yet been created, then
//            it is created
            if (!isset($orderedSubleases[$takerKey])) {
                $orderedSubleases[$takerKey] = new DTOParkingSubleaseTakerList($taker);
            }

            /* @var DTOParkingSubleaseTakerList $orderedSubleases[$takerKey] */
            $orderedSubleases[$takerKey]->addSublease($sublease);
        }

        return $orderedSubleases;
    }

    /**
     * Warning! This methods awaits an array from sortSubleasesByTaker() method!
     * This is NOT a simple array!
     *
     * @throws Exception
     */

//    à faire dans dto
    public function sortTakerSubleasesByMonth(array $subleases): array
    {
        $sortedSubleases = [];

//        pas compris le $takerKey => $subleaseInfo
        foreach ($subleases as $takerKey => $subleaseInfoArray) {
            if (!isset($subleaseInfoArray['subleases'])) {
                throw new Exception('Invalid sublease info array - The array is tri-dimentional!');
            }

            $sortedSubleases[$takerKey] = $subleaseInfoArray;
            $sortedSubleases[$takerKey]['subleases'] = $this->sortSubleasesByMonth($subleaseInfoArray['subleases']);
        }

        return $sortedSubleases;
    }

    public function sortSubleasesByMonth(array $subleases): array
    {
        $sortedSubleases = [];

        foreach ($subleases as $sublease) {
            /* @var ParkingSublease $sublease */
            $month = $sublease->getDayDate()->format('Y-m');
            if (!isset($sortedSubleases[$month])) {
                $sortedSubleases[$month] = [];
            }

            $sortedSubleases[$month][] = $sublease;
        }

        return $sortedSubleases;
    }

    public function sumSubleasesByMonth(array $subleases): array
    {
        $sortedSubleases = [];

        /* @var ParkingSublease[] $subleases */
        foreach ($subleases as $sublease => $month['subleases']) {
            /* @var ParkingSublease $month */
            $month = $month->getDayDate()->format('Y-m');
            $sortedSubleases[$month]['subleases']['sum'] += $sublease->getPrice();
        }

        return $sortedSubleases;
    }
}

/*

Statuts :

- Location proposée
- Location demandée
- Location acceptée
- Location refusée
                                                                                                Paiement validé
                        Acceptée > Attente que la location ait lieu > En attente de paiement >
Proposée > Demandée >                                                                           Paiement refusé
                        Refusée



 * */
