<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200504112839 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parkingsublease DROP FOREIGN KEY FK_66D682E89C24126');
        $this->addSql('DROP TABLE days');
        $this->addSql('DROP INDEX idx_day_user ON parkingsublease');
        $this->addSql('DROP INDEX IDX_66D682E89C24126 ON parkingsublease');
        $this->addSql('ALTER TABLE parkingsublease DROP day_id');
        $this->addSql('CREATE UNIQUE INDEX idx_day_date_user ON parkingsublease (day_date, user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE days (id INT AUTO_INCREMENT NOT NULL, date DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', is_free_parking_sublease_by_day_quantity INT NOT NULL, UNIQUE INDEX idx_date (date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP INDEX idx_day_date_user ON parkingsublease');
        $this->addSql('ALTER TABLE parkingsublease ADD day_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE parkingsublease ADD CONSTRAINT FK_66D682E89C24126 FOREIGN KEY (day_id) REFERENCES days (id)');
        $this->addSql('CREATE UNIQUE INDEX idx_day_user ON parkingsublease (day_id, user_id)');
        $this->addSql('CREATE INDEX IDX_66D682E89C24126 ON parkingsublease (day_id)');
    }
}
