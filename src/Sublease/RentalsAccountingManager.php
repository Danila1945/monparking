<?php

namespace App\Sublease;

use App\DTO\Sublease\DTOParkingSublease;
use App\DTO\Sublease\DTOParkingSubleaseTakerList;
use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Repository\ParkingSubleaseRepository;
use App\Repository\ParkingSubleaseRepositoryInterface;
use App\Repository\UserRepository;
use App\Utils\DateTools;
use DateTimeImmutable;
use Exception;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

class RentalsAccountingManager
{
    /**
     * @var ParkingSubleaseRepositoryInterface
     */
    private $parkingSubleaseRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var User
     */
    private $user;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var ParkingSublease[]
     */
    private $userSubleases = [];

    /**
     * @var User[]
     */
    private $contractors = [];

    /**
     * @var DateTimeImmutable
     */
    private $beginDate;

    /**
     * @var DateTimeImmutable
     */
    private $currentMonth;

    /**
     * @var string[]
     */
    private $last24MonthsTab;

    /**
     * @var ParkingManager
     */
    private $parkingManager;

    public function __construct(ParkingSubleaseRepository $parkingSubleaseRepository, Security $security,
                                AuthorizationCheckerInterface $authorizationChecker, UserRepository $userRepository,
                                ParkingManager $parkingManager)
    {
        $this->parkingSubleaseRepository = $parkingSubleaseRepository;
        $this->user = $security->getUser()->getUser();
        $this->authorizationChecker = $authorizationChecker;
        $this->userRepository = $userRepository;
        $this->parkingManager = $parkingManager;
    }

    /**
     * @return DTOParkingSublease[]
     *
     * @throws Exception
     */
    public function getPerMonthAndContractorTab(string $category): array
    {
        $beginDate = (new \DateTimeImmutable('first day of this month'))
            ->modify('+2 months')
            ->modify('midnight');
        $userSubleases = $subleases = $this->parkingSubleaseRepository
            ->findTabIsSubleaseTakenByRenter($this->user->getId(), $beginDate);
        $contractors = $this->userRepository->getAllTenantUsersList();

        DateTools::datesFulfillment($this->beginDate, $this->currentMonth);
        $this->parkingManager->subleasesAndContractorsFulfillment($this->userSubleases,
            $this->contractors, $this->beginDate);
        $this->last24MonthsTab = DateTools::last24MonthFulfillment($this->beginDate, $this->currentMonth);

        $DTOParkingSublease = null;
        $perMonthAndContractorTab = [];

        foreach ($this->last24MonthsTab as $dateMonth) {
            foreach ($this->contractors as $contractor) {
                if ($this->authorizationChecker->isGranted('ROLE_RENTER')) {
                    $DTOParkingSublease =
                        new DTOParkingSubleaseTakerList($dateMonth, $contractor, 'roleRenter', $this->userSubleases);
                } elseif ($this->authorizationChecker->isGranted('ROLE_TENANT')) {
                    $DTOParkingSublease =
                        new DTOParkingSubleaseTakerList($dateMonth, $contractor, 'roleTenant', $this->userSubleases);
                }

                if (!empty($DTOParkingSublease->getMonthSubleasesPerContractor())) {
                    if ('isPaid' === $category) {
                        if ($DTOParkingSublease->checkIfMonthIsPaid()
                            || 0 === $DTOParkingSublease->getMonthSubleasesPerContractorTotal()) {
                            $perMonthAndContractorTab[$dateMonth][$contractor->getId()][] = $DTOParkingSublease;
                        }
                    } elseif ('isNotPaid' === $category) {
                        if (!$DTOParkingSublease->checkIfMonthIsPaid()
                            && 0 !== $DTOParkingSublease->getMonthSubleasesPerContractorTotal()) {
                            $perMonthAndContractorTab[$dateMonth][$contractor->getId()][] = $DTOParkingSublease;
                        }
                    } else {
                        $perMonthAndContractorTab[$dateMonth][$contractor->getId()][] = $DTOParkingSublease;
                    }
                }
            }
        }

        return $perMonthAndContractorTab;
    }

    public function getContractorsWithSubleases(): array
    {
        if ($this->authorizationChecker->isGranted('ROLE_TENANT')) {
            return $this->getContractorsWithSubleasesForTenant();
        }

        return $this->getContractorsWithSubleasesForRenter();
    }

    public function getContractorsWithSubleasesForTenant(User $tenant, string $category): array
    {
        $tenantSubleases = null;
        if ('isPaid' === $category) {
            /* @var ParkingSublease[] $tenantSubleases */
            $tenantSubleases = $this
                ->parkingSubleaseRepository
                ->findTabPaidSubleasesTakenByTaker($tenant);
        } elseif ('isNotPaid' === $category) {
            /* @var ParkingSublease[] $tenantSubleases */
            $tenantSubleases = $this
                ->parkingSubleaseRepository
                ->findTabNotPaidSubleasesTakenByTaker($tenant);
        } else {
            /* @var ParkingSublease[] $tenantSubleases */
            $tenantSubleases = $this
                ->parkingSubleaseRepository
                ->findTabAllSubleasesTakenByTaker($tenant);
        }
        $tab = [];
        foreach ($tenantSubleases as $sublease) {
            $tab[$sublease->getUser()->getId()][$sublease->getDayDate()->format('Y-m')][] = $sublease;
        }

        return $tab;
    }

    public function getContractorsWithSubleasesForRenter(User $renter, string $category): array
    {
        $renterSubleases = null;
        if ('isPaid' === $category) {
            /* @var ParkingSublease[] $renterSubleases */
            $renterSubleases = $this
                ->parkingSubleaseRepository
                ->findTabPaidSubleasesTakenByRenter($renter);
        } elseif ('isNotPaid' === $category) {
            /* @var ParkingSublease[] $renterSubleases */
            $renterSubleases = $this
                ->parkingSubleaseRepository
                ->findTabNotPaidSubleasesTakenByRenter($renter);
        } else {
            /* @var ParkingSublease[] $renterSubleases */
            $renterSubleases = $this->parkingSubleaseRepository->findTabAllSubleasesTakenByRenter($renter);
        }
        $tab = [];
        foreach ($renterSubleases as $sublease) {
            $tab[$sublease->getTaker()->getId()][$sublease->getDayDate()->format('Y-m')][] = $sublease;
        }

        return $tab;
    }

    /**
     * @param ParkingSublease[] $subleases
     * @return ParkingSublease
     */
    public function getMinPaidAtValue(array $subleases): ParkingSublease
    {
        $min = reset($subleases);
        foreach ($subleases as $sublease) {
            if ($sublease->getPaidAt() < $min->getPaidAt()) {
                $min = $sublease;
            }
        }

        return $min;
    }
}
