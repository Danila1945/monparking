<?php

namespace App\Sublease;

use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Exception\InvalidUserException;
use App\Repository\ParkingSubleaseRepositoryInterface;
use App\Repository\SiteOptionRepository;
use App\Utils\DateTools;
use App\Utils\TabTools;
use DateTimeImmutable;
use Exception;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

class MoneyManager
{
    /**
     * @var ParkingSubleaseRepositoryInterface
     */
    private $parkingSubleaseRepository;

    /**
     * @var User
     */
    private $user;


    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var SiteOptionRepository
     */
    private $optionRepository;

    public function __construct(ParkingSubleaseRepositoryInterface $parkingSubleaseRepository, Security $security,
                                AuthorizationCheckerInterface $authorizationChecker,
                                SiteOptionRepository $optionRepository)
    {
        $this->parkingSubleaseRepository = $parkingSubleaseRepository;
        if (null !== $security->getUser()) {
            $this->user = $security->getUser()->getUser();
        }
        $this->authorizationChecker = $authorizationChecker;
        $this->optionRepository = $optionRepository;
    }

    public function getPrice()
    {
        return $this->optionRepository->findOneBy(['optionName' => 'parkingPrice'])->getPrice();
    }

    /**
     * @throws Exception
     */
    public function moneyAcknowledge(User $contractor, string $month): User
    {
        $userSubleases = $this->userSubleasesTableFulfillment($contractor, $month);

        /* @var ParkingSublease $anySublease */
        $anySublease = TabTools::getFirstElementOfATab($userSubleases);

        if (false === $anySublease) {
            throw InvalidUserException::isNoUser();
        }

        $anySubleaseRenterUser = $anySublease->getUser();

        if ($anySubleaseRenterUser !== $this->user &&
            !$this->authorizationChecker->isGranted('ROLE_PREVIOUS_ADMIN')) {
            throw InvalidUserException::isNotAuthorizedUser($anySubleaseRenterUser);
        }

//        if ($anySubleaseRenterUser->getIsFreeRenter()) {
//            throw InvalidUserException::isAFreeRenter($anySubleaseRenterUser);
//        }

        $this->markSubleasesAsPaid($userSubleases);

        $this->parkingSubleaseRepository->saveParkingSubleasesTab($userSubleases);

        return $this->user;
    }

    /**
     * @throws Exception
     */
    private function userSubleasesTableFulfillment(User $contractor, string $month): array
    {
        $beginDate = new DateTimeImmutable($month);
        $endDate = DateTools::getLastDayOfGivenMonth($beginDate);

        return $this->parkingSubleaseRepository->findTabIsSubleaseTakenByRenterAndByTakerBetweenTwoDates($this->user,
            $contractor, $beginDate, $endDate);
    }

    private function markSubleasesAsPaid(array $userSubleases)
    {
        /* @var ParkingSublease[] $userSubleases */
        foreach ($userSubleases as $sublease) {
            if ($sublease->getIsNotPaid()) {
                $sublease->markAsPaid();
            }
        }
    }
}
