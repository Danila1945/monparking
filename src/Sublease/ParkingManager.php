<?php

namespace App\Sublease;

use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Exception\InvalidDayDateException;
use App\Exception\InvalidSubleaseException;
use App\Exception\InvalidUserException;
use App\Exception\SubleaseAlreadyClosedException;
use App\Repository\ParkingSubleaseRepositoryInterface;
use App\Repository\UserRepository;
use App\Utils\DateTools;
use App\Utils\UserProvider;
use DateTimeImmutable;
use Exception;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

class ParkingManager
{
    /**
     * @var ParkingSubleaseRepositoryInterface
     */
    private $parkingSubleaseRepository;

    /**
     * @var User
     */
    private $user;

    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var MoneyManager
     */
    private $moneyManager;

    public function __construct(ParkingSubleaseRepositoryInterface $parkingSubleaseRepository, Security $security,
                                UserProvider $userProvider, UserRepository $userRepository,
                                AuthorizationCheckerInterface $authorizationChecker, MoneyManager $moneyManager)
    {
        $this->parkingSubleaseRepository = $parkingSubleaseRepository;
        if (null !== $security->getUser()) {
            $this->user = $security->getUser()->getUser();
        }
        $this->userProvider = $userProvider;
        $this->userRepository = $userRepository;
        $this->authorizationChecker = $authorizationChecker;
        $this->moneyManager = $moneyManager;
    }

    /**
     * @throws Exception
     */
    public function createOpenedSublease(string $dateString): ParkingSublease
    {
        if (!$this->user->getIsUserEnabled()) {
            throw InvalidUserException::isNotEnabled($this->user);
        }

        if (!$this->authorizationChecker->isGranted('ROLE_RENTER')) {
            throw InvalidUserException::isNotRoleRenter($this->user);
        }

        $dateFormat = DateTools::getDateFromString($dateString);
        $this->checkEligibleDayDate($dateFormat);

        $possibleExistingSublease = $this->parkingSubleaseRepository
            ->findIsSubleaseOpenedOrTakenByDayDateAndByRenter($dateFormat, $this->user);

        if (null !== $possibleExistingSublease) {
            if ($possibleExistingSublease->getIsSubleaseOpened()) {
                throw InvalidSubleaseException::isSubleaseAlreadyOpened();
            } elseif ($possibleExistingSublease->getIsTaken()) {
                throw InvalidSubleaseException::isSubleaseAlreadyTaken();
            }
        }

        $newParkingSublease = new ParkingSublease($dateFormat, $this->user, $this->moneyManager->getPrice());

        $this->parkingSubleaseRepository->save($newParkingSublease);

        return $newParkingSublease;
    }

    /**
     * @throws SubleaseAlreadyClosedException
     * @throws Exception
     */
    public function removeOpenedSublease(ParkingSublease $parkingSublease): DateTimeImmutable
    {
        $this->checkEligibleDayDate($parkingSublease->getDayDate());

        if ($this->user !== $parkingSublease->getUser()) {
            throw InvalidUserException::isNotAuthorizedUser($this->user);
        }

        if (!$parkingSublease->getIsSubleaseOpened()) {
            throw new SubleaseAlreadyClosedException('Sublease is not opened');
        }

        $concernedDayDate = $parkingSublease->getDayDate();

        $parkingSublease->unSublease();
        $this->parkingSubleaseRepository->remove($parkingSublease);

        return $concernedDayDate;
    }

    /**
     * @throws InvalidSubleaseException
     */
    public function unTakeSublease(ParkingSublease $parkingSublease): ParkingSublease
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMINISTRATOR')) {
            throw InvalidUserException::isNotRoleAdministrator($this->user);
        }

        if (false === $parkingSublease->getIsTaken() || true === $parkingSublease->getIsSubleaseOpened()) {
            throw InvalidSubleaseException::beingNotTakenOrOpen();
        }

        $parkingSublease->subleaseRetract();

        $this->parkingSubleaseRepository->save($parkingSublease);

        return $parkingSublease;
    }

    /**
     * @throws Exception
     */
    public function takeSublease(ParkingSublease $parkingSublease): ParkingSublease
    {
        if (!$this->user->getIsUserEnabled()) {
            throw InvalidUserException::isNotEnabled($this->user);
        }

        if ($this->user->getIsUserBlockedForInsolvency()) {
            throw InvalidUserException::isBlockedForInsolvency($this->user);
        }

        if (!$this->authorizationChecker->isGranted('ROLE_TENANT')) {
            throw InvalidUserException::isNotRoleTenant($this->user);
        }

        $this->checkEligibleDayDate($parkingSublease->getDayDate());

        // Check that the taker user is not trying to sublease more than one parking the same day
        $hasUserAlreadyAParkingTheSameDay = $this->parkingSubleaseRepository
            ->findIsSubleaseTakenByDayDateAndByTaker($parkingSublease->getDayDate(), $this->user);
        if ($hasUserAlreadyAParkingTheSameDay) {
            throw InvalidSubleaseException::AlreadyExistingSubleaseSameDay();
        }

        // Check that the found sublease proposal is in opened status and not in taken status
        if ($parkingSublease->getIsTaken()) {
            throw InvalidSubleaseException::beingTaken();
        }

        $parkingSublease->setTaker($this->user);
        $parkingSublease->setIsTaken();
        if ($parkingSublease->getPrice() > 0) {
            $parkingSublease->setDueDate();
        }

        $this->parkingSubleaseRepository->save($parkingSublease);

        return $parkingSublease;
    }

    /**
     * @throws Exception
     */
    private function checkEligibleDayDate(DateTimeImmutable $dateToBeFreedUp): void
    {
        $today = DateTools::getTodayDate();
        $todayPlusTwoMonths = $today->modify('+2 month');
        $isDateWithinTwoMonth = $dateToBeFreedUp <= $todayPlusTwoMonths;
        $isDatePriorToTodaysDate = $dateToBeFreedUp < $today;

        if (!$isDateWithinTwoMonth || $isDatePriorToTodaysDate) {
            throw InvalidDayDateException::isDateNotWithinTwoMonthsOrIsPriorToToday();
        }
    }

    /*
     * /////////////////////////////////////////////////////////////////////////////////////////
     * ///////////////////////////////////////////////////////////////////////////////////////
     * NavBarCounterController
     */


    /**
     * @param $receivableTotalAmount
     * @param $receivedTotalAmount
     * @param $receivableAndReceivedTotalAmount
     */
    public function computeRenterAmounts(float &$receivableTotalAmount, float &$receivedTotalAmount,
                                         float &$receivableAndReceivedTotalAmount)
    {
        $receivableTotalAmount =
            $this->parkingSubleaseRepository->findMoneyToBeReceivedSubleasesPerUserSum($this->user);

        $receivedTotalAmount =
            $this->parkingSubleaseRepository->findMoneyReceivedSubleasesPerUserSum($this->user);

        $receivableAndReceivedTotalAmount =
            $this->parkingSubleaseRepository->findMoneyToBeReceivedAndReceivedSubleasesPerUserSum($this->user);
    }

    /**
     * @param array $toBePaidSubleases
     *
     * @return float
     * @throws Exception
     */
    public function getDueAmountOnNextDueDate(): float
    {
        $toBePaidSubleases =
            $this->parkingSubleaseRepository->findTabMoneyDueSubleasesPerTaker($this->user);

        return $this->calculateDueAmountOnNextDueDate($toBePaidSubleases, $this->userProvider);
    }

    /**
     * @param ParkingSublease[] $toBePaidList
     * @return DateTimeImmutable
     */
    private function getFirstDueDate(array $toBePaidList, DateTimeImmutable $now): DateTimeImmutable
    {
        if (!empty($toBePaidList)) {
            $oldestSublease = reset($toBePaidList);
            if (null !== $oldestSublease) {
                return $oldestSublease->getDueDate();
            } else {
                return $now->modify('+ 1 years');
            }
        } else {
            return $now->modify('+1 years');
        }
    }

    /**
     * @param ParkingSublease[] $toBePaidList
     *
     * @return float
     * @throws Exception
     */
    private function calculateDueAmountOnNextDueDate(array $toBePaidList, UserProvider $userProvider): float
    {
        $dueAmountOnNextDueDate = 0.0;

        $now = DateTools::getTodayDate();

        $firstDueDate = $this->getFirstDueDate($toBePaidList, $now);
        $userProvider->toggleSetBlockedForInsolvency($now, $firstDueDate, $this->user);

        $monthFirstDayOfDueDate = $now->modify('first day of this month');
        foreach ($toBePaidList as $sublease) {
            if ($sublease->getDayDate() < $monthFirstDayOfDueDate) {
                $dueAmountOnNextDueDate += $sublease->getPrice();
            }
        }

        return $dueAmountOnNextDueDate;
    }


    /**
     * @param ParkingSublease[] $subleases
     * @param User[]            $contractors
     */
    public function subleasesAndContractorsFulfillment(
        array &$subleases,
        array &$contractors,
        DateTimeImmutable $beginDate
    ) {
        if ($this->authorizationChecker->isGranted('ROLE_RENTER')) {
            $subleases = $this->parkingSubleaseRepository
                ->findTabIsSubleaseTakenByRenter($this->user->getId(), $beginDate);
            $contractors = $this->userRepository->getAllTenantUsersList();
        } elseif ($this
            ->authorizationChecker->isGranted('ROLE_TENANT')) {
            $subleases = $this->parkingSubleaseRepository
                ->findTabIsSubleaseTakenByTaker($this->user, $beginDate);
            $contractors = $this->userRepository->getAllRenterUsersList();
        }
    }
}
