<?php

namespace App\Utils;

use App\ControllerHelpers\CalendarData;
use DateTimeImmutable;
use DateTimeZone;
use Exception;

class DateTools
{
    /**
     * @throws Exception
     */
    public static function getDateFromString(string $dateString, string $timezone = 'Europe/Paris'): DateTimeImmutable
    {
        return new DateTimeImmutable($dateString, new DateTimeZone($timezone));
    }

    /**
     * @throws Exception
     */
    public static function getTodayDate(): DateTimeImmutable
    {
        return new DateTimeImmutable('midnight', new DateTimeZone('Europe/Paris'));
    }

    public static function getFirstDayOfGivenMonth(DateTimeImmutable $givenDate): DateTimeImmutable
    {
        return $givenDate->modify('first day of this month');
    }

    public static function getLastDayOfGivenMonth(DateTimeImmutable $givenDate): DateTimeImmutable
    {
        return $givenDate->modify('last day of this month');
    }

    /**
     * @throws Exception
     */
    public static function getLastDayOfGivenMonthFromString(string $month): DateTimeImmutable
    {
        $date = new DateTimeImmutable($month);

        return $date->modify('last day of this month');
    }

    public static function getCurrentDatePlusTenDays(DateTimeImmutable $givenDate): DateTimeImmutable
    {
        return $givenDate->modify('+ 10 days');
    }

    /**
     * @throws Exception
     */
    public static function getCurrentMonthDueDate(): DateTimeImmutable
    {
        $currentDate = DateTools::getTodayDate();
        $currentDate = DateTools::getFirstDayOfGivenMonth($currentDate);

        return DateTools::getCurrentDatePlusTenDays($currentDate);
    }

    public static function getGivenDatePlusOneMonth(DateTimeImmutable $givenDate): DateTimeImmutable
    {
        return $givenDate->modify('+1 month');
    }

    public static function getGivenDatePlusOneYear(DateTimeImmutable $givenDate): DateTimeImmutable
    {
        return $givenDate->modify('+1 year');
    }

    public static function getGivenDatePlusTwoMonths(DateTimeImmutable $givenDate): DateTimeImmutable
    {
        return $givenDate->modify('+2 month');
    }


    public static function getGivenDateLess24Months(DateTimeImmutable $givenDate): DateTimeImmutable
    {
        return $givenDate->modify('-24 month');
    }

    /**
     * @throws Exception
     */
    public static function getPreviousMonthString(): string
    {
        $calendar = new CalendarData();

        return $calendar->toStringLowCasePreviousMonth();
    }


    /**
     * @param string $month
     * @return DateTimeImmutable
     * @throws Exception
     */
    public static function getLastDateOfAStringMonth(string $month): DateTimeImmutable
    {
        $date = new DateTimeImmutable($month);

        return $date->modify('last day of this month');
    }

    /**
     * @param string $month
     * @return DateTimeImmutable
     * @throws Exception
     */
    public static function getFirstDateOfAStringMonth(string $month): DateTimeImmutable
    {
        return new DateTimeImmutable($month);
    }

    /**
     * @throws Exception
     */
    public static function datesFulfillment(&$beginDate, &$currentMonth)
    {
        $tempBeginDate = DateTools::getTodayDate();
        $tempBeginDate = DateTools::getFirstDayOfGivenMonth($tempBeginDate);
        $tempBeginDate = DateTools::getGivenDatePlusTwoMonths($tempBeginDate);
        $currentMonth = $tempBeginDate;
        $beginDate = DateTools::getGivenDateLess24Months($tempBeginDate);
    }


    /**
     * @param DateTimeImmutable $beginDate
     * @param DateTimeImmutable $currentDate
     * @return string[]
     */
    public static function last24MonthFulfillment(DateTimeImmutable $beginDate,
                                                  DateTimeImmutable $currentDate): array
    {
        $last24MonthsTab = [];

        while ($beginDate <= $currentDate) {
            $last24MonthsTab[] = $beginDate->format('Y-m');
            $beginDate = $beginDate->modify('+1 month');
        }

        return array_reverse($last24MonthsTab);
    }

}
