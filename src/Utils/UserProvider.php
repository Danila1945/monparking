<?php

namespace App\Utils;

use App\Entity\Text;
use App\Entity\User;
use App\Entity\UsersLogsEvents;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use LogicException;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Security;

class UserProvider
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var User
     */
    private $loggedInUser;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        if (null !== $security->getUser()) {
            $this->loggedInUser = $security->getUser()->getUser();
        }
        $this->entityManager = $entityManager;
    }

    public function getLoggedInUser(): User
    {
        return $this->loggedInUser;
    }

    /**
     * @throws Exception
     */
    public function recordUsersLogs(string $visitedPage): void
    {
        if ((!$this->security->isGranted('ROLE_ADMINISTRATOR')) &&
            (!$this->security->isGranted('ROLE_PREVIOUS_ADMIN'))) {
            $loginEvent = new UsersLogsEvents($this->loggedInUser, new DateTimeImmutable('',
                new DateTimeZone('Europe/Paris')), $visitedPage);
            $this->entityManager->persist($loginEvent);
            $this->entityManager->flush();
        }
    }

    public function checkIsAdministratorOrLoggedInUser(User $consultedUser): void
    {
        if ((!$this->security->isGranted('ROLE_ADMINISTRATOR')) && $this->loggedInUser !== $consultedUser) {
            throw new LogicException('Logged in user not allowd to access the consulted user details');
        }
    }

    public function checkIsLoggedInUserSameAsChosenUser(User $chosenUser): void
    {
        if ($this->loggedInUser !== $chosenUser) {
            throw new LogicException('Logged in user must be the same as the chosen user');
        }
    }

    public function checkIsRenterOrTenantUser(User $chosenUser): void
    {
        if ('ROLE_RENTER' !== $chosenUser->getRole() && 'ROLE_TENANT' !== $chosenUser->getRole()) {
            throw new LogicException('User role must be ROLE_RENTER or ROLE_TENANT');
        }
    }

    public function immunityToggle(User $chosenUser): void
    {
        if (true !== $chosenUser->getInsolvencyImmunity()) {
            $chosenUser->setInsolvencyImmunity(true);
            if (true === $chosenUser->getIsUserBlockedForInsolvency()) {
                $chosenUser->setBlockedForInsolvency(false);
            }
        } elseif (true === $chosenUser->getInsolvencyImmunity()) {
            $chosenUser->setInsolvencyImmunity(false);
        }
        $this->entityManager->flush();
    }

    public function enableToggle(User $userToBeEnabled): void
    {
        if ('ROLE_ADMINISTRATOR' === $userToBeEnabled->getRole()) {
            throw new LogicException('The user with ROLE_ADMINISTRATOR role can\'t be enabled/disabled');
        }

        if ($userToBeEnabled->getIsUserEnabled()) {
            $userToBeEnabled->setIsUserNotEnabled();
        } else {
            $userToBeEnabled->setIsUserEnabled();
        }
        $this->entityManager->flush();
    }

    public function setFreeRenterToggle(User $user): void
    {
        if ('ROLE_RENTER' !== $user->getRole()) {
            throw new LogicException('Only renter users can be free renters');
        }

        if (null === $user->getIsFreeRenter() || false === $user->getIsFreeRenter()) {
            $user->setIsFreeRenter(true);
        } else {
            $user->setIsFreeRenter(false);
        }

        $this->entityManager->flush();
    }


    public function setUserTextingToggle(User $user): void
    {
        if (false === $user->getIsTextingEnabled() || null === $user->getIsTextingEnabled()) {
            $user->setIsTextingEnabled(true);
        } else {
            $user->setIsTextingEnabled(false);
        }
        $this->entityManager->flush();
    }

    public function checkIfAlreadyExistingParkingNumber(int $parkingNumber, FormInterface $form): void
    {
        $existingParkingNumberTab = $this->entityManager
            ->getRepository(User::class)->countUsersByParkingNumber($parkingNumber);

        if ('0' !== $existingParkingNumberTab[0]['total']) {
            $form->addError(new FormError('No de parking appartenant déjà à un autre prêteur'));
        }
    }

    public function checkIfExistingUser(string $username, FormInterface $form): void
    {
        $existingUser = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $username]);
        if ($existingUser instanceof User) {
            $form->addError(new FormError('Nom d\'utilisateur déjà utilisé'));
        }
    }

    public function checkIsLoggedInUserTexting(): void
    {
        if (!$this->loggedInUser->getIsTextingEnabled()) {
            throw new LogicException('User is not a texting user !');
        }
    }

    public function checkIsLoggedInUserTextAuthor(Text $text): void
    {
        if ($this->loggedInUser->getId() !== $text->getAuthorId()) {
            throw new LogicException('User is not this text author');
        }
    }

    public function toggleSetBlockedForInsolvency(DateTimeImmutable $now, DateTimeImmutable $firstDueDate, User $loggedInUser)
    {
        if ($now > $firstDueDate) {
            if (true !== $loggedInUser->getInsolvencyImmunity()) {
                $loggedInUser->setBlockedForInsolvency(true);
                $this->entityManager->flush();
            }
        } else {
            if (true === $loggedInUser->getIsUserBlockedForInsolvency()) {
                $loggedInUser->setBlockedForInsolvency(false);
                $this->entityManager->flush();
            }
        }
    }
}
