<?php

namespace App\Utils;

use App\Entity\Messages;
use App\Entity\User;
use App\Exception\InvalidMessageException;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MessagesProvider
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManger;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    public function __construct(EntityManagerInterface $entityManager,
                                AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->entityManger = $entityManager;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function receiptAcknowledgement(Messages $message): void
    {
        if ((true !== $message->getReceiptAcknowledgement() || null === $message->getReceiptAcknowledgement()) &&
            (!$this->authorizationChecker->isGranted('ROLE_PREVIOUS_ADMIN'))) {
            $message->setReceiptAcknowledgement(true);
            $message->setReadingDate(new DateTimeImmutable());
        }

        $this->entityManger->persist($message);
        $this->entityManger->flush();
    }

    /**
     * @param User $loggedInUser
     * @param Messages $message
     * @throws InvalidMessageException
     */
    public function checkLoggedInUserIsReceiver(User $loggedInUser, Messages $message): void
    {
        if ($loggedInUser !== $message->getReceiver()) {
            throw InvalidMessageException::loggedInUserIsNotTheAllowedReceiver();
        }
    }

    /**
     * @param User $loggedInUser
     * @param Messages $message
     * @throws InvalidMessageException
     */
    public function messageRemove(User $loggedInUser, Messages $message): void
    {
        if (($loggedInUser !== $message->getReceiver())
            && ($loggedInUser !== $message->getTransmitter())) {
            throw InvalidMessageException::isUserNotAuthorizedToCancelThisMessage();
        }

        $this->entityManger->remove($message);
        $this->entityManger->flush();
    }

    /**
     * @param User $loggedInUser
     * @param Messages $message
     * @throws InvalidMessageException
     */
    public function checkIsMessageRead(User $loggedInUser, Messages $message): void
    {
        if ($loggedInUser === $message->getReceiver()) {
            if (null === $message->getReadingDate()) {
                throw InvalidMessageException::isReceivedMessageNotReadYet();
            }
        }
    }

    /**
     * @param User $loggedInUser
     * @param Messages $message
     * @throws InvalidMessageException
     */
    public function setArchivedByReceiverOrTransmitter(User $loggedInUser, Messages $message): void
    {
        if ($loggedInUser === $message->getReceiver()) {
            $message->setIsArchivedByReceiver(true);
            $this->entityManger->flush();
        } elseif ($loggedInUser === $message->getTransmitter()) {
            $message->setIsArchivedByTransmitter(true);
            $this->entityManger->flush();
        } else {
            throw InvalidMessageException::isUserNotAuthorizedToArchiveThisMessage();
        }
    }

    public function checkIsLoggedInUserSendingMessageToHimself(User $sender, User $receiver)
    {
        if ($sender === $receiver) {
            throw InvalidMessageException::loggedInUserIsSendingMessageTomHimself();
        }
    }

}
