<?php

namespace App\Utils;

class TabTools
{
    public static function getFirstElementOfATab(array $tab)
    {
        return reset($tab);
    }
}
