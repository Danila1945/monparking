<?php

namespace App\Utils;

use Symfony\Component\HttpFoundation\Request;

class PageTools
{
    public static function isWordAppearing(string $url, string $word): bool
    {
        return false !== strpos($url, $word);
    }

    public static function getReferer(Request $request): string
    {
        return $request->headers->get('referer', '/');
    }
}
