<?php


namespace App\Utils;

use Doctrine\ORM\EntityManagerInterface;

class DayProvider
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

//    /**
//     * @param DateTimeImmutable $dayDate
//     * @return Day
//     */
//    public function getOrCreateDay(DateTimeImmutable $dayDate): Day
//    {
//        $day = $this->entityManager->getRepository(Day::class)->findOneBy(['date' => $dayDate]);
//
//        if (null === $day) {
//            $day = new Day($dayDate);
//            $this->entityManager->persist($day);
//        }
//
//        $this->entityManager->flush();
//
//        return $day;
//    }
}
