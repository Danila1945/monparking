<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Exception;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ParkingSubleaseRepository")
 * @ORM\Table(name="parkingsublease", uniqueConstraints={@UniqueConstraint(name="idx_day_date_user", columns={"day_date", "user_id"})})
 */
class ParkingSublease
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="datetime_immutable")
     * @OrderBy({"name" = "ASC"})
     */
    private $creationDate;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $paidAt;

    public function getCreationDate(): ?DateTimeImmutable
    {
        return $this->creationDate;
    }

    /**
     * @ORM\Column(type="datetime_immutable")
     * @OrderBy({"name" = "ASC"})
     */
    private $dayDate;

    public function getDayDate(): DateTimeImmutable
    {
        return $this->dayDate;
    }

    /**
     * @ORM\Column(type="integer")
     */
    private $subleaseParkingNumber;

    public function getSubleaseParkingNumber(): int
    {
        return $this->subleaseParkingNumber;
    }

    public function setSubleaseParkingNumber(int $parkinNumber): void
    {
        $this->subleaseParkingNumber = $parkinNumber;
    }

    public function getRenterParkingNumber(): ?int
    {
        return $this->user->getParkingNumber();
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="parkingSublease", cascade={"persist"})
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="parkingSubleaseAsTaker", cascade={"persist"})
     */
    private $taker;

    public function getTaker(): ?User
    {
        return $this->taker;
    }

    public function setTaker(User $taker): void
    {
        $this->taker = $taker;
    }

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    public function getPrice(): float
    {
        return $this->price;
    }

    private function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSubleaseOpened;

    public function subleaseRetract(): void
    {
        $this->isSubleaseOpened = true;
        $this->isTaken = false;
        $this->taker = null;
        $this->paidAt = null;
        $this->dueDate = null;
    }

    public function getIsSubleaseOpened(): bool
    {
        return $this->isSubleaseOpened;
    }

    public function unSublease(): void
    {
        $this->isSubleaseOpened = false;
    }

    /**
     * @ORM\Column(type="boolean")
     */
    private $isTaken;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $dueDate;

    public function getIsTaken(): bool
    {
        return $this->isTaken;
    }

    public function setIsTaken(): void
    {
        $this->isTaken = true;
        $this->unSublease();
    }

    public function markAsPaid(): void
    {
        if (0.0 !== $this->price) {
            $this->paidAt = new DateTimeImmutable();
            if (null !== $this->dueDate) {
                $this->dueDate = null;
            }
        }
    }

    public function markAsNotPaid(): void
    {
        if (0.0 !== $this->price) {
            $this->paidAt = null;
            $this->setDueDate();
        }
    }

    public function getIsPaid(): bool
    {
        return null !== $this->paidAt;
    }

    public function getIsNotPaid(): bool
    {
        return null === $this->paidAt;
    }

    public function getPaidAt(): ?DateTimeImmutable
    {
        return $this->paidAt;
    }

    /**
     * ParkingSublease constructor.
     *
     * @throws Exception
     */
    public function __construct(DateTimeImmutable $dayDate, User $user, float $price)
    {
        $this->day = null;
        $this->dayDate = $dayDate;
        $this->isSubleaseOpened = true;
        $this->isTaken = false;
        $this->creationDate = new DateTimeImmutable();
        $this->user = $user;
        $this->subleaseParkingNumber = $user->getParkingNumber();
        if ($this->user->getIsFreeRenter()) {
            $this->price = 0.0;
        } else {
            $this->price = $price;
        }
    }

    public function getDueDate(): ?DateTimeImmutable
    {
        return $this->dueDate;
    }

    public function setDueDate(): self
    {
        $dayDueDate = $this->dayDate;
        $dayDueDate = $dayDueDate->modify('last day of this month');
        $dayDueDate = $dayDueDate->modify('+ 10 days');

        $this->dueDate = $dayDueDate;

        return $this;
    }
}
