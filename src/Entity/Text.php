<?php

namespace App\Entity;

use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TextRepository")
 */
class Text
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $content;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDiscarded;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $creationDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $authorId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIsDiscarded(): ?bool
    {
        return $this->isDiscarded;
    }

    public function setIsDiscarded(bool $isDiscarded): self
    {
        $this->isDiscarded = $isDiscarded;

        return $this;
    }

    /**
     * Requests constructor.
     *
     * @throws Exception
     */
    public function __construct(string $content, User $author)
    {
        $this->creationDate = new DateTimeImmutable('', new DateTimeZone('Europe/Paris'));
        $this->isDiscarded = false;
        $this->content = $content;
        $this->authorId = $author->getId();
    }

    public function getCreationDate(): ?DateTimeImmutable
    {
        return $this->creationDate;
    }

    public function setCreationDate(DateTimeImmutable $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getAuthorId(): ?int
    {
        return $this->authorId;
    }

    public function setAuthorId(int $authorId): self
    {
        $this->authorId = $authorId;

        return $this;
    }
}
