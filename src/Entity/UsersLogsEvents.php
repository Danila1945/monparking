<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
     * @ORM\Entity(repositoryClass="App\Repository\UsersLogsEventsRepository")
     */
    class UsersLogsEvents
    {
        /**
         * @ORM\Id()
         * @ORM\GeneratedValue()
         * @ORM\Column(type="integer")
         */
        private $id;

        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="LoginEvents")
         */
        private $loggedInUser;

        /**
         * @ORM\Column(type="datetime_immutable", nullable=true)
         */
        private $loginDate;

        /**
         * @ORM\Column(type="string", length=25, nullable=true)
         */
        private $visitedPage;

        public function getId(): ?int
        {
            return $this->id;
        }

        public function getLoggedInUser(): ?User
        {
            return $this->loggedInUser;
        }

        public function setLoggedInUser(?User $loggedInUser): self
        {
            $this->loggedInUser = $loggedInUser;

            return $this;
        }

        public function getLoginDate(): ?DateTimeImmutable
        {
            return $this->loginDate;
        }

        public function setLoginDate(?DateTimeImmutable $loginDate): self
        {
            $this->loginDate = $loginDate;

            return $this;
        }

        public function __construct(User $loggedInUser, DateTimeImmutable $loginDate, string $visitedPage)
        {
            $this->loggedInUser = $loggedInUser;
            $this->loginDate = $loginDate;
            $this->visitedPage = $visitedPage;
        }

        public function getVisitedPage(): ?string
        {
            return $this->visitedPage;
        }

        public function setVisitedPage(?string $visitedPage): self
        {
            $this->visitedPage = $visitedPage;

            return $this;
        }
    }
