<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SiteOptionRepository")
 */
class SiteOption
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", options={"default":"5.0"})
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $optionName;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getPrice(): ?float
    {
        return $this->price;
    }


    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * SiteOption constructor.
     * @param float $price
     */
    public function __construct(float $price, string $optionName)
    {
        $this->price = $price;
        $this->optionName = $optionName;
    }

    public function getOptionName(): ?string
    {
        return $this->optionName;
    }

    public function setOptionName(string $optionName): self
    {
        $this->optionName = $optionName;

        return $this;
    }

}
