<?php

namespace App\ControllerHelpers;

use DateTimeImmutable;
use Exception;

class LastDayOfAMonth
{
    /**
     * @var DateTimeImmutable
     */
    private $outputDate;

    /**
     * @var DateTimeImmutable
     */
    private $beginDate;

    /**
     * @return DateTimeImmutable
     */
    public function getOutputDate()
    {
        return $this->outputDate;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getBeginDate()
    {
        return $this->beginDate;
    }

    /**
     * LastDayOfAMonth constructor.
     *
     * @throws Exception
     */
    public function __construct(string $month)
    {
        $this->beginDate = new DateTimeImmutable($month);
        $this->outputDate = $this->beginDate->modify('last day of this month');
    }
}
