<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 28.08.2018
 * Time: 21:11.
 */

namespace App\ControllerHelpers;

use App\Entity\Day;
use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Repository\ParkingSubleaseRepository;
use DateTimeImmutable;

/**
 * Class DayData.
 */
class DayData
{
    /*
     * properties
     * constructor
     * public methods
     *  > getters/setters
     *  > others
     * protected methods
     * private methods
     *
     *
     * */


    /**
     * @var DateTimeImmutable
     */
    private $date;

    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @var Day
     */
    private $day;

    public function getDayDate(): ?Day
    {
        if (null !== $this->day) {
            return $this->day;
        }

        return null;
    }

    public function getDayId(): ?int
    {
        if (null !== $this->day) {
            return $this->day->getId();
        }

        return null;
    }

    /**
     * @return ParkingSublease[]|null
     */
    public function getSubleasedList(): ?array
    {
        return null !== $this->day ? [$this->day->getParkingSublease()] : [];
    }

    public function getIsFreeParkingSubleaseByDay(): ?int
    {
        if (null !== $this->day) {
            return $this->day->getIsFreeParkingSubleaseByDayQuantity();
        }

        return false;
    }

    /**
     * @return ParkingSublease|null
     */
    public function getIsSubleaseOpenedByDayAndUser(ParkingSubleaseRepository $parkingSubleaseRepository, ?Day $day, User $user)
    {
        if (null !== $day) {
            return $parkingSubleaseRepository->findIsSubleaseOpenedByDayAndByUser($day, $user);
        }

        return null;
    }


    public function getDayFromDate(array $allDays, DateTimeImmutable $date): ?Day
    {
        foreach ($allDays as $day) {
            if ($day->getDate()->format('Y-m-d') === $date->format('Y-m-d')) {
                return $day;
            }
        }

        return null;
    }


    /**
     * DayData constructor.
     */
    public function __construct(DateTimeImmutable $date, Day $day)
    {
        $this->date = $date;
        $this->day = $day;
    }
}
