<?php

namespace App\ControllerHelpers;

use App\Entity\User;

class UserAssignment
{
    /**
     * @var User
     */
    private $renterUser;

    /**
     * @var User
     */
    private $takerUser;

    /**
     * @var User
     */
    private $loggedInUser;

    /**
     * @var User
     */
    private $contractorUser;

    /**
     * @return User
     */
    public function getRenterUser()
    {
        return  $this->renterUser;
    }

    public function getTakerUser()
    {
        return $this->takerUser;
    }

    public function usersAssign()
    {
        if ('ROLE_RENTER' === $this->loggedInUser->getRole()) {
            $this->renterUser = $this->loggedInUser;
            $this->takerUser = $this->contractorUser;
        } elseif ('ROLE_TENANT' === $this->loggedInUser->getRole()) {
            $this->renterUser = $this->contractorUser;
            $this->takerUser = $this->loggedInUser;
        }
    }

    /**
     * UserAssignment constructor.
     */
    public function __construct(User $loggedInUser, User $contractorUser)
    {
        $this->loggedInUser = $loggedInUser;
        $this->contractorUser = $contractorUser;
    }
}
