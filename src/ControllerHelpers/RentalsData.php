<?php

namespace App\ControllerHelpers;

use App\Entity\ParkingSublease;

/**
 * Class RentalsData.
 */
class RentalsData
{
    /**
     * @var bool
     */
    private $isWithPrice;

    public function getIsWithPrice(): bool
    {
        return $this->isWithPrice;
    }

    public function setIsWithPrice(): void
    {
        $this->isWithPrice = true;
    }

    public function setIsNotWithPrice(): void
    {
        $this->isWithPrice = false;
    }

    public function isMonthIncludedIntoQuarter(string $subleaseMonth, string $quarter): bool
    {
        if ('first' === $quarter) {
            $isMonthIntoQuarter = '01' === $subleaseMonth || '02' === $subleaseMonth || '03' === $subleaseMonth;
        } elseif ('second' === $quarter) {
            $isMonthIntoQuarter = '04' === $subleaseMonth || '05' === $subleaseMonth || '06' === $subleaseMonth;
        } elseif ('third' === $quarter) {
            $isMonthIntoQuarter = '07' === $subleaseMonth || '08' === $subleaseMonth || '09' === $subleaseMonth;
        } else {
            $isMonthIntoQuarter = '10' === $subleaseMonth || '11' === $subleaseMonth || '12' === $subleaseMonth;
        }

        return $isMonthIntoQuarter;
    }

    /**
     * @param $parkingSublease
     * @return bool
     */
    public function quarterCheck(bool $isChosenYearChosenQuarterExisting, $parkingSublease, string $chosenYearString, string $quarter): bool
    {
        if (false === $isChosenYearChosenQuarterExisting) {
            $isMonthIntoQuarter = $this->isMonthIncludedIntoQuarter($this->getSubleaseMonthString($parkingSublease), $quarter);
            if ($this->yearsAndMonthsCompare($chosenYearString, $this->getSubleaseYearString($parkingSublease), $isMonthIntoQuarter)) {
                return true;
            }
        }

        return false;
    }

    public function getSubleaseMonthString(ParkingSublease $parkingSublease): string
    {
        return $parkingSublease->getDayDate()->format('m');
    }

    public function getSubleaseYearString(ParkingSublease $parkingSublease): string
    {
        return $parkingSublease->getDayDate()->format('Y');
    }

    /**
     * @return bool
     */
    public function yearsAndMonthsCompare(string $previousYear, string $subleaseYear, bool $isMonthIncludedIntoQuarter)
    {
        if ($previousYear === $subleaseYear && $isMonthIncludedIntoQuarter) {
            return true;
        }

        return false;
    }
}
