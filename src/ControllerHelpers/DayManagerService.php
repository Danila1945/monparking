<?php


namespace App\ControllerHelpers;


use App\Entity\Day;
use App\Repository\DayRepository;
use DateTimeImmutable;

class DayManagerService
{

    /**
     * @var DayRepository
     */
    private $dayRepo;

    /**
     * DayManagerService constructor.
     * @param DayRepository $repo
     */
    public function __construct(DayRepository $repo) {
        $this->dayRepo = $repo;
    }

    /**
     * @param DateTimeImmutable $date
     * @return Day|null
     */
    public function getNewDay(DateTimeImmutable $date) {
        return $this->dayRepo->findOneBy(['date' => $date]);
    }

}
