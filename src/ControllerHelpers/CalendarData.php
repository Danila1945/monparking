<?php

namespace App\ControllerHelpers;

use DateTimeImmutable;
use DateTimeZone;
use Exception;

class CalendarData
{
    /**
     * @var array
     *            table with the name of each month written in french,
     *            usefull to show the month name on the calendar web page
     */
    public $months = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Décembre',
    ];

    /**
     * @var array
     */
    public $monthsLowCase = [
        'janvier',
        'février',
        'mars',
        'avril',
        'mai',
        'juin',
        'juillet',
        'août',
        'septembre',
        'octobre',
        'novembre',
        'décembre',
    ];

    /**
     * @var int|null
     */
    private $month;

    public function getMonth(): int
    {
        return $this->month;
    }

    /**
     * @var int|null
     */
    private $year;

    public function getYear(): int
    {
        return $this->year;
    }


    /**
     * @var array
     */
    private $datesList;


    /**
     * @return DateTimeImmutable[]
     */
    public function getDatesList(): array
    {
        return $this->datesList;
    }

    /**
     * @var
     */
    private $isEmployee;

    public function getIsEmployee(): bool
    {
        return $this->isEmployee;
    }

    /**
     * CalendarData constructor.
     * Month is between 1 and 12.
     * @param int|null $year
     * @param int|null $month
     */
    public function __construct(?int $year = null, ?int $month = null)
    {
        if (null === $year) {
            $year = intval(date('Y'));
        }

        if (null === $month || $month < 1 || $month > 12) {
            $month = intval(date('m'));
        }

        $this->year = $year;
        $this->month = $month;
        $this->datesList = $this->createDatesList($year, $month);
    }

    /**
     * returns the first day of the month.
     *
     * @throws Exception
     */
    public function getStartingDay(): DateTimeImmutable
    {
        return (new DateTimeImmutable())->setDate($this->year, $this->month, 1);
    }

    /**
     * $this->months retourne le numéro du mois en cours
     * permet de naviguer d'un mois à l'autre
     * returns the month name.
     */
    public function toString(): string
    {
        return $this->months[$this->month - 1].' '.$this->year;
    }

    public function toStringLowCase(): string
    {
        return $this->monthsLowCase[$this->month - 1].' '.$this->year;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function toStringLowCasePreviousMonth(): string
    {
        $currentYearLessTwo = new DateTimeImmutable('0:00 first day of previous month', new DateTimeZone('Europe/Paris'));
        $previousMonthDateString = $currentYearLessTwo->format('m');
        $previousMonthDateInt = (int) ($previousMonthDateString);
        $YearPreviousMonthDateString = $currentYearLessTwo->format('Y');

        return $this->monthsLowCase[$previousMonthDateInt - 1].' '.$YearPreviousMonthDateString;
    }

    public function toStringFromMonthAndYearString(string $yearMonth): string
    {
        $month = substr($yearMonth, -2);
        $year = substr($yearMonth, 0, 4);
        $intMonth = (int) $month;
        $intYear = (int) $year;

        return $this->months[$intMonth - 1].' '.$intYear;
    }

    public function toStringLowCaseFromMonthAndYearString(string $yearMonth): string
    {
        $month = substr($yearMonth, -2);
        $year = substr($yearMonth, 0, 4);
        $intMonth = (int) $month;
        $intYear = (int) $year;

        return $this->monthsLowCase[$intMonth - 1].' '.$intYear;
    }

    /**
     * Renvoie le nombre de semaines dans le mois.
     *
     * @throws Exception
     */
    public function getWeeks(): int
    {
        $start = $this->getStartingDay();
        $end = $start->modify('+1 month -1 day');
        $startWeek = intval($start->format('W'));
        $endWeek = intval($end->format('W'));
        if (1 === $endWeek) {
            $endWeek = intval($end->modify('- 7 days')->format('W')) + 1;
        }
        $weeks = $endWeek - $startWeek + 1;
        if ($weeks < 0) {
            $weeks = intval($end->format('W'));
        }
//        $weeks = 6;
        return $weeks;
    }

    /**
     * Est-ce que le jour est dans le mois en cours.
     *
     * @throws Exception
     */
    public function withinMonth(\DateTimeImmutable $date): bool
    {
        return $this->getStartingDay()->format('Y-m') === $date->format('Y-m');
    }

    /**
     * Renvoie le mois suivant.
     *
     * @throws Exception
     */
    public function nextMonth(): CalendarData
    {
        $month = $this->month + 1;
        $year = $this->year;
        if ($month > 12) {
            $month = 1;
            ++$year;
        }

        return new CalendarData($year, $month);
    }

    /**
     * Renvoie le mois précédent.
     *
     * @throws Exception
     */
    public function previousMonth(): CalendarData
    {
        $month = $this->month - 1;
        $year = $this->year;
        if ($month < 1) {
            $month = 12;
            --$year;
        }

        return new CalendarData($year, $month);
    }



    private function createDatesList(int $year, int $month): array
    {
        $datesList = [];
        $startOfMonth = (new DateTimeImmutable())->setDate($year, $month, 1)->setTime(0, 0, 0, 0);
        $endOfMonth = $startOfMonth->modify('+1 month')->modify('-1 days');
        while ($startOfMonth <= $endOfMonth) {
//            $formattedDate = $startOfMonth->format('Y-m-d');

            $datesList[] = $startOfMonth;

            /**
             * $startOfMonth is incremented by one day at each loop
             * until it reaches the last day of the month.
             */
            $startOfMonth = $startOfMonth->modify('+1 days');
        }

        return $datesList;
    }

}
