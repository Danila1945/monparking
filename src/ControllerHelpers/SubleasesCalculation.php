<?php

namespace App\ControllerHelpers;

use App\Entity\ParkingSublease;
use App\Entity\User;
use LogicException;

class SubleasesCalculation
{
    /**
     * @var float
     */
    private $isPaidSum = 0;

    /**
     * @var float
     */
    private $isNotPaidSum = 0;

    /**
     * @var float
     */
    private $totalDueAmountForReceipt = 0;

    /**
     * @var ParkingSublease[]
     */
    private $subleases;

    /**
     * @return float
     */
    public function getIsPaidSum()
    {
        return $this->isPaidSum;
    }

    /**
     * @return float
     */
    public function getIsNotPaidSum()
    {
        return $this->isNotPaidSum;
    }

    /**
     * @return float
     */
    public function getTotalDueAmountForReceipt()
    {
        return $this->totalDueAmountForReceipt;
    }

    /**
     * @return User
     */
    public function getSubleasesUser()
    {
        return reset($this->subleases)->getUser();
    }

    /**
     * @return User
     */
    public function getSubleasesTaker()
    {
        return reset($this->subleases)->getTaker();
    }

    public function getException()
    {
        throw new LogicException('Logged-in user not allowed to do this operation');
    }

    public function checkIsCurrentLoggedInUserAuthorized(User $loggedInUser)
    {
        if ('ROLE_RENTER' === $loggedInUser->getRole()) {
            if ($loggedInUser !== $this->getSubleasesUser()) {
                $this->getException();
            }
        } elseif ('ROLE_TENANT' === $loggedInUser->getRole()) {
            if ($loggedInUser !== $this->getSubleasesTaker()) {
                $this->getException();
            }
        }
    }

    public function calculate()
    {
        foreach ($this->subleases as $sublease) {
            if ($sublease->getIsPaid()) {
                $this->isPaidSum += $sublease->getPrice();
            } elseif ($sublease->getIsNotPaid()) {
                $this->isNotPaidSum += $sublease->getPrice();
            }
            $this->totalDueAmountForReceipt += $sublease->getPrice();
        }
    }

    /**
     * SubleasesCalculation constructor.
     */
    public function __construct(array $subleases, User $loggedInUser)
    {
        $this->subleases = $subleases;
        $this->checkIsCurrentLoggedInUserAuthorized($loggedInUser);
        $this->calculate();
    }
}
