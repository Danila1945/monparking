<?php

namespace App\Tests\Sublease;

use App\ControllerHelpers\Security\SecurityUser;
use App\Entity\Day;
use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Exception\InvalidDayDateException;
use App\Exception\InvalidSubleaseException;
use App\Exception\InvalidUserException;
use App\Exception\SubleaseAlreadyClosedException;
use App\Repository\ParkingSubleaseRepository;
use App\Sublease\ParkingManager;
use App\Tests\Common\User\getUserTrait;
use App\Utils\DayProvider;
use DateTime;
use DateTimeImmutable;
use Exception;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Symfony\Component\Security\Core\Security;

class ParkingManagerTest extends TestCase
{
    use getUserTrait;

    /**
     * @throws Exception
     * @covers \App\Sublease\ParkingManager::createOpenedSublease
     */
    public function testCreateOpenedSubleaseWillThrowErrorOnInvalidUser()
    {
        $disabledUser = $this->getAuthorizedRenterUser();
        $disabledUser->setIsUserNotEnabled();

        $parkingManager = $this->getParkingManager($disabledUser);

        $this->expectException(InvalidUserException::class);
        $parkingManager->createOpenedSublease(date('Y-m-d'));
    }

    /**
     * @throws Exception
     * @covers \App\Sublease\ParkingManager::createOpenedSublease
     */
    public function testCreateOpenedSubleaseWillThrowErrorOnUnauthorizedUserRole()
    {
        $takerUser = $this->getAuthorizedTakerUser();

        $parkingManager = $this->getParkingManager($takerUser);

        /*
         * Only a Renter user can create parking sublease
         */
        $this->expectException(InvalidUserException::class);
        $parkingManager->createOpenedSublease(date('Y-m-d'));
    }

    /**
     * @throws Exception
     * @covers \App\Sublease\ParkingManager::createOpenedSublease
     * @dataProvider dateProvider
     */
    public function testCreateOpenedSubleaseWillThrowErrorOnInvalidDay(string $dateString)
    {
        $renterUser = $this->getAuthorizedRenterUser();
        $parkingManager = $this->getParkingManager($renterUser);

        $this->expectException(InvalidDayDateException::class);
        $parkingManager->createOpenedSublease($dateString);
    }

    public function dateProvider(): iterable
    {
        return [
            [date('Y-m-d', strtotime('-3 years'))],
            [date('Y-m-d', strtotime('+3 years'))],
            [date('Y-m-d', strtotime('+63 days'))],
            [date('Y-m-d', strtotime('-1 day'))],
        ];
    }

    /**
     * @covers \App\Sublease\ParkingManager::createOpenedSublease
     *
     * @throws Exception
     */
    public function testCreateAlreadCreatedOpenedSubleaseWillThrowInvalidSubleaseError()
    {
        $date = date('Y-m-d', strtotime('+1 week'));
        $dateTimeImmutable = new DateTimeImmutable($date);
        $day = new Day($dateTimeImmutable);

        $renterUser = $this->getAuthorizedRenterUser();
        $parkingSublease = new ParkingSublease($day, $renterUser, 5);
        $parkingManager = $this->getParkingManager($renterUser, $day, $parkingSublease);

        /*
         * ce test ne fonctionne car c'est au moment de la création qu'il est vérifié si oui ou non le parkingsublease
         * existe déjà et est déjà en open ou taken. Ce n'est pas le cas du test suivant "testTakeAlreadyTakenSublease"
         * qui se base sur un testTakeAlreadyTakenSublease qui existe déjà et qui est déjà en taken
         * Il faut donc déterminer pour quelle raison ce test ne tient pas compte de ce qui existe déjà dans la bd
         * (parkingsubelase préexistant et en opened)
         */

        $parkingSublease = $parkingManager->createOpenedSublease($date);

        $this->expectException(InvalidSubleaseException::class);
        $parkingManager->createOpenedSublease($date);
    }

    /**
     * @covers \App\Sublease\ParkingManager::createOpenedSublease
     *
     * @throws Exception
     */
    public function testTakeAlreadyTakenSubleaseWillThrowInvalidSubleaseError()
    {
        $date = date('Y-m-d', strtotime('+1 week'));
        $dateTimeImmutable = new DateTimeImmutable($date);
        $day = new Day($dateTimeImmutable);

        $renterUser = $this->getAuthorizedRenterUser();
        $renterParkingManager = $this->getParkingManager($renterUser, $day);
        $parkingSublease = $renterParkingManager->createOpenedSublease($date);

        /* To skip the invalidDayException and directly go to the InvalidSubleaseException */
        $parkingSublease->getDay()->isFreeParkingSubleaseByDayIncreaseByOne();

        $tenantUser = $this->getAuthorizedTakerUser();
        $tenantParkingManager = $this->getParkingManager($tenantUser);
        $tenantParkingManager->takeSublease($parkingSublease);

        $this->expectException(InvalidSubleaseException::class);
        $tenantParkingManager->takeSublease($parkingSublease);
    }

    /**
     * @throws Exception
     * @covers \App\Sublease\ParkingManager::removeOpenedSublease
     */
    public function testRemoveOpenedSubleaseWillThrowErrorOnUnauthorizedLoggedInUser()
    {
        $authorizedUser = $this->getAuthorizedRenterUser();
        $unauthorizedUser = $this->getUnauthorizedRenterUser();

        $date = new DateTimeImmutable();
        $dateString = $date->format('Y-m-d');

        $parkingManagerAuthorizedUser = $this->getParkingManager($authorizedUser);
        $parkingSublease = $parkingManagerAuthorizedUser->createOpenedSublease($dateString);

        $parkingManagerUnauthorizedUser = $this->getParkingManager($unauthorizedUser);
        $this->expectException(InvalidUserException::class);
        $parkingManagerUnauthorizedUser->removeOpenedSublease($parkingSublease);
    }

    /**
     * @covers \App\Sublease\ParkingManager::removeOpenedSublease
     *
     * @throws Exception
     */
    public function testRemoveOpenedSubleaseWillThrowErrorOnClosedSublease()
    {
        $renterUser = $this->getAuthorizedRenterUser();
        $parkingManager = $this->getParkingManager($renterUser);
        $date = new DateTimeImmutable();
        $dateString = $date->format('Y-m-d');

        $parkingSublease = $parkingManager->createOpenedSublease($dateString);
        $parkingSublease->setIsTaken();

        $this->expectException(SubleaseAlreadyClosedException::class);
        $parkingManager->removeOpenedSublease($parkingSublease);
    }

    /**
     * @throws Exception
     * @covers \App\Sublease\ParkingManager::createOpenedSublease
     */
    public function testCreateOpenedSubleaseOnSuccess()
    {
        $date = date('Y-m-d', strtotime('+1 week'));
        $dateTimeImmutable = new DateTimeImmutable($date);
        $day = new Day($dateTimeImmutable);

        $loggedInUser = $this->getAuthorizedRenterUser();

        $parkingManager = $this->getParkingManager($loggedInUser, $day);

        $parkingSublease = $parkingManager->createOpenedSublease($date);
        $parkingSubleaseDay = $parkingSublease->getDay();

        $this->assertSame($date, $parkingSubleaseDay->getDate()->format('Y-m-d'));
        $this->assertSame(1, $parkingSubleaseDay->getIsFreeParkingSubleaseByDayQuantity());
        $this->assertSame($loggedInUser, $parkingSublease->getUser());
        $this->assertSame(true, $parkingSublease->getIsSubleaseOpened());
    }

    /**
     * @covers \App\Sublease\ParkingManager::removeOpenedSublease
     *
     * @throws SubleaseAlreadyClosedException
     * @throws Exception
     */
    public function testRemoveOpenedSublease()
    {
        $date = date('Y-m-d', strtotime('+1 week'));
        $dateTime = new DateTime($date);
        $day = new Day(DateTimeImmutable::createFromMutable($dateTime));

        $renterUser = $this->getAuthorizedRenterUser();

        $parkingManager = $this->getParkingManager($renterUser, $day, new ParkingSublease($day, $renterUser, 5));
        $parkingSublease = $parkingManager->createOpenedSublease($date);
        $parkingManager->removeOpenedSublease($parkingSublease);

        $parkingSubleaseDay = $parkingSublease->getDay();

        $this->assertSame($date, $parkingSubleaseDay->getDate()->format('Y-m-d'));
        $this->assertSame(0, $parkingSubleaseDay->getIsFreeParkingSubleaseByDayQuantity());
        $this->assertSame($renterUser, $parkingSublease->getUser());
        $this->assertFalse($parkingSublease->getIsSubleaseOpened());
    }

    /**
     * @covers \App\Sublease\ParkingManager::takeSublease
     *
     * @throws Exception
     */
    public function testTakeSublease()
    {
        $date = date('Y-m-d', strtotime('+1 week'));
        $dateTime = new DateTime($date);
        $day = new Day(DateTimeImmutable::createFromMutable($dateTime));

        $renterUser = $this->getAuthorizedRenterUser();
        $takerLoggedInUser = $this->getAuthorizedTakerUser();

        $parkingManagerTaker = $this->getParkingManager($takerLoggedInUser);
        $parkingManagerRenter = $this->getParkingManager($renterUser, $day);

        $parkingSublease = $parkingManagerRenter->createOpenedSublease($date);
        $parkingManagerTaker->takeSublease($parkingSublease);

        $this->assertSame($renterUser, $parkingSublease->getUser());
        $this->assertTrue($parkingSublease->getIsTaken());
        $this->assertSame($takerLoggedInUser, $parkingSublease->getTaker());
        $this->assertSame(0, $parkingSublease->getDay()->getIsFreeParkingSubleaseByDayQuantity());
    }

    /**
     * @throws Exception
     */
    public function testUntakeSublease()
    {
        $date = date('Y-m-d', strtotime('+1 week'));

        $renterUser = $this->getAuthorizedRenterUser();
        $takerUser = $this->getAuthorizedTakerUser();
        $administratorUser = $this->getAdministratorUser();

        $parkingManagerRenter = $this->getParkingManager($renterUser);
        $parkingSublease = $parkingManagerRenter->createOpenedSublease($date);

        $parkingSublease->getDay()->isFreeParkingSubleaseByDayIncreaseByOne();

        $parkingManagerTaker = $this->getParkingManager($takerUser);
        $parkingManagerTaker->takeSublease($parkingSublease);

        $parkingManagerAdministrator = $this->getParkingManager($administratorUser);
        $parkingManagerAdministrator->unTakeSublease($parkingSublease);

        $this->assertSame($renterUser, $parkingSublease->getUser());
        $this->assertTrue($parkingSublease->getIsSubleaseOpened());
        $this->assertSame(false, $parkingSublease->getIsTaken());
        $this->assertSame(null, $parkingSublease->getTaker());
        $this->assertSame(null, $parkingSublease->getDueDate());
        $this->assertSame(1, $parkingSublease->getDay()->getIsFreeParkingSubleaseByDayQuantity());
    }

    /**
     * @throws Exception
     */
    private function getParkingManager(?User $loggedInUser = null, ?Day $day = null,
                                       ?ParkingSublease $parkingSublease = null): ParkingManager
    {
        return new ParkingManager(
            $this->getParkingSubleaseRepository($loggedInUser, $day, $parkingSublease),
            $this->getSecurity($loggedInUser),
            $this->getDayProvider($day)
        );
    }

    /**
     * @throws Exception
     */
    private function getParkingSubleaseRepository(User $loggedInUser, ?Day $day = null,
                                                  ?ParkingSublease $parkingSublease = null): ParkingSubleaseRepository
    {
        /** @var ParkingSubleaseRepository $repository */
        $repository = $this->prophesize(ParkingSubleaseRepository::class);

        $day = $day ?? new Day(new DateTimeImmutable());

        /**
         * $parkingSublease which will be created through the $parkingManager->createOpenedSublease() function call.
         */
        $parkingSublease = $parkingSublease ?? new ParkingSublease($day, $loggedInUser, 5);

        if ('ROLE_RENTER' === $loggedInUser->getRole()) {
            $repository->findIsSubleaseOpenedOrTakenByDayDateAndByRenter(Argument::any(), Argument::any())->willReturn(null);
        } elseif ('ROLE_TENANT' === $loggedInUser->getRole()) {
            $repository->findIsSubleaseTakenByDayDateAndByTaker(Argument::any(), Argument::any())->willReturn(null);
        }

        $repository->save(Argument::any())->willReturn($parkingSublease);
        $repository->remove(Argument::any())->willReturn($parkingSublease);

        return $repository->reveal();
    }

    /**
     * @throws Exception
     */
    private function getDayProvider(?Day $day = null): DayProvider
    {
        $dayProvider = $this->prophesize(DayProvider::class);

        $day = $day ?? new Day(new DateTimeImmutable());

        /* @var DayProvider $dayProvider  */
        $dayProvider->getOrCreateDay(Argument::any())->willReturn($day);

        return $dayProvider->reveal();
    }

    private function getSecurity(?User $loggedInUser = null): Security
    {
        $security = $this->prophesize(Security::class);

        /* @var SecurityUser $securityUser */
        $securityUser = $this->prophesize(SecurityUser::class);
        $securityUser->getUser()->willReturn($loggedInUser ?? $this->getAuthorizedTakerUser());

        /* @var Security $security */
        $security->getUser()->willReturn($securityUser);

        return $security->reveal();
    }
}
