<?php

namespace App\Tests\DTO\Sublease;

use App\DTO\Sublease\DTOParkingSubleaseTakerList;
use App\Entity\Day;
use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Tests\Common\Entity\FakeUser;
use DateTimeImmutable;
use Faker\Factory as FakerFactory;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;


/**
 * DTOParkingSubleaseTakerListTest
 */
class DTOParkingSubleaseTakerListTest extends TestCase
{


    /**
     * @var FakerFactory
     */
    private $faker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = FakerFactory::create();
    }

    private function getDateTimeAsString(
        string $startDate = '-30 years',
        string $endDate = 'now',
        $timezone = null,
        string $format = 'Y-m-d'
    ): string {
        return ($this->faker->dateTimeBetween($startDate, $endDate, $timezone))->format($format);
    }

    private function getUser(string $role): User
    {
        $user = FakeUser::generate(
            null,
            null,
            null,
            null,
            $role,
            [
                'id' => $this->faker->numberBetween(),
                'parkingNumber' => $this->faker->numberBetween(0, 50),
            ]
        );

        return $user;
    }

    private function getSubleases(User $user, int $count = 50): array
    {
        $subleases = [];

        for ($index = 0; $index < $count; $index++) {
            $subleases[] = new ParkingSublease(
                new Day(new DateTimeImmutable($this->getDateTimeAsString('-2 years', '-1 week'))),
                $user, 5
            );
        }

        return $subleases;
    }

    private function getDTOParkingSubleaseTakerList(
        ?string $month = null,
        ?User $user = null,
        ?string $role = null,
        ?array $subleases = null
    ): DTOParkingSubleaseTakerList {
        $month = $month ?? $this->getDateTimeAsString('-2 years', '-1 week', null, 'Y-m');
        $role = $role ?? 'roleTenant';
        $user = $user ?? $this->getUser($role);
        $subleases = $subleases ?? $this->getSubleases($user);

        return new DTOParkingSubleaseTakerList(
            $month,
            $user,
            $role,
            $subleases
        );
    }


    public function testConstructor(): void
    {
        $month = $this->getDateTimeAsString('-2 years', '-1 week', null, 'Y-m');
        $role = 'roleTenant';
        $user = $this->getUser($role);
        $subleases = $this->getSubleases($user);

        $takerList = $this->getDTOParkingSubleaseTakerList(
            $month,
            $user,
            $role,
            $subleases
        );

        $this->assertEquals($month, $takerList->getMonth());
        $this->assertEquals($user, $takerList->getContractor());
        $this->assertEquals($role, $takerList->getUserRole());
    }

    public function testUnPaidSumCalculate(): void
    {
        $takerList = $this->getDTOParkingSubleaseTakerList();

        // Un-comment this when constructor doesn't call `unPaidSumCalculate`
        // $this->assertNull($takerList->getUnpaidSum());
        // $takerList->unPaidSumCalculate();
        $this->assertNotNull($takerList->getUnpaidSum());

        $expectedSum = 0;
        $subleasesPerContractor = $takerList->getMonthSubleasesPerContractor();
        if (!empty($subleasesPerContractor)) {
            print('Not empty table !');
            foreach ($subleasesPerContractor as $sublease) {
                /* @var ParkingSublease $sublease */
                if ($sublease->getIsNotPaid()) {
                    $expectedSum += $sublease->getPrice();
                }
            }
        }else{
            print('Empty table !');
        }

        $this->assertEquals($expectedSum, $takerList->getUnpaidSum());
    }

    public function testGetBeginDueDate(): void
    {
        $currentMonth = $this->getDTOParkingSubleaseTakerList('2020-02');
        $presumedDueDate = new DateTimeImmutable('midnight');
        $presumedDueDate = $presumedDueDate->setDate(2020,03,01);
        $this->assertEquals($presumedDueDate, $currentMonth->getBeginDueDate());
    }

    public function testGetDueDate(): void
    {
        $currentMonth = $this->getDTOParkingSubleaseTakerList('2020-02');
        $presumedDueDate = new DateTimeImmutable('midnight');
        $presumedDueDate = $presumedDueDate->setDate(2020,03,11);
        $this->assertEquals($presumedDueDate, $currentMonth->getDueDate());
    }

    public function testGetMonth(): void
    {
        $currentMonth = $this->getDTOParkingSubleaseTakerList('2020-02');
        $presumedStringMonth = '2020-02';
        $this->assertEquals($presumedStringMonth, $currentMonth->getMonth());
    }

    public function testCheckIfMonthIsPaid(): void
    {
        $takerList = $this->getDTOParkingSubleaseTakerList();

        // Un-comment this when constructor doesn't call `unPaidSumCalculate`
        // $this->assertNull($takerList->getUnpaidSum());
        // $takerList->unPaidSumCalculate();

        $expectedResult = true;
        $subleasesPerContractor = $takerList->getMonthSubleasesPerContractor();

        if (!empty($subleasesPerContractor)) {
//            print('Not empty table !');
            foreach ($subleasesPerContractor as $sublease) {

                /* @var ParkingSublease $sublease */
                if (($sublease->getIsNotPaid()) && ($expectedResult === true)) {
                    $expectedResult = false;
                }
            }
        }else{
            print('Empty table !');
        }
        $this->assertEquals($expectedResult, $takerList->checkIfMonthIsPaid());
    }


    public function testGetPaymentDateWhenIsUnpaid(): void
    {
        $takerList = $this->getDTOParkingSubleaseTakerList();
        $subleasesPerContractor = $takerList->getMonthSubleasesPerContractor();


        /* @var ParkingSublease $lastSublease */
        if (!empty($subleasesPerContractor)) {
            print('Not empty table !');
            $lastSublease = end($subleasesPerContractor);
            $lastSublease->markAsNotPaid();
            $paymentDate = $lastSublease->getPaidAt();
        }else{
            print('Empty table !');
            $paymentDate = null;
        }

        $this->assertEquals($paymentDate, $takerList->getPaymentDate());
    }

    public function testGetPaymentDateWhenIsPaid(): void
    {
        $takerList = $this->getDTOParkingSubleaseTakerList();
        $subleasesPerContractor = $takerList->getMonthSubleasesPerContractor();

        $lastSublease = null;

        /* @var ParkingSublease $lastSublease */
        if (!empty($subleasesPerContractor)) {
//            exit();
            $lastSublease = end($subleasesPerContractor);
            $lastSublease->setIsTaken();
            $lastSublease->setDueDate();
            $lastSublease->markAsPaid();
            $paymentDate = $lastSublease->getPaidAt();
        }else{
            print('Empty table !');
            $paymentDate = null;
        }

        $this->assertEquals($paymentDate, $takerList->getPaymentDate());
    }


    public function testMonthSubleasesPerContractor(): void
    {
        $takerList = $this->getDTOParkingSubleaseTakerList();
        $subleasesPerContractor = $takerList->getMonthSubleasesPerContractor();

        $list = [];

        if (!empty($subleasesPerContractor)){
            print('Not empty table !');
            foreach ($subleasesPerContractor as $sublease) {
                $list[] = $sublease;
            }
        }else{
            print('Empty table !');
        }

        $this->assertEquals($list, $takerList->getMonthSubleasesPerContractor());
    }

    public function testGetContractor(): void
    {
//        $this->markTestSkipped();
        $takerList = $this->getDTOParkingSubleaseTakerList();
        $subleasesPerContractor = $takerList->getMonthSubleasesPerContractor();

        /* @var User $contractor*/
        $contractor = null;

        /* @var ParkingSublease $lastSublease */
        if (!empty($subleasesPerContractor)) {
            print('Not empty table !');
//            exit();
            $lastSublease = end($subleasesPerContractor);
            if ($this->getUser('ROLE_RENTER')->getRole() === 'ROLE_RENTER' && $lastSublease->getIsTaken()){
                print('ok role renter is connected');
                $contractor = $lastSublease->getTaker();
            }elseif ($this->getUser('ROLE_TENANT')->getRole() === 'ROLE_TENANT' && $lastSublease->getIsTaken()){
                print('ok role tenant is connected');
                $contractor = $lastSublease->getUser();
            }
        }else{
            print('Empty table !');
        }

        $this->assertEquals($contractor, $takerList->getContractor());
    }


}
