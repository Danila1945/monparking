<?php

namespace App\Tests\Common\User;

use App\Entity\User;

trait getUserTrait
{
    public function getUser(): User
    {
        return new User(
            'test_user',
            'password',
            'User',
            'User',
            'ROLE_USER'
        );
    }

    public function getAdministratorUser(): User
    {
        return new User(
            'test_administrator',
            'password',
            'Administrator',
            'Admin',
            'ROLE_ADMINISTRATOR'
        );
    }

    public function getAuthorizedRenterUser(): User
    {
        return new User(
            'test_authorized_renter',
            'password',
            'Renter',
            'Authorized',
            'ROLE_RENTER'
        );
    }

    public function getOtherAuthorizedRenterUser(): User
    {
        return new User(
            'test_other_authorized_renter',
            'password',
            'OtherRenter',
            'OtherAuthorized',
            'ROLE_RENTER'
        );
    }

    public function getUnauthorizedRenterUser()
    {
        return new User(
            'test_unauthorized_renter',
            'password',
            'Renter',
            'Unauthorized',
            'ROLE_RENTER'
        );
    }

    public function getAuthorizedTakerUser()
    {
        return new User(
            'test_authorized_taker',
            'password',
            'TakerAuthorized',
            'UserAuthorized',
            'ROLE_TENANT'
        );
    }

    public function getUnauthorizedTakerUser()
    {
        return new User(
            'test_unauthorized_taker',
            'password',
            'TakerUnauthorized',
            'UserUnauthorized',
            'ROLE_TENANT'
        );
    }
}
