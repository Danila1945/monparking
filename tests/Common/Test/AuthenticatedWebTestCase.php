<?php

namespace App\Tests\Common\Test;

use App\ControllerHelpers\Security\SecurityUser;
use App\Entity\User;
use App\Tests\Common\Entity\FakeUser;
use Faker\Factory as FakerFactory;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class AuthenticatedWebTestCase
 */
class AuthenticatedWebTestCase extends WebTestCase
{
    /**
     * @var KernelBrowser
     */
    protected $client;

    /**
     * @var \Faker\Generator
     */
    protected $faker;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->faker = FakerFactory::create();
    }

    /**
     * Authenticate a given user.
     * Warning: User MUST be a real user. User's username AND id MUST match with database data!
     *
     * @param User $user
     */
    protected function logInAsUser(User $user): void
    {
        $session = $this->client->getContainer()->get('session');

        $firewallName = 'mon_parking_secured_area';
        // if you don't define multiple connected firewalls, the context defaults to the firewall name
        // See https://symfony.com/doc/current/reference/configuration/security.html#firewall-context
        $firewallContext = $firewallName;

        // you may need to use a different token class depending on your application.
        // for example, when using Guard authentication you must instantiate PostAuthenticationGuardToken
        $token = new UsernamePasswordToken(new SecurityUser($user), null, $firewallName, [$user->getRole()]);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    protected function logInAsAdmin(): void
    {
        $this->logInAsUser(FakeUser::generate(
            'admin',
            null,
            null,
            null,
            'ROLE_ADMINISTRATOR',
            [
                'id' => 1,
            ]
        ));
    }


    protected function logInAsTenant(): void
    {
        $this->logInAsUser(FakeUser::generate(
            'tenant',
            null,
            null,
            null,
            'ROLE_TENANT',
            [
                'id' => 2,
            ]
        ));
    }

    protected function logInAsTenant2(): void
    {
        $this->logInAsUser(FakeUser::generate(
            'tenant2',
            null,
            null,
            null,
            'ROLE_TENANT',
            [
                'id' => 3,
            ]
        ));
    }

    protected function logInAsRenter(): void
    {
        $this->logInAsUser(FakeUser::generate(
            'renter',
            null,
            null,
            null,
            'ROLE_RENTER',
            [
                'id' => 4,
            ]
        ));
    }


    public function logInAsRenter2(): void
    {
        $this->logInAsUser(FakeUser::generate(
            'renter2',
            null,
            null,
            null,
            'ROLE_RENTER',
            [
                'id' => 5,
            ]
        ));
    }

}
