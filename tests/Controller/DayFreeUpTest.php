<?php

namespace App\Tests\Controller;

use App\Tests\Common\Test\AuthenticatedWebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * DayFreeUpTest
 *
 * @see https://symfony.com/doc/current/testing/http_authentication.html
 */
class DayFreeUpTest extends AuthenticatedWebTestCase
{
    /**
     * When not authenticated, page should redirect to login page
     */
    public function testInvokeWhenNotAuthenticated(): void
    {
        $this->client->request('GET', '/dayfreeup/2020-02-02');
        $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
        $this->assertTrue(
            $this->client->getResponse()->isRedirect('http://localhost/login')
        );
    }


    /**
     * When authenticated with authorized role, page should redirect to calendar page and begin the sublease process
     * Ne marche pas :-(
     */
    public function testInvokeWhenAuthenticatedAsRenter(): void
    {
        $this->logInAsRenter();
        $crawler = $this->client->request('POST', '/dayfreeup/2020-02-02');
        $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
    }


    /**
     * When authenticated with unauthorized role, page should redirect to 403 error page
     */
    public function testInvokeWhenunauthorizedAuthenticated(): void
    {
        $this->logInAsTenant();
        $this->client->request('GET', '/dayfreeup/2020-02-02');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->client->getResponse()->getStatusCode());
    }

}
