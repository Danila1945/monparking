<?php

namespace App\Tests\Controller;

use App\Tests\Common\Test\AuthenticatedWebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * UsersListTest
 *
 * @see https://symfony.com/doc/current/testing/http_authentication.html
 */
class UsersListTest extends AuthenticatedWebTestCase
{
    /**
     * When not authenticated, page should redirect to login page
     */
    public function testInvokeWhenNotAuthenticated(): void
    {
        $this->client->request('GET', '/usersList/');
        $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
    }

    public function testInvokeWhenAuthenticatedAsAdmin(): void
    {
        $this->logInAsAdmin();
        $crawler = $this->client->request('GET', '/usersList/');
        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    public function testInvokeWhenAuthenticatedAsTenant(): void
    {
        $this->logInAsTenant();
        $crawler = $this->client->request('GET', '/usersList/');
        $this->assertTrue(
            $this->client->getResponse()->isForbidden()
        );
    }

    public function testInvokeWhenAuthenticatedAsRenter(): void
    {
        $this->logInAsRenter();
        $crawler = $this->client->request('GET', '/usersList/');
        $this->assertTrue(
            $this->client->getResponse()->isForbidden()
        );
    }
}
